-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- DROP DATABASE "al_kautsar" ------------------------------
DROP DATABASE IF EXISTS `al_kautsar`;
-- ---------------------------------------------------------


-- CREATE DATABASE "al_kautsar" ----------------------------
CREATE DATABASE `al_kautsar` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `al_kautsar`;
-- ---------------------------------------------------------


-- CREATE TABLE "addresses" ------------------------------------
CREATE TABLE `addresses`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`user_id` Int NOT NULL,
	`name` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`address` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`city` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`postal_code` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`district` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`region` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`phone` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`latitude` Decimal NULL,
	`longitude` Decimal NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 2;
-- -------------------------------------------------------------


-- CREATE TABLE "admins" ---------------------------------------
CREATE TABLE `admins`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`name` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`email` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`password` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `admins_email_unique` UNIQUE( `email` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 2;
-- -------------------------------------------------------------


-- CREATE TABLE "ads" ------------------------------------------
CREATE TABLE `ads`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`user_id` Int NOT NULL,
	`title` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`link` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`start_date` Date NOT NULL,
	`end_date` Date NOT NULL,
	`image_url` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 4;
-- -------------------------------------------------------------


-- CREATE TABLE "authors" --------------------------------------
CREATE TABLE `authors`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`name` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`biography` Text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`phone` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`email` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`whatsapp` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 10;
-- -------------------------------------------------------------


-- CREATE TABLE "books" ----------------------------------------
CREATE TABLE `books`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`title` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`link` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`category_id` Int NOT NULL,
	`description` Text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`summary` Text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`author_id` Int NULL,
	`isbn` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`cover` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`pages` Int NOT NULL DEFAULT 0,
	`weight` Int NOT NULL DEFAULT 0,
	`size` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`price` Int NOT NULL DEFAULT 0,
	`image_url` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`file_url` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`stock` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
	`status` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 11;
-- -------------------------------------------------------------


-- CREATE TABLE "catalogs" -------------------------------------
CREATE TABLE `catalogs`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`user_id` Int NOT NULL,
	`title` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`sub_title` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`link` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`image_url` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`download_url` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`status` TinyInt NOT NULL DEFAULT 0,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 4;
-- -------------------------------------------------------------


-- CREATE TABLE "categories" -----------------------------------
CREATE TABLE `categories`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`title` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`link` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`description` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 7;
-- -------------------------------------------------------------


-- CREATE TABLE "cities" ---------------------------------------
CREATE TABLE `cities`( 
	`id` BigInt( 20 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`province_id` Int( 11 ) NOT NULL,
	`province` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`type` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`postal_code` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 502;
-- -------------------------------------------------------------


-- CREATE TABLE "documents" ------------------------------------
CREATE TABLE `documents`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`title` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`email` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`upload_url` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "events" ---------------------------------------
CREATE TABLE `events`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`title` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`link` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`place` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`start_date` Date NULL,
	`end_date` Date NULL,
	`booth` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`image_url` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------


-- CREATE TABLE "failed_jobs" ----------------------------------
CREATE TABLE `failed_jobs`( 
	`id` BigInt( 20 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`connection` Text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`queue` Text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`payload` LongText CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`exception` LongText CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`failed_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "messages" -------------------------------------
CREATE TABLE `messages`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`name` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`email` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`message` Text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 2;
-- -------------------------------------------------------------


-- CREATE TABLE "migrations" -----------------------------------
CREATE TABLE `migrations`( 
	`id` Int UNSIGNED AUTO_INCREMENT NOT NULL,
	`migration` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`batch` Int NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 47;
-- -------------------------------------------------------------


-- CREATE TABLE "news" -----------------------------------------
CREATE TABLE `news`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`title` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`link` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`body` Text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`status` TinyInt NOT NULL DEFAULT 0,
	`image_url` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`download_url` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 2;
-- -------------------------------------------------------------


-- CREATE TABLE "order_details" --------------------------------
CREATE TABLE `order_details`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`order_id` Int NOT NULL,
	`product_id` Int NOT NULL,
	`price` Int NOT NULL DEFAULT 0,
	`qty` Int NOT NULL DEFAULT 0,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 5;
-- -------------------------------------------------------------


-- CREATE TABLE "orders" ---------------------------------------
CREATE TABLE `orders`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`code` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`user_id` Int NOT NULL,
	`sub_total` Int NOT NULL DEFAULT 0,
	`courier_budget` Int NOT NULL DEFAULT 0,
	`total` Int NOT NULL DEFAULT 0,
	`payment_status` TinyInt NOT NULL DEFAULT 0 COMMENT '0: pending, 1: accept, 2: reject',
	`status` TinyInt NOT NULL DEFAULT 0 COMMENT '0: pending, 1: packaging, 2: delivery, 3: member accepted, 4: finished',
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 5;
-- -------------------------------------------------------------


-- CREATE TABLE "password_resets" ------------------------------
CREATE TABLE `password_resets`( 
	`email` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`token` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`created_at` Timestamp NULL )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------


-- CREATE TABLE "profiles" -------------------------------------
CREATE TABLE `profiles`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`user_id` Int NOT NULL,
	`name` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`description` Text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`email` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`phone` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`whatsapp` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`facebook` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`instagram` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`twitter` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`linkedin` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "promo_products" -------------------------------
CREATE TABLE `promo_products`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`promo_id` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`product_id` Int NOT NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 53;
-- -------------------------------------------------------------


-- CREATE TABLE "promos" ---------------------------------------
CREATE TABLE `promos`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`title` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`link` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`start_date` Date NOT NULL,
	`end_date` Date NOT NULL,
	`percentage` Int NOT NULL DEFAULT 0,
	`fix_price` Int NOT NULL DEFAULT 0,
	`status` TinyInt NOT NULL DEFAULT 0 COMMENT '0: inactive, 1: active',
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 6;
-- -------------------------------------------------------------


-- CREATE TABLE "provinces" ------------------------------------
CREATE TABLE `provinces`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`name` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 35;
-- -------------------------------------------------------------


-- CREATE TABLE "scripts" --------------------------------------
CREATE TABLE `scripts`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`user_id` Int NOT NULL,
	`title` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`summary` Text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`file_url` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`status` TinyInt NOT NULL DEFAULT 0 COMMENT '0: pending, 1: accept, 2: reject',
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "sliders" --------------------------------------
CREATE TABLE `sliders`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`title` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`sub_title` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`description` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`link` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`image_url` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`start_date` Date NULL,
	`end_date` Date NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 4;
-- -------------------------------------------------------------


-- CREATE TABLE "users" ----------------------------------------
CREATE TABLE `users`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`name` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`email` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`email_verified_at` Timestamp NULL,
	`password` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`remember_token` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`region` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`zipcode` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`city` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`district` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`address` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`phone` VarChar CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `users_email_unique` UNIQUE( `email` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 2;
-- -------------------------------------------------------------


-- CREATE TABLE "wishlists" ------------------------------------
CREATE TABLE `wishlists`( 
	`id` BigInt UNSIGNED AUTO_INCREMENT NOT NULL,
	`book_id` Int NOT NULL,
	`user_id` Int NOT NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- Dump data of "addresses" --------------------------------
INSERT INTO `addresses`(`id`,`user_id`,`name`,`address`,`city`,`postal_code`,`district`,`region`,`phone`,`latitude`,`longitude`,`created_at`,`updated_at`) VALUES 
( '1', '1', 'Elvis Sonatha', 'Jl. Ursula 177 RT 2 RW 4', 'Bekasi', '13880', 'Jatimurni', 'Jawa Barat', '085218056736', NULL, NULL, '2020-04-12 02:19:36', '2020-04-12 02:19:36' ),
( '2', '2', 'Umar', 'Jl. Ursula 177 RT 2 RW 4 Jatimurni', 'Bekasi', '13880', NULL, 'Jawa Barat', '08765678876', NULL, NULL, NULL, NULL );
-- ---------------------------------------------------------


-- Dump data of "admins" -----------------------------------
INSERT INTO `admins`(`id`,`name`,`email`,`password`,`created_at`,`updated_at`) VALUES 
( '1', 'elviskudo', 'elviskudo@gmail.com', '$2y$10$69vCjC.PKwB5WAYtiU4m4esih.cd1jo0kdi3rZLhA74UIo7jkeKjC', '2020-04-20 02:44:03', '2020-04-20 02:44:03' );
-- ---------------------------------------------------------


-- Dump data of "ads" --------------------------------------
INSERT INTO `ads`(`id`,`user_id`,`title`,`link`,`start_date`,`end_date`,`image_url`,`created_at`,`updated_at`) VALUES 
( '1', '1', 'Kirim Naskah', 'send-document', '2020-03-28', '2020-03-29', 'ads_kirim-naskah.jpg', '2020-03-28 00:52:09', '2020-04-02 14:33:38' ),
( '2', '1', 'Promo bulan ini', 'promo/promo-bulan-ini', '2020-03-28', '2020-03-29', 'ads_promo-bulan-ini.jpg', '2020-03-28 10:25:41', '2020-04-18 03:15:33' ),
( '3', '1', 'Sirah Nabawiyah', 'ads/sirah-nabawiyah', '2020-03-28', '2020-03-29', 'ads_pre-order.jpg', '2020-03-28 10:26:19', '2020-04-18 03:16:10' );
-- ---------------------------------------------------------


-- Dump data of "authors" ----------------------------------
INSERT INTO `authors`(`id`,`name`,`biography`,`phone`,`email`,`whatsapp`,`created_at`,`updated_at`) VALUES 
( '1', 'Manshur Abdul Hakim', 'Manshur Abdul Hakim ingin memperkenalkan lebih dalam tentang salah satu sosok terpenting dalam perjuangan dan penyebaran dakwah Islam di seluruh Jazirah Arab, daerah Persia, Syam, dan penakluk Kisra dan Kaisar, yaitu Khalid bin Al-Walid; Pedang Allah yang Tak Terkalahkan.', '0987654321', NULL, '0987654321', '2020-03-27 05:50:56', '2020-03-27 05:58:19' ),
( '2', 'Prof. DR. Raghib As-Sirjani', 'Manshur Abdul Hakim ingin memperkenalkan lebih dalam tentang salah satu sosok terpenting dalam perjuangan dan penyebaran dakwah Islam di seluruh Jazirah Arab, daerah Persia, Syam, dan penakluk Kisra dan Kaisar, yaitu Khalid bin Al-Walid; Pedang Allah yang Tak Terkalahkan.', '0987654', 'raghib@gmail.com', '09876545', '2020-03-28 10:33:45', '2020-03-28 10:33:45' ),
( '3', 'Rabi\' Abdur Rauf Az-Zawawi', 'Rabi\' Abdur Rauf Az-Zawawi', '09876543', NULL, NULL, '2020-03-28 10:39:26', '2020-03-28 10:39:26' ),
( '4', 'Dr. Muhammad Ali Al-Hasyimi', 'Dr. Muhammad Ali Al-Hasyimi', '09876543', NULL, NULL, '2020-03-28 10:40:55', '2020-03-28 10:40:55' ),
( '5', 'Syaikh Hasan Ayyub', 'Syaikh Hasan Ayyub', '087654345', NULL, NULL, '2020-03-28 10:42:18', '2020-03-28 10:42:18' ),
( '6', 'Dr. Musthafa Dieb Al-Bugha, Syeikh Muhyiddin Mistu', 'Dr. Musthafa Dieb Al-Bugha, Syeikh Muhyiddin Mistu', '07876789876', NULL, NULL, '2020-03-28 10:43:57', '2020-03-28 10:43:57' ),
( '7', 'DR. Saiful Bahri', 'DR. Saiful Bahri', '0765676', NULL, NULL, '2020-03-28 10:45:26', '2020-03-28 10:45:26' ),
( '8', 'Imam As-Syafi\'i', 'Imam As-Syafi\'i', '0876787', NULL, NULL, '2020-03-28 10:46:59', '2020-03-28 10:46:59' ),
( '9', 'Prof. DR. Ali Muhammad Ash-Shallabi', 'Prof. DR. Ali Muhammad Ash-Shallabi', '078987678', NULL, NULL, '2020-03-28 10:50:38', '2020-03-28 10:50:38' );
-- ---------------------------------------------------------


-- Dump data of "books" ------------------------------------
INSERT INTO `books`(`id`,`title`,`link`,`category_id`,`description`,`summary`,`author_id`,`isbn`,`cover`,`pages`,`weight`,`size`,`price`,`image_url`,`file_url`,`stock`,`status`,`created_at`,`updated_at`) VALUES 
( '1', 'Khalid bin Walid', 'khalid-bin-walid', '1', 'Manshur Abdul Hakim ingin memperkenalkan lebih dalam tentang salah satu sosok terpenting dalam perjuangan dan penyebaran dakwah Islam di seluruh Jazirah Arab, daerah Persia, Syam, dan penakluk Kisra dan Kaisar, yaitu Khalid bin Al-Walid; Pedang Allah yang Tak Terkalahkan.

Khalid bin Al-Walid belum pernah mengalami kekalahan sekalipun dalam semua pertempuran yang dihadapinya sepanjang hidupnya, dengan keistimewaan kegeniusannya dan pengalaman medan tempur yang luas, dia mampu merumuskan strategi perang yang terprogram dengan baik, mempertimbangkan dan menimbang kekuatan musuhnya, mengintai situasi dan kondisi medan tempur serta melancarkan serangan mendadak. Dia mampu menghancurkan kekuatan pasukan musuh dan mengendalikan jalannya peperangan, dan mengarahkan demi kepentingannya dalam waktu yang singkat, hingga mampu menghancurkan kekuatan musuh dengan lebih cepat, sampai musuh-musuhnya meyakini bahwa Khalid bin Al-Walid memiliki sebuah pedang dari Allah untuk berperang dan meraih kemenangan.', 'Manshur Abdul Hakim ingin memperkenalkan lebih dalam tentang salah satu sosok terpenting dalam perjuangan dan penyebaran dakwah Islam di seluruh Jazirah Arab, daerah Persia, Syam, dan penakluk Kisra dan Kaisar, yaitu Khalid bin Al-Walid; Pedang Allah yang Tak Terkalahkan.', '1', '978-979-592-6870', 'Hard Cover', '623', '990', '15.5 x 24.5 cm', '120000', 'khalid-bin-walid.jpg', NULL, '0', '0', '2020-03-27 06:06:15', '2020-04-02 14:25:03' ),
( '2', 'Bangkit dan Runtuhnya Andalusia', 'bangkit-dan-runtuhnya-andalusia', '1', 'Manshur Abdul Hakim ingin memperkenalkan lebih dalam tentang salah satu sosok terpenting dalam perjuangan dan penyebaran dakwah Islam di seluruh Jazirah Arab, daerah Persia, Syam, dan penakluk Kisra dan Kaisar, yaitu Khalid bin Al-Walid; Pedang Allah yang Tak Terkalahkan.

Khalid bin Al-Walid belum pernah mengalami kekalahan sekalipun dalam semua pertempuran yang dihadapinya sepanjang hidupnya, dengan keistimewaan kegeniusannya dan pengalaman medan tempur yang luas, dia mampu merumuskan strategi perang yang terprogram dengan baik, mempertimbangkan dan menimbang kekuatan musuhnya, mengintai situasi dan kondisi medan tempur serta melancarkan serangan mendadak. Dia mampu menghancurkan kekuatan pasukan musuh dan mengendalikan jalannya peperangan, dan mengarahkan demi kepentingannya dalam waktu yang singkat, hingga mampu menghancurkan kekuatan musuh dengan lebih cepat, sampai musuh-musuhnya meyakini bahwa Khalid bin Al-Walid memiliki sebuah pedang dari Allah untuk berperang dan meraih kemenangan.', 'Manshur Abdul Hakim ingin memperkenalkan lebih dalam tentang salah satu sosok terpenting dalam perjuangan dan penyebaran dakwah Islam di seluruh Jazirah Arab, daerah Persia, Syam, dan penakluk Kisra dan Kaisar, yaitu Khalid bin Al-Walid; Pedang Allah yang Tak Terkalahkan.', '2', '978-979-592-6870', 'Hard Cover', '300', '200', '15.5 x 24.5 cm', '160000', 'book_bangkit-dan-runtuhnya-andalusia.jpg', NULL, '0', '0', '2020-03-28 10:35:19', '2020-04-02 14:30:18' ),
( '3', 'al Baqiyatus Sholihat', 'al-baqiyatus-sholihat', '1', 'al Baqiyatus Sholihat', 'al Baqiyatus Sholihat', '3', '978-979-592-6870', 'Hard Cover', '400', '300', '15.5 x 24.5 cm', '80000', 'book_al-baqiyatus-shalihat.jpg', NULL, '0', '0', '2020-03-28 10:40:19', '2020-04-02 14:30:24' ),
( '4', 'Jati Diri Wanita Muslimah', 'jati-diri-wanita-muslimah', '2', 'Jati Diri Wanita Muslimah', 'Jati Diri Wanita Muslimah', '4', '978-979-592-6870', 'Hard Cover', '400', '230', '15.5 x 24.5 cm', '90000', 'book_jati-diri-wanita-muslimah.jpg', NULL, '0', '0', '2020-03-28 10:41:47', '2020-04-02 14:31:18' ),
( '5', 'as Suluk al Ijtima\'i', 'as-suluk-al-ijtima\'i', '1', 'as Suluk al Ijtima&#039;i', 'as Suluk al Ijtima&#039;i', '5', '978-979-592-6870', 'Hard Cover', '360', '250', '15.5 x 24.5 cm', '96000', 'book_as-suluk-al-ijtimai.jpg', NULL, '0', '0', '2020-03-28 10:43:22', '2020-04-02 14:31:28' ),
( '6', 'al Wafi\'', 'al-wafi', '1', 'al Wafi&#039;', 'al Wafi&#039;', '6', '978-979-592-6870', 'Hard Cover', '340', '230', '15.5 x 24.5 cm', '110000', 'book_al-wafi.jpg', NULL, '0', '0', '2020-03-28 10:44:44', '2020-04-02 14:31:35' ),
( '7', 'Tadabur Juz amma', 'tadabur-juz-amma', '4', 'Tadabur Juz amma', 'Tadabur Juz amma', '7', '978-979-592-6870', 'Hard Cover', '320', '270', '15.5 x 24.5 cm', '140000', 'book_tadabur-juz-amma.jpg', NULL, '0', '0', '2020-03-28 10:46:23', '2020-04-02 14:31:42' ),
( '8', 'ar Risalah', 'ar-risalah', '1', 'ar Risalah', 'ar Risalah', '8', '978-979-592-6870', 'Hard Cover', '640', '250', '15.5 x 24.5 cm', '260000', 'book_ar-risalah.jpg', NULL, '0', '0', '2020-03-28 10:47:46', '2020-04-02 14:31:48' ),
( '9', 'Sejarah Bangsa Tartar', 'sejarah-bangsa-tartar', '6', 'Sejarah Bangsa Tartar', 'Sejarah Bangsa Tartar', '2', '978-979-592-6870', 'Hard Cover', '550', '430', '15.5 x 24.5 cm', '340000', 'book_sejarah-bangsa-tartar.jpg', NULL, '0', '0', '2020-03-28 10:49:42', '2020-04-02 14:31:54' ),
( '10', 'Bangkit dan Runtuhnya Khilafah Utsmaniyah', 'bangkit-dan-runtuhnya-khilafah-utsmaniyah', '6', 'Khilafah Utsmaniyah', 'Khilafah Utsmaniyah', '9', '978-979-592-6870', 'Hard Cover', '780', '480', '15.5 x 24.5 cm', '460000', 'book_bangkit-dan-runtuhnya-khilafah-utsmaniyah.jpg', NULL, '0', '0', '2020-03-28 10:52:05', '2020-04-02 14:32:00' );
-- ---------------------------------------------------------


-- Dump data of "catalogs" ---------------------------------
INSERT INTO `catalogs`(`id`,`user_id`,`title`,`sub_title`,`link`,`image_url`,`download_url`,`status`,`created_at`,`updated_at`) VALUES 
( '1', '1', 'Katalog Buku Promo', 'Februari 2020', 'katalog-buku-promo', 'catalog_ar-risalah.jpg', 'catalog_belajar-inggris-sampe-bisa-free.pdf', '0', '2020-04-11 07:18:40', '2020-04-11 09:44:51' ),
( '2', '1', 'Katalog Buku Anak', 'Januari 2020', 'katalog-buku-anak', 'catalog_al-baqiyatus-shalihatjpg', 'catalog_belajar-nahwu-dari-nol-1.pdf', '0', '2020-04-11 09:47:49', '2020-04-11 09:48:39' ),
( '3', '1', 'Big Sale up to 40%', 'Desember 2019', 'big-sale-up-to-40', 'catalog_as-suluk-al-ijtimai.jpg', 'catalog_belajar-nahwu-dari-nol-2.pdf', '0', '2020-04-11 09:49:37', '2020-04-11 09:49:37' );
-- ---------------------------------------------------------


-- Dump data of "categories" -------------------------------
INSERT INTO `categories`(`id`,`title`,`link`,`description`,`created_at`,`updated_at`) VALUES 
( '1', 'Aqidah', 'aqidah', 'Buku tentang aqidah', '2020-03-26 15:07:23', '2020-04-10 13:38:39' ),
( '2', 'Fiqh', 'fiqh', 'Buku tentang fiqh', '2020-03-26 15:09:22', '2020-04-10 13:40:07' ),
( '3', 'Keluarga', 'keluarga', 'Buku tentang keluarga', '2020-03-26 15:11:25', '2020-04-10 13:40:12' ),
( '4', 'Anak', 'anak', 'Buku tentang anak', '2020-03-26 15:12:23', '2020-04-10 13:40:18' ),
( '5', 'Hadits', 'hadits', 'Buku tentang hadits', '2020-03-26 15:19:36', '2020-04-10 13:40:25' ),
( '6', 'Sejarah', 'sejarah', 'Sejarah', '2020-03-28 10:48:47', '2020-04-10 13:40:37' );
-- ---------------------------------------------------------


-- Dump data of "cities" -----------------------------------
INSERT INTO `cities`(`id`,`province_id`,`province`,`type`,`name`,`postal_code`,`created_at`,`updated_at`) VALUES 
( '1', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Barat', '23681', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '2', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Barat Daya', '23764', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '3', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Besar', '23951', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '4', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Jaya', '23654', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '5', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Selatan', '23719', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '6', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Singkil', '24785', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '7', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Tamiang', '24476', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '8', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Tengah', '24511', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '9', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Tenggara', '24611', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '10', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Timur', '24454', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '11', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Utara', '24382', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '12', '32', 'Sumatera Barat', 'Kabupaten', 'Agam', '26411', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '13', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Alor', '85811', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '14', '19', 'Maluku', 'Kota', 'Ambon', '97222', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '15', '34', 'Sumatera Utara', 'Kabupaten', 'Asahan', '21214', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '16', '24', 'Papua', 'Kabupaten', 'Asmat', '99777', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '17', '1', 'Bali', 'Kabupaten', 'Badung', '80351', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '18', '13', 'Kalimantan Selatan', 'Kabupaten', 'Balangan', '71611', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '19', '15', 'Kalimantan Timur', 'Kota', 'Balikpapan', '76111', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '20', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Banda Aceh', '23238', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '21', '18', 'Lampung', 'Kota', 'Bandar Lampung', '35139', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '22', '9', 'Jawa Barat', 'Kabupaten', 'Bandung', '40311', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '23', '9', 'Jawa Barat', 'Kota', 'Bandung', '40111', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '24', '9', 'Jawa Barat', 'Kabupaten', 'Bandung Barat', '40721', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '25', '29', 'Sulawesi Tengah', 'Kabupaten', 'Banggai', '94711', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '26', '29', 'Sulawesi Tengah', 'Kabupaten', 'Banggai Kepulauan', '94881', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '27', '2', 'Bangka Belitung', 'Kabupaten', 'Bangka', '33212', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '28', '2', 'Bangka Belitung', 'Kabupaten', 'Bangka Barat', '33315', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '29', '2', 'Bangka Belitung', 'Kabupaten', 'Bangka Selatan', '33719', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '30', '2', 'Bangka Belitung', 'Kabupaten', 'Bangka Tengah', '33613', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '31', '11', 'Jawa Timur', 'Kabupaten', 'Bangkalan', '69118', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '32', '1', 'Bali', 'Kabupaten', 'Bangli', '80619', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '33', '13', 'Kalimantan Selatan', 'Kabupaten', 'Banjar', '70619', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '34', '9', 'Jawa Barat', 'Kota', 'Banjar', '46311', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '35', '13', 'Kalimantan Selatan', 'Kota', 'Banjarbaru', '70712', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '36', '13', 'Kalimantan Selatan', 'Kota', 'Banjarmasin', '70117', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '37', '10', 'Jawa Tengah', 'Kabupaten', 'Banjarnegara', '53419', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '38', '28', 'Sulawesi Selatan', 'Kabupaten', 'Bantaeng', '92411', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '39', '5', 'DI Yogyakarta', 'Kabupaten', 'Bantul', '55715', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '40', '33', 'Sumatera Selatan', 'Kabupaten', 'Banyuasin', '30911', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '41', '10', 'Jawa Tengah', 'Kabupaten', 'Banyumas', '53114', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '42', '11', 'Jawa Timur', 'Kabupaten', 'Banyuwangi', '68416', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '43', '13', 'Kalimantan Selatan', 'Kabupaten', 'Barito Kuala', '70511', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '44', '14', 'Kalimantan Tengah', 'Kabupaten', 'Barito Selatan', '73711', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '45', '14', 'Kalimantan Tengah', 'Kabupaten', 'Barito Timur', '73671', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '46', '14', 'Kalimantan Tengah', 'Kabupaten', 'Barito Utara', '73881', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '47', '28', 'Sulawesi Selatan', 'Kabupaten', 'Barru', '90719', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '48', '17', 'Kepulauan Riau', 'Kota', 'Batam', '29413', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '49', '10', 'Jawa Tengah', 'Kabupaten', 'Batang', '51211', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '50', '8', 'Jambi', 'Kabupaten', 'Batang Hari', '36613', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '51', '11', 'Jawa Timur', 'Kota', 'Batu', '65311', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '52', '34', 'Sumatera Utara', 'Kabupaten', 'Batu Bara', '21655', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '53', '30', 'Sulawesi Tenggara', 'Kota', 'Bau-Bau', '93719', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '54', '9', 'Jawa Barat', 'Kabupaten', 'Bekasi', '17837', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '55', '9', 'Jawa Barat', 'Kota', 'Bekasi', '17121', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '56', '2', 'Bangka Belitung', 'Kabupaten', 'Belitung', '33419', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '57', '2', 'Bangka Belitung', 'Kabupaten', 'Belitung Timur', '33519', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '58', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Belu', '85711', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '59', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Bener Meriah', '24581', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '60', '26', 'Riau', 'Kabupaten', 'Bengkalis', '28719', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '61', '12', 'Kalimantan Barat', 'Kabupaten', 'Bengkayang', '79213', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '62', '4', 'Bengkulu', 'Kota', 'Bengkulu', '38229', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '63', '4', 'Bengkulu', 'Kabupaten', 'Bengkulu Selatan', '38519', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '64', '4', 'Bengkulu', 'Kabupaten', 'Bengkulu Tengah', '38319', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '65', '4', 'Bengkulu', 'Kabupaten', 'Bengkulu Utara', '38619', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '66', '15', 'Kalimantan Timur', 'Kabupaten', 'Berau', '77311', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '67', '24', 'Papua', 'Kabupaten', 'Biak Numfor', '98119', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '68', '22', 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Bima', '84171', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '69', '22', 'Nusa Tenggara Barat (NTB)', 'Kota', 'Bima', '84139', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '70', '34', 'Sumatera Utara', 'Kota', 'Binjai', '20712', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '71', '17', 'Kepulauan Riau', 'Kabupaten', 'Bintan', '29135', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '72', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Bireuen', '24219', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '73', '31', 'Sulawesi Utara', 'Kota', 'Bitung', '95512', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '74', '11', 'Jawa Timur', 'Kabupaten', 'Blitar', '66171', '2020-04-28 21:31:15', '2020-04-28 21:31:15' ),
( '75', '11', 'Jawa Timur', 'Kota', 'Blitar', '66124', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '76', '10', 'Jawa Tengah', 'Kabupaten', 'Blora', '58219', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '77', '7', 'Gorontalo', 'Kabupaten', 'Boalemo', '96319', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '78', '9', 'Jawa Barat', 'Kabupaten', 'Bogor', '16911', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '79', '9', 'Jawa Barat', 'Kota', 'Bogor', '16119', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '80', '11', 'Jawa Timur', 'Kabupaten', 'Bojonegoro', '62119', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '81', '31', 'Sulawesi Utara', 'Kabupaten', 'Bolaang Mongondow (Bolmong)', '95755', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '82', '31', 'Sulawesi Utara', 'Kabupaten', 'Bolaang Mongondow Selatan', '95774', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '83', '31', 'Sulawesi Utara', 'Kabupaten', 'Bolaang Mongondow Timur', '95783', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '84', '31', 'Sulawesi Utara', 'Kabupaten', 'Bolaang Mongondow Utara', '95765', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '85', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Bombana', '93771', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '86', '11', 'Jawa Timur', 'Kabupaten', 'Bondowoso', '68219', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '87', '28', 'Sulawesi Selatan', 'Kabupaten', 'Bone', '92713', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '88', '7', 'Gorontalo', 'Kabupaten', 'Bone Bolango', '96511', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '89', '15', 'Kalimantan Timur', 'Kota', 'Bontang', '75313', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '90', '24', 'Papua', 'Kabupaten', 'Boven Digoel', '99662', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '91', '10', 'Jawa Tengah', 'Kabupaten', 'Boyolali', '57312', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '92', '10', 'Jawa Tengah', 'Kabupaten', 'Brebes', '52212', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '93', '32', 'Sumatera Barat', 'Kota', 'Bukittinggi', '26115', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '94', '1', 'Bali', 'Kabupaten', 'Buleleng', '81111', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '95', '28', 'Sulawesi Selatan', 'Kabupaten', 'Bulukumba', '92511', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '96', '16', 'Kalimantan Utara', 'Kabupaten', 'Bulungan (Bulongan)', '77211', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '97', '8', 'Jambi', 'Kabupaten', 'Bungo', '37216', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '98', '29', 'Sulawesi Tengah', 'Kabupaten', 'Buol', '94564', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '99', '19', 'Maluku', 'Kabupaten', 'Buru', '97371', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '100', '19', 'Maluku', 'Kabupaten', 'Buru Selatan', '97351', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '101', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Buton', '93754', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '102', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Buton Utara', '93745', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '103', '9', 'Jawa Barat', 'Kabupaten', 'Ciamis', '46211', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '104', '9', 'Jawa Barat', 'Kabupaten', 'Cianjur', '43217', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '105', '10', 'Jawa Tengah', 'Kabupaten', 'Cilacap', '53211', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '106', '3', 'Banten', 'Kota', 'Cilegon', '42417', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '107', '9', 'Jawa Barat', 'Kota', 'Cimahi', '40512', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '108', '9', 'Jawa Barat', 'Kabupaten', 'Cirebon', '45611', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '109', '9', 'Jawa Barat', 'Kota', 'Cirebon', '45116', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '110', '34', 'Sumatera Utara', 'Kabupaten', 'Dairi', '22211', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '111', '24', 'Papua', 'Kabupaten', 'Deiyai (Deliyai)', '98784', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '112', '34', 'Sumatera Utara', 'Kabupaten', 'Deli Serdang', '20511', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '113', '10', 'Jawa Tengah', 'Kabupaten', 'Demak', '59519', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '114', '1', 'Bali', 'Kota', 'Denpasar', '80227', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '115', '9', 'Jawa Barat', 'Kota', 'Depok', '16416', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '116', '32', 'Sumatera Barat', 'Kabupaten', 'Dharmasraya', '27612', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '117', '24', 'Papua', 'Kabupaten', 'Dogiyai', '98866', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '118', '22', 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Dompu', '84217', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '119', '29', 'Sulawesi Tengah', 'Kabupaten', 'Donggala', '94341', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '120', '26', 'Riau', 'Kota', 'Dumai', '28811', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '121', '33', 'Sumatera Selatan', 'Kabupaten', 'Empat Lawang', '31811', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '122', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Ende', '86351', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '123', '28', 'Sulawesi Selatan', 'Kabupaten', 'Enrekang', '91719', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '124', '25', 'Papua Barat', 'Kabupaten', 'Fakfak', '98651', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '125', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Flores Timur', '86213', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '126', '9', 'Jawa Barat', 'Kabupaten', 'Garut', '44126', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '127', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Gayo Lues', '24653', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '128', '1', 'Bali', 'Kabupaten', 'Gianyar', '80519', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '129', '7', 'Gorontalo', 'Kabupaten', 'Gorontalo', '96218', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '130', '7', 'Gorontalo', 'Kota', 'Gorontalo', '96115', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '131', '7', 'Gorontalo', 'Kabupaten', 'Gorontalo Utara', '96611', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '132', '28', 'Sulawesi Selatan', 'Kabupaten', 'Gowa', '92111', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '133', '11', 'Jawa Timur', 'Kabupaten', 'Gresik', '61115', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '134', '10', 'Jawa Tengah', 'Kabupaten', 'Grobogan', '58111', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '135', '5', 'DI Yogyakarta', 'Kabupaten', 'Gunung Kidul', '55812', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '136', '14', 'Kalimantan Tengah', 'Kabupaten', 'Gunung Mas', '74511', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '137', '34', 'Sumatera Utara', 'Kota', 'Gunungsitoli', '22813', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '138', '20', 'Maluku Utara', 'Kabupaten', 'Halmahera Barat', '97757', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '139', '20', 'Maluku Utara', 'Kabupaten', 'Halmahera Selatan', '97911', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '140', '20', 'Maluku Utara', 'Kabupaten', 'Halmahera Tengah', '97853', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '141', '20', 'Maluku Utara', 'Kabupaten', 'Halmahera Timur', '97862', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '142', '20', 'Maluku Utara', 'Kabupaten', 'Halmahera Utara', '97762', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '143', '13', 'Kalimantan Selatan', 'Kabupaten', 'Hulu Sungai Selatan', '71212', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '144', '13', 'Kalimantan Selatan', 'Kabupaten', 'Hulu Sungai Tengah', '71313', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '145', '13', 'Kalimantan Selatan', 'Kabupaten', 'Hulu Sungai Utara', '71419', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '146', '34', 'Sumatera Utara', 'Kabupaten', 'Humbang Hasundutan', '22457', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '147', '26', 'Riau', 'Kabupaten', 'Indragiri Hilir', '29212', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '148', '26', 'Riau', 'Kabupaten', 'Indragiri Hulu', '29319', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '149', '9', 'Jawa Barat', 'Kabupaten', 'Indramayu', '45214', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '150', '24', 'Papua', 'Kabupaten', 'Intan Jaya', '98771', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '151', '6', 'DKI Jakarta', 'Kota', 'Jakarta Barat', '11220', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '152', '6', 'DKI Jakarta', 'Kota', 'Jakarta Pusat', '10540', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '153', '6', 'DKI Jakarta', 'Kota', 'Jakarta Selatan', '12230', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '154', '6', 'DKI Jakarta', 'Kota', 'Jakarta Timur', '13330', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '155', '6', 'DKI Jakarta', 'Kota', 'Jakarta Utara', '14140', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '156', '8', 'Jambi', 'Kota', 'Jambi', '36111', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '157', '24', 'Papua', 'Kabupaten', 'Jayapura', '99352', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '158', '24', 'Papua', 'Kota', 'Jayapura', '99114', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '159', '24', 'Papua', 'Kabupaten', 'Jayawijaya', '99511', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '160', '11', 'Jawa Timur', 'Kabupaten', 'Jember', '68113', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '161', '1', 'Bali', 'Kabupaten', 'Jembrana', '82251', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '162', '28', 'Sulawesi Selatan', 'Kabupaten', 'Jeneponto', '92319', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '163', '10', 'Jawa Tengah', 'Kabupaten', 'Jepara', '59419', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '164', '11', 'Jawa Timur', 'Kabupaten', 'Jombang', '61415', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '165', '25', 'Papua Barat', 'Kabupaten', 'Kaimana', '98671', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '166', '26', 'Riau', 'Kabupaten', 'Kampar', '28411', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '167', '14', 'Kalimantan Tengah', 'Kabupaten', 'Kapuas', '73583', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '168', '12', 'Kalimantan Barat', 'Kabupaten', 'Kapuas Hulu', '78719', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '169', '10', 'Jawa Tengah', 'Kabupaten', 'Karanganyar', '57718', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '170', '1', 'Bali', 'Kabupaten', 'Karangasem', '80819', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '171', '9', 'Jawa Barat', 'Kabupaten', 'Karawang', '41311', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '172', '17', 'Kepulauan Riau', 'Kabupaten', 'Karimun', '29611', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '173', '34', 'Sumatera Utara', 'Kabupaten', 'Karo', '22119', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '174', '14', 'Kalimantan Tengah', 'Kabupaten', 'Katingan', '74411', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '175', '4', 'Bengkulu', 'Kabupaten', 'Kaur', '38911', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '176', '12', 'Kalimantan Barat', 'Kabupaten', 'Kayong Utara', '78852', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '177', '10', 'Jawa Tengah', 'Kabupaten', 'Kebumen', '54319', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '178', '11', 'Jawa Timur', 'Kabupaten', 'Kediri', '64184', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '179', '11', 'Jawa Timur', 'Kota', 'Kediri', '64125', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '180', '24', 'Papua', 'Kabupaten', 'Keerom', '99461', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '181', '10', 'Jawa Tengah', 'Kabupaten', 'Kendal', '51314', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '182', '30', 'Sulawesi Tenggara', 'Kota', 'Kendari', '93126', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '183', '4', 'Bengkulu', 'Kabupaten', 'Kepahiang', '39319', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '184', '17', 'Kepulauan Riau', 'Kabupaten', 'Kepulauan Anambas', '29991', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '185', '19', 'Maluku', 'Kabupaten', 'Kepulauan Aru', '97681', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '186', '32', 'Sumatera Barat', 'Kabupaten', 'Kepulauan Mentawai', '25771', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '187', '26', 'Riau', 'Kabupaten', 'Kepulauan Meranti', '28791', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '188', '31', 'Sulawesi Utara', 'Kabupaten', 'Kepulauan Sangihe', '95819', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '189', '6', 'DKI Jakarta', 'Kabupaten', 'Kepulauan Seribu', '14550', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '190', '31', 'Sulawesi Utara', 'Kabupaten', 'Kepulauan Siau Tagulandang Biaro (Sitaro)', '95862', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '191', '20', 'Maluku Utara', 'Kabupaten', 'Kepulauan Sula', '97995', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '192', '31', 'Sulawesi Utara', 'Kabupaten', 'Kepulauan Talaud', '95885', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '193', '24', 'Papua', 'Kabupaten', 'Kepulauan Yapen (Yapen Waropen)', '98211', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '194', '8', 'Jambi', 'Kabupaten', 'Kerinci', '37167', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '195', '12', 'Kalimantan Barat', 'Kabupaten', 'Ketapang', '78874', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '196', '10', 'Jawa Tengah', 'Kabupaten', 'Klaten', '57411', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '197', '1', 'Bali', 'Kabupaten', 'Klungkung', '80719', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '198', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Kolaka', '93511', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '199', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Kolaka Utara', '93911', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '200', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Konawe', '93411', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '201', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Konawe Selatan', '93811', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '202', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Konawe Utara', '93311', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '203', '13', 'Kalimantan Selatan', 'Kabupaten', 'Kotabaru', '72119', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '204', '31', 'Sulawesi Utara', 'Kota', 'Kotamobagu', '95711', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '205', '14', 'Kalimantan Tengah', 'Kabupaten', 'Kotawaringin Barat', '74119', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '206', '14', 'Kalimantan Tengah', 'Kabupaten', 'Kotawaringin Timur', '74364', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '207', '26', 'Riau', 'Kabupaten', 'Kuantan Singingi', '29519', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '208', '12', 'Kalimantan Barat', 'Kabupaten', 'Kubu Raya', '78311', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '209', '10', 'Jawa Tengah', 'Kabupaten', 'Kudus', '59311', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '210', '5', 'DI Yogyakarta', 'Kabupaten', 'Kulon Progo', '55611', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '211', '9', 'Jawa Barat', 'Kabupaten', 'Kuningan', '45511', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '212', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Kupang', '85362', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '213', '23', 'Nusa Tenggara Timur (NTT)', 'Kota', 'Kupang', '85119', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '214', '15', 'Kalimantan Timur', 'Kabupaten', 'Kutai Barat', '75711', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '215', '15', 'Kalimantan Timur', 'Kabupaten', 'Kutai Kartanegara', '75511', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '216', '15', 'Kalimantan Timur', 'Kabupaten', 'Kutai Timur', '75611', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '217', '34', 'Sumatera Utara', 'Kabupaten', 'Labuhan Batu', '21412', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '218', '34', 'Sumatera Utara', 'Kabupaten', 'Labuhan Batu Selatan', '21511', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '219', '34', 'Sumatera Utara', 'Kabupaten', 'Labuhan Batu Utara', '21711', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '220', '33', 'Sumatera Selatan', 'Kabupaten', 'Lahat', '31419', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '221', '14', 'Kalimantan Tengah', 'Kabupaten', 'Lamandau', '74611', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '222', '11', 'Jawa Timur', 'Kabupaten', 'Lamongan', '64125', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '223', '18', 'Lampung', 'Kabupaten', 'Lampung Barat', '34814', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '224', '18', 'Lampung', 'Kabupaten', 'Lampung Selatan', '35511', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '225', '18', 'Lampung', 'Kabupaten', 'Lampung Tengah', '34212', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '226', '18', 'Lampung', 'Kabupaten', 'Lampung Timur', '34319', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '227', '18', 'Lampung', 'Kabupaten', 'Lampung Utara', '34516', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '228', '12', 'Kalimantan Barat', 'Kabupaten', 'Landak', '78319', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '229', '34', 'Sumatera Utara', 'Kabupaten', 'Langkat', '20811', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '230', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Langsa', '24412', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '231', '24', 'Papua', 'Kabupaten', 'Lanny Jaya', '99531', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '232', '3', 'Banten', 'Kabupaten', 'Lebak', '42319', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '233', '4', 'Bengkulu', 'Kabupaten', 'Lebong', '39264', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '234', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Lembata', '86611', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '235', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Lhokseumawe', '24352', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '236', '32', 'Sumatera Barat', 'Kabupaten', 'Lima Puluh Koto/Kota', '26671', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '237', '17', 'Kepulauan Riau', 'Kabupaten', 'Lingga', '29811', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '238', '22', 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Lombok Barat', '83311', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '239', '22', 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Lombok Tengah', '83511', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '240', '22', 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Lombok Timur', '83612', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '241', '22', 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Lombok Utara', '83711', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '242', '33', 'Sumatera Selatan', 'Kota', 'Lubuk Linggau', '31614', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '243', '11', 'Jawa Timur', 'Kabupaten', 'Lumajang', '67319', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '244', '28', 'Sulawesi Selatan', 'Kabupaten', 'Luwu', '91994', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '245', '28', 'Sulawesi Selatan', 'Kabupaten', 'Luwu Timur', '92981', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '246', '28', 'Sulawesi Selatan', 'Kabupaten', 'Luwu Utara', '92911', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '247', '11', 'Jawa Timur', 'Kabupaten', 'Madiun', '63153', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '248', '11', 'Jawa Timur', 'Kota', 'Madiun', '63122', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '249', '10', 'Jawa Tengah', 'Kabupaten', 'Magelang', '56519', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '250', '10', 'Jawa Tengah', 'Kota', 'Magelang', '56133', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '251', '11', 'Jawa Timur', 'Kabupaten', 'Magetan', '63314', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '252', '9', 'Jawa Barat', 'Kabupaten', 'Majalengka', '45412', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '253', '27', 'Sulawesi Barat', 'Kabupaten', 'Majene', '91411', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '254', '28', 'Sulawesi Selatan', 'Kota', 'Makassar', '90111', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '255', '11', 'Jawa Timur', 'Kabupaten', 'Malang', '65163', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '256', '11', 'Jawa Timur', 'Kota', 'Malang', '65112', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '257', '16', 'Kalimantan Utara', 'Kabupaten', 'Malinau', '77511', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '258', '19', 'Maluku', 'Kabupaten', 'Maluku Barat Daya', '97451', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '259', '19', 'Maluku', 'Kabupaten', 'Maluku Tengah', '97513', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '260', '19', 'Maluku', 'Kabupaten', 'Maluku Tenggara', '97651', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '261', '19', 'Maluku', 'Kabupaten', 'Maluku Tenggara Barat', '97465', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '262', '27', 'Sulawesi Barat', 'Kabupaten', 'Mamasa', '91362', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '263', '24', 'Papua', 'Kabupaten', 'Mamberamo Raya', '99381', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '264', '24', 'Papua', 'Kabupaten', 'Mamberamo Tengah', '99553', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '265', '27', 'Sulawesi Barat', 'Kabupaten', 'Mamuju', '91519', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '266', '27', 'Sulawesi Barat', 'Kabupaten', 'Mamuju Utara', '91571', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '267', '31', 'Sulawesi Utara', 'Kota', 'Manado', '95247', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '268', '34', 'Sumatera Utara', 'Kabupaten', 'Mandailing Natal', '22916', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '269', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Manggarai', '86551', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '270', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Manggarai Barat', '86711', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '271', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Manggarai Timur', '86811', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '272', '25', 'Papua Barat', 'Kabupaten', 'Manokwari', '98311', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '273', '25', 'Papua Barat', 'Kabupaten', 'Manokwari Selatan', '98355', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '274', '24', 'Papua', 'Kabupaten', 'Mappi', '99853', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '275', '28', 'Sulawesi Selatan', 'Kabupaten', 'Maros', '90511', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '276', '22', 'Nusa Tenggara Barat (NTB)', 'Kota', 'Mataram', '83131', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '277', '25', 'Papua Barat', 'Kabupaten', 'Maybrat', '98051', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '278', '34', 'Sumatera Utara', 'Kota', 'Medan', '20228', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '279', '12', 'Kalimantan Barat', 'Kabupaten', 'Melawi', '78619', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '280', '8', 'Jambi', 'Kabupaten', 'Merangin', '37319', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '281', '24', 'Papua', 'Kabupaten', 'Merauke', '99613', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '282', '18', 'Lampung', 'Kabupaten', 'Mesuji', '34911', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '283', '18', 'Lampung', 'Kota', 'Metro', '34111', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '284', '24', 'Papua', 'Kabupaten', 'Mimika', '99962', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '285', '31', 'Sulawesi Utara', 'Kabupaten', 'Minahasa', '95614', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '286', '31', 'Sulawesi Utara', 'Kabupaten', 'Minahasa Selatan', '95914', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '287', '31', 'Sulawesi Utara', 'Kabupaten', 'Minahasa Tenggara', '95995', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '288', '31', 'Sulawesi Utara', 'Kabupaten', 'Minahasa Utara', '95316', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '289', '11', 'Jawa Timur', 'Kabupaten', 'Mojokerto', '61382', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '290', '11', 'Jawa Timur', 'Kota', 'Mojokerto', '61316', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '291', '29', 'Sulawesi Tengah', 'Kabupaten', 'Morowali', '94911', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '292', '33', 'Sumatera Selatan', 'Kabupaten', 'Muara Enim', '31315', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '293', '8', 'Jambi', 'Kabupaten', 'Muaro Jambi', '36311', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '294', '4', 'Bengkulu', 'Kabupaten', 'Muko Muko', '38715', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '295', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Muna', '93611', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '296', '14', 'Kalimantan Tengah', 'Kabupaten', 'Murung Raya', '73911', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '297', '33', 'Sumatera Selatan', 'Kabupaten', 'Musi Banyuasin', '30719', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '298', '33', 'Sumatera Selatan', 'Kabupaten', 'Musi Rawas', '31661', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '299', '24', 'Papua', 'Kabupaten', 'Nabire', '98816', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '300', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Nagan Raya', '23674', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '301', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Nagekeo', '86911', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '302', '17', 'Kepulauan Riau', 'Kabupaten', 'Natuna', '29711', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '303', '24', 'Papua', 'Kabupaten', 'Nduga', '99541', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '304', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Ngada', '86413', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '305', '11', 'Jawa Timur', 'Kabupaten', 'Nganjuk', '64414', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '306', '11', 'Jawa Timur', 'Kabupaten', 'Ngawi', '63219', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '307', '34', 'Sumatera Utara', 'Kabupaten', 'Nias', '22876', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '308', '34', 'Sumatera Utara', 'Kabupaten', 'Nias Barat', '22895', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '309', '34', 'Sumatera Utara', 'Kabupaten', 'Nias Selatan', '22865', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '310', '34', 'Sumatera Utara', 'Kabupaten', 'Nias Utara', '22856', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '311', '16', 'Kalimantan Utara', 'Kabupaten', 'Nunukan', '77421', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '312', '33', 'Sumatera Selatan', 'Kabupaten', 'Ogan Ilir', '30811', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '313', '33', 'Sumatera Selatan', 'Kabupaten', 'Ogan Komering Ilir', '30618', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '314', '33', 'Sumatera Selatan', 'Kabupaten', 'Ogan Komering Ulu', '32112', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '315', '33', 'Sumatera Selatan', 'Kabupaten', 'Ogan Komering Ulu Selatan', '32211', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '316', '33', 'Sumatera Selatan', 'Kabupaten', 'Ogan Komering Ulu Timur', '32312', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '317', '11', 'Jawa Timur', 'Kabupaten', 'Pacitan', '63512', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '318', '32', 'Sumatera Barat', 'Kota', 'Padang', '25112', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '319', '34', 'Sumatera Utara', 'Kabupaten', 'Padang Lawas', '22763', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '320', '34', 'Sumatera Utara', 'Kabupaten', 'Padang Lawas Utara', '22753', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '321', '32', 'Sumatera Barat', 'Kota', 'Padang Panjang', '27122', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '322', '32', 'Sumatera Barat', 'Kabupaten', 'Padang Pariaman', '25583', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '323', '34', 'Sumatera Utara', 'Kota', 'Padang Sidempuan', '22727', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '324', '33', 'Sumatera Selatan', 'Kota', 'Pagar Alam', '31512', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '325', '34', 'Sumatera Utara', 'Kabupaten', 'Pakpak Bharat', '22272', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '326', '14', 'Kalimantan Tengah', 'Kota', 'Palangka Raya', '73112', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '327', '33', 'Sumatera Selatan', 'Kota', 'Palembang', '30111', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '328', '28', 'Sulawesi Selatan', 'Kota', 'Palopo', '91911', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '329', '29', 'Sulawesi Tengah', 'Kota', 'Palu', '94111', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '330', '11', 'Jawa Timur', 'Kabupaten', 'Pamekasan', '69319', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '331', '3', 'Banten', 'Kabupaten', 'Pandeglang', '42212', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '332', '9', 'Jawa Barat', 'Kabupaten', 'Pangandaran', '46511', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '333', '28', 'Sulawesi Selatan', 'Kabupaten', 'Pangkajene Kepulauan', '90611', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '334', '2', 'Bangka Belitung', 'Kota', 'Pangkal Pinang', '33115', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '335', '24', 'Papua', 'Kabupaten', 'Paniai', '98765', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '336', '28', 'Sulawesi Selatan', 'Kota', 'Parepare', '91123', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '337', '32', 'Sumatera Barat', 'Kota', 'Pariaman', '25511', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '338', '29', 'Sulawesi Tengah', 'Kabupaten', 'Parigi Moutong', '94411', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '339', '32', 'Sumatera Barat', 'Kabupaten', 'Pasaman', '26318', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '340', '32', 'Sumatera Barat', 'Kabupaten', 'Pasaman Barat', '26511', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '341', '15', 'Kalimantan Timur', 'Kabupaten', 'Paser', '76211', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '342', '11', 'Jawa Timur', 'Kabupaten', 'Pasuruan', '67153', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '343', '11', 'Jawa Timur', 'Kota', 'Pasuruan', '67118', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '344', '10', 'Jawa Tengah', 'Kabupaten', 'Pati', '59114', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '345', '32', 'Sumatera Barat', 'Kota', 'Payakumbuh', '26213', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '346', '25', 'Papua Barat', 'Kabupaten', 'Pegunungan Arfak', '98354', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '347', '24', 'Papua', 'Kabupaten', 'Pegunungan Bintang', '99573', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '348', '10', 'Jawa Tengah', 'Kabupaten', 'Pekalongan', '51161', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '349', '10', 'Jawa Tengah', 'Kota', 'Pekalongan', '51122', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '350', '26', 'Riau', 'Kota', 'Pekanbaru', '28112', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '351', '26', 'Riau', 'Kabupaten', 'Pelalawan', '28311', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '352', '10', 'Jawa Tengah', 'Kabupaten', 'Pemalang', '52319', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '353', '34', 'Sumatera Utara', 'Kota', 'Pematang Siantar', '21126', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '354', '15', 'Kalimantan Timur', 'Kabupaten', 'Penajam Paser Utara', '76311', '2020-04-28 21:31:16', '2020-04-28 21:31:16' ),
( '355', '18', 'Lampung', 'Kabupaten', 'Pesawaran', '35312', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '356', '18', 'Lampung', 'Kabupaten', 'Pesisir Barat', '35974', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '357', '32', 'Sumatera Barat', 'Kabupaten', 'Pesisir Selatan', '25611', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '358', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Pidie', '24116', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '359', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Pidie Jaya', '24186', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '360', '28', 'Sulawesi Selatan', 'Kabupaten', 'Pinrang', '91251', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '361', '7', 'Gorontalo', 'Kabupaten', 'Pohuwato', '96419', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '362', '27', 'Sulawesi Barat', 'Kabupaten', 'Polewali Mandar', '91311', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '363', '11', 'Jawa Timur', 'Kabupaten', 'Ponorogo', '63411', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '364', '12', 'Kalimantan Barat', 'Kabupaten', 'Pontianak', '78971', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '365', '12', 'Kalimantan Barat', 'Kota', 'Pontianak', '78112', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '366', '29', 'Sulawesi Tengah', 'Kabupaten', 'Poso', '94615', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '367', '33', 'Sumatera Selatan', 'Kota', 'Prabumulih', '31121', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '368', '18', 'Lampung', 'Kabupaten', 'Pringsewu', '35719', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '369', '11', 'Jawa Timur', 'Kabupaten', 'Probolinggo', '67282', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '370', '11', 'Jawa Timur', 'Kota', 'Probolinggo', '67215', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '371', '14', 'Kalimantan Tengah', 'Kabupaten', 'Pulang Pisau', '74811', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '372', '20', 'Maluku Utara', 'Kabupaten', 'Pulau Morotai', '97771', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '373', '24', 'Papua', 'Kabupaten', 'Puncak', '98981', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '374', '24', 'Papua', 'Kabupaten', 'Puncak Jaya', '98979', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '375', '10', 'Jawa Tengah', 'Kabupaten', 'Purbalingga', '53312', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '376', '9', 'Jawa Barat', 'Kabupaten', 'Purwakarta', '41119', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '377', '10', 'Jawa Tengah', 'Kabupaten', 'Purworejo', '54111', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '378', '25', 'Papua Barat', 'Kabupaten', 'Raja Ampat', '98489', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '379', '4', 'Bengkulu', 'Kabupaten', 'Rejang Lebong', '39112', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '380', '10', 'Jawa Tengah', 'Kabupaten', 'Rembang', '59219', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '381', '26', 'Riau', 'Kabupaten', 'Rokan Hilir', '28992', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '382', '26', 'Riau', 'Kabupaten', 'Rokan Hulu', '28511', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '383', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Rote Ndao', '85982', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '384', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Sabang', '23512', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '385', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sabu Raijua', '85391', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '386', '10', 'Jawa Tengah', 'Kota', 'Salatiga', '50711', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '387', '15', 'Kalimantan Timur', 'Kota', 'Samarinda', '75133', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '388', '12', 'Kalimantan Barat', 'Kabupaten', 'Sambas', '79453', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '389', '34', 'Sumatera Utara', 'Kabupaten', 'Samosir', '22392', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '390', '11', 'Jawa Timur', 'Kabupaten', 'Sampang', '69219', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '391', '12', 'Kalimantan Barat', 'Kabupaten', 'Sanggau', '78557', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '392', '24', 'Papua', 'Kabupaten', 'Sarmi', '99373', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '393', '8', 'Jambi', 'Kabupaten', 'Sarolangun', '37419', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '394', '32', 'Sumatera Barat', 'Kota', 'Sawah Lunto', '27416', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '395', '12', 'Kalimantan Barat', 'Kabupaten', 'Sekadau', '79583', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '396', '28', 'Sulawesi Selatan', 'Kabupaten', 'Selayar (Kepulauan Selayar)', '92812', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '397', '4', 'Bengkulu', 'Kabupaten', 'Seluma', '38811', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '398', '10', 'Jawa Tengah', 'Kabupaten', 'Semarang', '50511', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '399', '10', 'Jawa Tengah', 'Kota', 'Semarang', '50135', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '400', '19', 'Maluku', 'Kabupaten', 'Seram Bagian Barat', '97561', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '401', '19', 'Maluku', 'Kabupaten', 'Seram Bagian Timur', '97581', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '402', '3', 'Banten', 'Kabupaten', 'Serang', '42182', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '403', '3', 'Banten', 'Kota', 'Serang', '42111', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '404', '34', 'Sumatera Utara', 'Kabupaten', 'Serdang Bedagai', '20915', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '405', '14', 'Kalimantan Tengah', 'Kabupaten', 'Seruyan', '74211', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '406', '26', 'Riau', 'Kabupaten', 'Siak', '28623', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '407', '34', 'Sumatera Utara', 'Kota', 'Sibolga', '22522', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '408', '28', 'Sulawesi Selatan', 'Kabupaten', 'Sidenreng Rappang/Rapang', '91613', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '409', '11', 'Jawa Timur', 'Kabupaten', 'Sidoarjo', '61219', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '410', '29', 'Sulawesi Tengah', 'Kabupaten', 'Sigi', '94364', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '411', '32', 'Sumatera Barat', 'Kabupaten', 'Sijunjung (Sawah Lunto Sijunjung)', '27511', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '412', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sikka', '86121', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '413', '34', 'Sumatera Utara', 'Kabupaten', 'Simalungun', '21162', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '414', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Simeulue', '23891', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '415', '12', 'Kalimantan Barat', 'Kota', 'Singkawang', '79117', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '416', '28', 'Sulawesi Selatan', 'Kabupaten', 'Sinjai', '92615', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '417', '12', 'Kalimantan Barat', 'Kabupaten', 'Sintang', '78619', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '418', '11', 'Jawa Timur', 'Kabupaten', 'Situbondo', '68316', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '419', '5', 'DI Yogyakarta', 'Kabupaten', 'Sleman', '55513', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '420', '32', 'Sumatera Barat', 'Kabupaten', 'Solok', '27365', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '421', '32', 'Sumatera Barat', 'Kota', 'Solok', '27315', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '422', '32', 'Sumatera Barat', 'Kabupaten', 'Solok Selatan', '27779', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '423', '28', 'Sulawesi Selatan', 'Kabupaten', 'Soppeng', '90812', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '424', '25', 'Papua Barat', 'Kabupaten', 'Sorong', '98431', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '425', '25', 'Papua Barat', 'Kota', 'Sorong', '98411', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '426', '25', 'Papua Barat', 'Kabupaten', 'Sorong Selatan', '98454', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '427', '10', 'Jawa Tengah', 'Kabupaten', 'Sragen', '57211', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '428', '9', 'Jawa Barat', 'Kabupaten', 'Subang', '41215', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '429', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Subulussalam', '24882', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '430', '9', 'Jawa Barat', 'Kabupaten', 'Sukabumi', '43311', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '431', '9', 'Jawa Barat', 'Kota', 'Sukabumi', '43114', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '432', '14', 'Kalimantan Tengah', 'Kabupaten', 'Sukamara', '74712', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '433', '10', 'Jawa Tengah', 'Kabupaten', 'Sukoharjo', '57514', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '434', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sumba Barat', '87219', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '435', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sumba Barat Daya', '87453', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '436', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sumba Tengah', '87358', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '437', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sumba Timur', '87112', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '438', '22', 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Sumbawa', '84315', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '439', '22', 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Sumbawa Barat', '84419', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '440', '9', 'Jawa Barat', 'Kabupaten', 'Sumedang', '45326', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '441', '11', 'Jawa Timur', 'Kabupaten', 'Sumenep', '69413', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '442', '8', 'Jambi', 'Kota', 'Sungaipenuh', '37113', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '443', '24', 'Papua', 'Kabupaten', 'Supiori', '98164', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '444', '11', 'Jawa Timur', 'Kota', 'Surabaya', '60119', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '445', '10', 'Jawa Tengah', 'Kota', 'Surakarta (Solo)', '57113', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '446', '13', 'Kalimantan Selatan', 'Kabupaten', 'Tabalong', '71513', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '447', '1', 'Bali', 'Kabupaten', 'Tabanan', '82119', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '448', '28', 'Sulawesi Selatan', 'Kabupaten', 'Takalar', '92212', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '449', '25', 'Papua Barat', 'Kabupaten', 'Tambrauw', '98475', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '450', '16', 'Kalimantan Utara', 'Kabupaten', 'Tana Tidung', '77611', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '451', '28', 'Sulawesi Selatan', 'Kabupaten', 'Tana Toraja', '91819', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '452', '13', 'Kalimantan Selatan', 'Kabupaten', 'Tanah Bumbu', '72211', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '453', '32', 'Sumatera Barat', 'Kabupaten', 'Tanah Datar', '27211', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '454', '13', 'Kalimantan Selatan', 'Kabupaten', 'Tanah Laut', '70811', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '455', '3', 'Banten', 'Kabupaten', 'Tangerang', '15914', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '456', '3', 'Banten', 'Kota', 'Tangerang', '15111', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '457', '3', 'Banten', 'Kota', 'Tangerang Selatan', '15332', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '458', '18', 'Lampung', 'Kabupaten', 'Tanggamus', '35619', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '459', '34', 'Sumatera Utara', 'Kota', 'Tanjung Balai', '21321', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '460', '8', 'Jambi', 'Kabupaten', 'Tanjung Jabung Barat', '36513', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '461', '8', 'Jambi', 'Kabupaten', 'Tanjung Jabung Timur', '36719', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '462', '17', 'Kepulauan Riau', 'Kota', 'Tanjung Pinang', '29111', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '463', '34', 'Sumatera Utara', 'Kabupaten', 'Tapanuli Selatan', '22742', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '464', '34', 'Sumatera Utara', 'Kabupaten', 'Tapanuli Tengah', '22611', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '465', '34', 'Sumatera Utara', 'Kabupaten', 'Tapanuli Utara', '22414', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '466', '13', 'Kalimantan Selatan', 'Kabupaten', 'Tapin', '71119', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '467', '16', 'Kalimantan Utara', 'Kota', 'Tarakan', '77114', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '468', '9', 'Jawa Barat', 'Kabupaten', 'Tasikmalaya', '46411', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '469', '9', 'Jawa Barat', 'Kota', 'Tasikmalaya', '46116', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '470', '34', 'Sumatera Utara', 'Kota', 'Tebing Tinggi', '20632', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '471', '8', 'Jambi', 'Kabupaten', 'Tebo', '37519', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '472', '10', 'Jawa Tengah', 'Kabupaten', 'Tegal', '52419', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '473', '10', 'Jawa Tengah', 'Kota', 'Tegal', '52114', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '474', '25', 'Papua Barat', 'Kabupaten', 'Teluk Bintuni', '98551', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '475', '25', 'Papua Barat', 'Kabupaten', 'Teluk Wondama', '98591', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '476', '10', 'Jawa Tengah', 'Kabupaten', 'Temanggung', '56212', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '477', '20', 'Maluku Utara', 'Kota', 'Ternate', '97714', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '478', '20', 'Maluku Utara', 'Kota', 'Tidore Kepulauan', '97815', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '479', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Timor Tengah Selatan', '85562', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '480', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Timor Tengah Utara', '85612', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '481', '34', 'Sumatera Utara', 'Kabupaten', 'Toba Samosir', '22316', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '482', '29', 'Sulawesi Tengah', 'Kabupaten', 'Tojo Una-Una', '94683', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '483', '29', 'Sulawesi Tengah', 'Kabupaten', 'Toli-Toli', '94542', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '484', '24', 'Papua', 'Kabupaten', 'Tolikara', '99411', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '485', '31', 'Sulawesi Utara', 'Kota', 'Tomohon', '95416', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '486', '28', 'Sulawesi Selatan', 'Kabupaten', 'Toraja Utara', '91831', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '487', '11', 'Jawa Timur', 'Kabupaten', 'Trenggalek', '66312', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '488', '19', 'Maluku', 'Kota', 'Tual', '97612', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '489', '11', 'Jawa Timur', 'Kabupaten', 'Tuban', '62319', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '490', '18', 'Lampung', 'Kabupaten', 'Tulang Bawang', '34613', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '491', '18', 'Lampung', 'Kabupaten', 'Tulang Bawang Barat', '34419', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '492', '11', 'Jawa Timur', 'Kabupaten', 'Tulungagung', '66212', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '493', '28', 'Sulawesi Selatan', 'Kabupaten', 'Wajo', '90911', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '494', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Wakatobi', '93791', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '495', '24', 'Papua', 'Kabupaten', 'Waropen', '98269', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '496', '18', 'Lampung', 'Kabupaten', 'Way Kanan', '34711', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '497', '10', 'Jawa Tengah', 'Kabupaten', 'Wonogiri', '57619', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '498', '10', 'Jawa Tengah', 'Kabupaten', 'Wonosobo', '56311', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '499', '24', 'Papua', 'Kabupaten', 'Yahukimo', '99041', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '500', '24', 'Papua', 'Kabupaten', 'Yalimo', '99481', '2020-04-28 21:31:17', '2020-04-28 21:31:17' ),
( '501', '5', 'DI Yogyakarta', 'Kota', 'Yogyakarta', '55111', '2020-04-28 21:31:17', '2020-04-28 21:31:17' );
-- ---------------------------------------------------------


-- Dump data of "documents" --------------------------------
-- ---------------------------------------------------------


-- Dump data of "events" -----------------------------------
INSERT INTO `events`(`id`,`title`,`link`,`place`,`start_date`,`end_date`,`booth`,`image_url`,`created_at`,`updated_at`) VALUES 
( '1', 'Malang Book Fair 1', 'malang-book-fair-1', 'Malang', '2020-03-28', '2020-03-29', 'Stand VIP 4 (empat)', 'event_malang-book-fair-2020.jpg', '2020-03-28 03:15:48', '2020-04-02 14:36:30' ),
( '2', 'Islamic Book Fair 2020 : “Literasi Islam Cahaya Untuk Negeri“', 'islamic-book-fair-2020-:-“literasi-islam-cahaya-untuk-negeri“', 'Hall A-B, Jakarta Convention Center (JCC)', '2020-04-03', '2020-04-05', '100-104', 'event_islamic-book-fair-2020.jpg', '2020-04-02 14:38:32', '2020-04-02 14:38:32' );
-- ---------------------------------------------------------


-- Dump data of "failed_jobs" ------------------------------
-- ---------------------------------------------------------


-- Dump data of "messages" ---------------------------------
INSERT INTO `messages`(`id`,`name`,`email`,`message`,`created_at`,`updated_at`) VALUES 
( '1', 'test', 'test@test.com', 'test', '2020-04-10 14:59:36', '2020-04-10 14:59:36' );
-- ---------------------------------------------------------


-- Dump data of "migrations" -------------------------------
INSERT INTO `migrations`(`id`,`migration`,`batch`) VALUES 
( '1', '2014_10_12_000000_create_users_table', '1' ),
( '2', '2014_10_12_100000_create_password_resets_table', '1' ),
( '3', '2019_08_19_000000_create_failed_jobs_table', '1' ),
( '4', '2020_03_26_145610_create_categories', '2' ),
( '5', '2020_03_27_025112_create_books', '3' ),
( '6', '2020_03_27_025933_create_authors', '3' ),
( '7', '2020_03_27_030317_create_events', '3' ),
( '8', '2020_03_27_030638_create_messages', '3' ),
( '9', '2020_03_27_031108_create_orders', '3' ),
( '10', '2020_03_27_031546_create_order_details', '3' ),
( '11', '2020_03_27_031823_create_addresses', '3' ),
( '12', '2020_03_27_032755_create_promos', '3' ),
( '13', '2020_03_27_035439_create_scripts', '3' ),
( '14', '2020_03_27_035654_create_profiles', '3' ),
( '15', '2020_03_27_042411_create_promo_products', '4' ),
( '16', '2020_03_28_002738_create_ads', '5' ),
( '19', '2020_03_28_033306_create_news', '6' ),
( '21', '2020_03_28_094224_create_slider', '7' ),
( '26', '2020_03_30_130513_alter_books_add_link', '8' ),
( '27', '2020_03_30_131955_create_wishlists', '8' ),
( '28', '2020_04_02_003453_alter_promo_product_add_promo_id', '9' ),
( '29', '2020_04_02_142555_alter_ads_add_link', '10' ),
( '30', '2020_04_02_142606_alter_event_add_link', '10' ),
( '31', '2020_04_02_142615_alter_news_add_link', '10' ),
( '32', '2020_04_02_142624_alter_promo_add_link', '10' ),
( '33', '2020_04_10_133436_alter_categories_add_link', '11' ),
( '35', '2020_04_11_053502_create_catalogs', '12' ),
( '37', '2020_04_11_104402_create_documents', '13' ),
( '38', '2020_04_12_020817_alter_addresses_add_fields', '14' ),
( '39', '2020_04_15_030104_create_admins_table', '15' ),
( '40', '2020_04_20_040433_alter_books_add_status_stock', '16' ),
( '44', '2020_04_25_222418_alter_users_add_fields', '17' ),
( '45', '2020_04_28_211329_create_provinces', '18' ),
( '46', '2020_04_28_211424_create_cities', '18' ),
( '48', '2020_05_03_022803_alter_users_add_birthdate', '19' );
-- ---------------------------------------------------------


-- Dump data of "news" -------------------------------------
INSERT INTO `news`(`id`,`title`,`link`,`body`,`status`,`image_url`,`download_url`,`created_at`,`updated_at`) VALUES 
( '1', 'CARA MENADABURI AL-QUR\'AN', 'cara-menadaburi-al-quran', 'Segala puji bagi Allah yang telah memberikan kepada kami nikmat yang tak terhingga bilangannya. Salam dan shalawat bagi junjungan kami Nabi Muhammad Shallallahu Alaihi wa Sallam beserta segenap keluarga dan sahabat-sahabatnya, serta pengikutnya hingga akhir zaman.

Motto kami “PENERBIT BUKU ISLAM UTAMA” Kami berdiri untuk semua umat Islam, kami independen. Kami bukan milik suatu kelompok / golongan / aliran tertentu, dan tidak berafiliasi ke mana pun. Kami tidak disponsori juga tidak menjadi sponsor ataupun mewakili satu kelompok / golongan / aliran tertentu. Insya Allah kami berusaha komitmen memperjuangkan Izzul Islam Wal Muslimin, bersandar pada Kitabullah dan Sunnah Rasul. Kami adalah Ahlu Sunnah wal Jama’ah yang anti terhadap segala jenis bid’ah, kesesatan, dan penodaan agama maupun sikap berlebih-lebihan (ghuluw) dalam beragama.

Tema buku yang kami terbitkan pun sangat beragam. Ada akidah, tafsir, hadits, akhlak, sejarah Islam, Sirah Nabi, biografi sahabat dan para ulama, kisah-kisah Islami, pendidikan, ekonomi Islam, politik, kewanitaan dan keluarga, tips pengembangan diri, manajemen, maupun buku-buku pemikiran kontemporer.

Buku-buku kontemporer kami banyak menanggapi persoalan-persoalan yang dihadapi umat Islam Indonesia, terlebih dengan maraknya berbagai kelompok dakwah dan aliran keagamaan, serta paham menyimpang yang mengaku Islam. Sering kali pendapat kami tidak sejalan dengan aliran atau kelompok tersebut. Namun kritik yang kami sampaikan maupun melalui buku, insya Allah semata-mata didasari oleh semangat ilmu dan kebenaran, bukan semangat fanatisme kelompok. Insya Allah, dengan buku, kita akan kembangkan budaya kritis dan dialogis demi terwujudnya kemajuan dan kesadaran umat Islam.

Selain menerbitkan buku-buku terjemahan dari Timur Tengah, kami juga menerbitkan buku-buku karya penulis lokal. Saat ini, sudah sekitar 30-an penulis lokal yang buku karyanya kami terbitkan. Dan kurang lebih ada 60 orang penerjemah free lance yang setiap saat siap menerjemahkan naskah-naskah berbahasa Arab / Inggris yang kami sodorkan.

Dengan seleksi naskah yang cukup ketat, baik untuk karya penulis lokal maupun buku-buku terjemahan, insya Allah kami senantiasa berusaha untuk memberikan yang terbaik bagi pembaca setia buku-buku Pustaka Al-Kautsar.

Kini sudah lebih dari 500 judul buku berhasil diluncurkan dan sudah jutaan eksemplar buku-buku kami dibaca di seluruh penjuru tanah air, dari Sabang sampai Merauke. Bahkan hingga ke manca negara.', '1', 'news_cara-menadaburi-al-quran.jpg', 'news_Trust.txt', '2020-03-28 04:09:34', '2020-04-18 04:59:30' );
-- ---------------------------------------------------------


-- Dump data of "order_details" ----------------------------
INSERT INTO `order_details`(`id`,`order_id`,`product_id`,`price`,`qty`,`created_at`,`updated_at`) VALUES 
( '1', '3', '2', '130000', '1', '2020-04-29 04:04:02', '2020-04-29 04:04:02' ),
( '2', '3', '8', '234000', '1', '2020-04-29 04:04:02', '2020-04-29 04:04:02' ),
( '3', '4', '2', '130000', '1', '2020-04-29 04:05:59', '2020-04-29 04:05:59' ),
( '4', '4', '8', '234000', '1', '2020-04-29 04:05:59', '2020-04-29 04:05:59' );
-- ---------------------------------------------------------


-- Dump data of "orders" -----------------------------------
INSERT INTO `orders`(`id`,`code`,`user_id`,`sub_total`,`courier_budget`,`total`,`payment_status`,`status`,`created_at`,`updated_at`) VALUES 
( '1', '7F1A0E', '1', '100000', '20000', '120000', '1', '1', '2020-03-28 02:39:00', '2020-04-26 08:55:23' ),
( '3', '2E56', '1', '0', '14000', '378000', '0', '0', '2020-04-29 04:04:02', '2020-04-29 04:04:02' ),
( '4', '2E56', '1', '0', '14000', '378000', '0', '0', '2020-04-29 04:05:59', '2020-04-29 04:05:59' );
-- ---------------------------------------------------------


-- Dump data of "password_resets" --------------------------
-- ---------------------------------------------------------


-- Dump data of "profiles" ---------------------------------
-- ---------------------------------------------------------


-- Dump data of "promo_products" ---------------------------
INSERT INTO `promo_products`(`id`,`promo_id`,`product_id`,`created_at`,`updated_at`) VALUES 
( '24', '1', '1', '2020-04-03 03:38:15', '2020-04-03 03:38:15' ),
( '25', '1', '2', '2020-04-03 03:38:15', '2020-04-03 03:38:15' ),
( '26', '1', '8', '2020-04-03 03:38:15', '2020-04-03 03:38:15' ),
( '27', '1', '7', '2020-04-03 03:38:15', '2020-04-03 03:38:15' ),
( '28', '1', '4', '2020-04-03 03:38:15', '2020-04-03 03:38:15' ),
( '29', '1', '9', '2020-04-03 03:38:15', '2020-04-03 03:38:15' ),
( '30', '1', '10', '2020-04-03 03:38:15', '2020-04-03 03:38:15' ),
( '40', '2', '2', '2020-04-18 04:19:53', '2020-04-18 04:19:53' ),
( '41', '2', '3', '2020-04-18 04:19:53', '2020-04-18 04:19:53' ),
( '42', '2', '4', '2020-04-18 04:19:53', '2020-04-18 04:19:53' ),
( '43', '3', '7', '2020-04-18 04:39:35', '2020-04-18 04:39:35' ),
( '44', '3', '6', '2020-04-18 04:39:35', '2020-04-18 04:39:35' ),
( '45', '3', '5', '2020-04-18 04:39:35', '2020-04-18 04:39:35' ),
( '46', '3', '8', '2020-04-18 04:39:35', '2020-04-18 04:39:35' ),
( '47', '4', '9', '2020-04-18 04:46:43', '2020-04-18 04:46:43' ),
( '48', '4', '10', '2020-04-18 04:46:43', '2020-04-18 04:46:43' ),
( '49', '4', '8', '2020-04-18 04:46:43', '2020-04-18 04:46:43' ),
( '50', '4', '5', '2020-04-18 04:46:43', '2020-04-18 04:46:43' ),
( '51', '5', '4', '2020-04-18 04:50:36', '2020-04-18 04:50:36' ),
( '52', '5', '3', '2020-04-18 04:50:36', '2020-04-18 04:50:36' );
-- ---------------------------------------------------------


-- Dump data of "promos" -----------------------------------
INSERT INTO `promos`(`id`,`title`,`link`,`start_date`,`end_date`,`percentage`,`fix_price`,`status`,`created_at`,`updated_at`) VALUES 
( '1', 'Flash Sale', 'flash-sale', '2020-03-28', '2020-04-04', '10', '0', '1', '2020-03-28 01:27:07', '2020-04-03 03:38:15' ),
( '2', 'Promo bulan ini', 'promo-bulan-ini', '2020-04-01', '2020-04-30', '0', '30000', '1', '2020-04-18 03:12:50', '2020-04-18 04:19:53' ),
( '3', 'Buku Baru', 'buku-baru', '2020-04-01', '2020-04-30', '20', '0', '1', '2020-04-18 04:39:15', '2020-04-18 04:39:15' ),
( '4', 'Best Sellers', 'best-sellers', '2020-04-01', '2020-04-30', '40', '0', '1', '2020-04-18 04:46:10', '2020-04-18 04:46:10' ),
( '5', 'Buku Anak', 'buku-anak', '2020-04-01', '2020-04-30', '0', '25000', '1', '2020-04-18 04:50:21', '2020-04-18 04:50:21' );
-- ---------------------------------------------------------


-- Dump data of "provinces" --------------------------------
INSERT INTO `provinces`(`id`,`name`,`created_at`,`updated_at`) VALUES 
( '1', 'Bali', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '2', 'Bangka Belitung', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '3', 'Banten', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '4', 'Bengkulu', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '5', 'DI Yogyakarta', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '6', 'DKI Jakarta', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '7', 'Gorontalo', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '8', 'Jambi', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '9', 'Jawa Barat', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '10', 'Jawa Tengah', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '11', 'Jawa Timur', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '12', 'Kalimantan Barat', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '13', 'Kalimantan Selatan', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '14', 'Kalimantan Tengah', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '15', 'Kalimantan Timur', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '16', 'Kalimantan Utara', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '17', 'Kepulauan Riau', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '18', 'Lampung', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '19', 'Maluku', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '20', 'Maluku Utara', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '21', 'Nanggroe Aceh Darussalam (NAD)', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '22', 'Nusa Tenggara Barat (NTB)', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '23', 'Nusa Tenggara Timur (NTT)', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '24', 'Papua', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '25', 'Papua Barat', '2020-04-28 21:25:50', '2020-04-28 21:25:50' ),
( '26', 'Riau', '2020-04-28 21:25:51', '2020-04-28 21:25:51' ),
( '27', 'Sulawesi Barat', '2020-04-28 21:25:51', '2020-04-28 21:25:51' ),
( '28', 'Sulawesi Selatan', '2020-04-28 21:25:51', '2020-04-28 21:25:51' ),
( '29', 'Sulawesi Tengah', '2020-04-28 21:25:51', '2020-04-28 21:25:51' ),
( '30', 'Sulawesi Tenggara', '2020-04-28 21:25:51', '2020-04-28 21:25:51' ),
( '31', 'Sulawesi Utara', '2020-04-28 21:25:51', '2020-04-28 21:25:51' ),
( '32', 'Sumatera Barat', '2020-04-28 21:25:51', '2020-04-28 21:25:51' ),
( '33', 'Sumatera Selatan', '2020-04-28 21:25:51', '2020-04-28 21:25:51' ),
( '34', 'Sumatera Utara', '2020-04-28 21:25:51', '2020-04-28 21:25:51' );
-- ---------------------------------------------------------


-- Dump data of "scripts" ----------------------------------
-- ---------------------------------------------------------


-- Dump data of "sliders" ----------------------------------
INSERT INTO `sliders`(`id`,`title`,`sub_title`,`description`,`link`,`image_url`,`start_date`,`end_date`,`created_at`,`updated_at`) VALUES 
( '1', 'Buku Baru!', 'Fiqh Wanita', 'Buku best seller dengan cetakan yang sudah ke 40, dapatkan segera sebelum kehabisan.', 'buku-baru!', 'slider_slider1-eposi6-1920x933.jpg', '2020-03-28', '2020-03-29', '2020-03-28 09:58:43', '2020-04-02 14:35:29' ),
( '2', 'Promo Hari ini!', 'Fiqh Wanita', 'Buku best seller dengan cetakan yang sudah ke 40, dapatkan segera sebelum kehabisan.', 'promo-hari-ini!', 'slider_slider2-eposi6-1920x933.jpg', '2020-03-28', '2020-03-29', '2020-03-28 10:03:19', '2020-04-02 14:35:35' ),
( '3', 'Segera Terbit!', 'Fiqh Wanita', 'Buku best seller dengan cetakan yang sudah ke 40, dapatkan segera sebelum kehabisan.', 'segera-terbit!', 'slider_slider3-eposi6-1920x933.jpg', '2020-03-28', '2020-03-29', '2020-03-28 10:03:43', '2020-04-02 14:35:41' );
-- ---------------------------------------------------------


-- Dump data of "users" ------------------------------------
INSERT INTO `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`remember_token`,`date_of_birth`,`place_of_birth`,`sex`,`region`,`zipcode`,`city`,`district`,`address`,`phone`,`created_at`,`updated_at`) VALUES 
( '1', 'Elvis Sonatha', 'elviskudo@gmail.com', NULL, '$2y$10$cMEjGbEn3Si/rDMQ7OUzKe4kIH4hCrporHOHal5gKM5c/e6VyZNBK', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '2020-04-11 11:00:43', '2020-04-11 11:00:43' ),
( '2', 'Umar', 'umar.said@gmail.com', NULL, '$2y$10$QzVZ4Ju.KD6l8XBT3335qe/yUBO7X1FaNpIOdkkMIiivAVUzvOyNm', NULL, '2009-12-30', 'Malang', 'M', '9', '17121', '55', ' - ', 'Jl. Ursula 177 RT 2 RW 4 Jatimurni', '077867678', '2020-05-03 03:02:03', '2020-05-03 03:02:03' );
-- ---------------------------------------------------------


-- Dump data of "wishlists" --------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "password_resets_email_index" ------------------
CREATE INDEX `password_resets_email_index` USING BTREE ON `password_resets`( `email` );
-- -------------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


