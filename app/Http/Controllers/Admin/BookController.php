<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Book;
use App\Models\Category;
use App\Models\Author;

class BookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = Book::all();

        return view('admin.book.index', compact('model'));
    }

    /**
     * Display a listing of the resource to select2
     */
    public function select ()
    {
        $model = Book::all();

        $data = [];
        foreach ($model as $row) {
            $data[] = [
                'id' => $row->id,
                'title' => $row->title
            ];
        }

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('title')->get();
        $authors = Author::orderBy('name')->get();

        return view('admin.book.create', compact('categories', 'authors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filename = '';
        if ($request->file('image_url')) {
            $path = 'uploads/';
            $file = $request->file('image_url');

            $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $filename = 'book_' . Str::slug($filename, '-');
            $filename .= '.' . $extension;

            $file->move($path, $filename);
        }

        $model = new Book;
        $model->title = $request->input('title');
        $model->link = Str::slug($request->input('title'), '-');
        $model->category_id = $request->input('category_id');
        $model->summary = $request->input('summary');
        $model->description = $request->input('description');
        $model->author_id = $request->input('author_id');
        $model->isbn = $request->input('isbn');
        $model->cover = $request->input('cover');
        $model->pages = $request->input('pages');
        $model->weight = $request->input('weight');
        $model->size = $request->input('size');
        $model->price = $request->input('price');
        $model->discount = $request->input('discount');
        $model->stock = $request->input('stock');
        $model->status = $request->input('status');
        $model->published_at = $request->input('published_at');
        $model->image_url = $filename;
        $model->save();

        return redirect()->route('admin.book.index')->with('success', 'Buku berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Book::find($id);
        $categories = Category::orderBy('title')->get();
        $authors = Author::orderBy('name')->get();

        return view('admin.book.edit', compact('model', 'categories', 'authors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $filename = $request->input('is_image');

        if ($request->file('image_url')) {
            $path = 'uploads/';
            $file = $request->file('image_url');

            $filename = \pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $filename = 'book_' . Str::slug($filename, '-');
            $filename .= '.' . $extension;

            $file->move($path, $filename);
        }

        $model = Book::find($id);
        // dump($model);
        $model->title = $request->input('title');
        $model->link = Str::slug($request->input('title'), '-');
        $model->category_id = $request->input('category_id');
        $model->summary = $request->input('summary');
        $model->description = $request->input('description');
        // dump($model);
        $model->author_id = $request->input('author_id');
        $model->isbn = $request->input('isbn');
        $model->cover = $request->input('cover');
        $model->pages = $request->input('pages');
        $model->weight = $request->input('weight');
        $model->size = $request->input('size');
        $model->price = $request->input('price');
        $model->discount = $request->input('discount');
        $model->stock = $request->input('stock');
        $model->status = $request->input('status');
        $model->published_at = $request->input('published_at');
        $model->image_url = $filename;
        // dump($model);
        // exit;
        $model->save();

        return redirect()->route('admin.book.index')->with('success', 'Buku berhasil dibuat');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
