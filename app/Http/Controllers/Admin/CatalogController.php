<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Catalog;

class CatalogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = Catalog::all();

        return view('admin.catalog.index', compact('model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.catalog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ini_set('upload_max_filesize', '24M');
        ini_set('post_max_size', '24M');
        ini_set('max_input_time', 1024);
        ini_set('max_execution_time', 1024);

        $filename = '';
        if ($request->file('image_url')) {
            $path = 'uploads/';
            $file = $request->file('image_url');
            
            $filename = \pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $filename = 'catalog_' . Str::slug($filename, '-');
            $filename .= '.' . $extension;
    
            $file->move($path, $filename);
        }

        $download_filename = '';
        if ($request->file('download_url')) {
            $path = 'uploads/';
            $file = $request->file('download_url');
            
            $download_filename = \pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $download_filename = 'catalog_' . Str::slug($download_filename, '-');
            $download_filename .= '.' . $extension;
    
            $file->move($path, $download_filename);
        }

        $model = new Catalog;
        $model->user_id = 1;
        $model->title = $request->input('title');
        $model->sub_title = $request->input('sub_title');
        $model->link = Str::slug($request->input('title'), '-');
        $model->image_url = $filename;
        $model->download_url = $download_filename;
        $model->save();

        return redirect()->route('admin.catalog.index')->with('success', 'Data iklan berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Catalog::find($id);

        return view('admin.catalog.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $filename = $request->input('is_image');
        $download_filename = $request->input('is_download');

        if ($request->file('image_url')) {
            $path = 'uploads/';
            $file = $request->file('image_url');
            
            $filename = \pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $filename = 'catalog_' . Str::slug($filename, '-');
            $filename .= '.' . $extension;
    
            $file->move($path, $filename);
        }

        if ($request->file('download_url')) {
            $path = 'uploads/';
            $file = $request->file('download_url');

            $download_filename = \pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $download_filename = 'catalog_' . Str::slug($download_filename, '-');
            $download_filename .= '.' . $extension;
    
            $file->move($path, $download_filename);
        }

        $model = Catalog::find($id);
        $model->title = $request->input('title');
        $model->link = Str::slug($request->input('title'), '-');
        $model->sub_title = $request->input('sub_title');
        $model->image_url = $filename;
        $model->download_url = $download_filename;
        $model->save();

        return redirect()->route('admin.catalog.index')->with('success', 'Data iklan berhasil diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Catalog::find($id);

        if ($model->delete())
            return response()->json([
                'success' => true,
                'message' => 'Data iklan berhasil dihapus',
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Data iklan tidak berhasil dihapus',
            ]);
    }
}
