<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Event;

class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = Event::all();

        return view('admin.event.index', compact('model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.event.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filename = '';
        if ($request->file('image_url')) {
            $path = 'uploads/';
            $file = $request->file('image_url');
            
            $filename = \pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $filename = 'event_' . Str::slug($filename, '-');
            $filename .= '.' . $extension;
    
            $file->move($path, $filename);
        }

        $model = new Event;
        $model->title = $request->input('title');
        $model->link = Str::slug($request->input('title'), '-');
        $model->place = $request->input('place');
        $model->start_date = $request->input('start_date');
        $model->end_date = $request->input('end_date');
        $model->booth = $request->input('booth');
        $model->image_url = $filename;
        $model->save();

        return redirect()->route('admin.event.index')->with('success', 'Data iklan berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Event::find($id);

        return view('admin.event.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Event::find($id);

        return view('admin.event.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $filename = $request->input('is_image');

        if ($request->file('image_url') != null) {
            $path = 'uploads/';
            $file = $request->file('image_url');
            
            $filename = \pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $filename = 'event_' . Str::slug($filename, '-');
            $filename .= '.' . $extension;
    
            $file->move($path, $filename);
        }

        $model = Event::find($id);
        $model->title = $request->input('title');
        $model->link = Str::slug($request->input('title'), '-');
        $model->place = $request->input('place');
        $model->start_date = $request->input('start_date');
        $model->end_date = $request->input('end_date');
        $model->booth = $request->input('booth');
        $model->image_url = $filename;
        $model->save();

        return redirect()->route('admin.event.index')->with('success', 'Data iklan berhasil diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Event::find($id);

        if ($model->delete())
            return response()->json([
                'success' => true,
                'message' => 'Data pameran berhasil dihapus',
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Data pameran tidak berhasil dihapus',
            ]);

    }
}
