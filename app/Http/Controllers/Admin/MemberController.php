<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class MemberController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = User::select(
            'users.id',
            'users.name',
            'users.email',
            'addresses.phone',
            'addresses.address',
            'addresses.city',
            'addresses.district',
            'addresses.region',
            'addresses.postal_code'
        )
            ->leftJoin('addresses', 'addresses.user_id', '=', 'users.id')
            ->orderBy('users.name')->get();

        return view('admin.member.index', compact('model'));
    }

    /**
     * Display a listing of the resource with transaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function transactions()
    {
        $model = User::join('orders', 'users.id', '=', 'orders.user_id')
            ->orderBy('users.name')
            ->get();
        
        return view('admin.member.index', compact('model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.member.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = Member::create($request->all());

        return redirect()->route('admin.member.index')->with('success', 'Data Member berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Member::find($id);

        return view('admin.member.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Member::find($id);

        if ($model->delete())
            return response()->json([
                'success' => true,
                'message' => 'Data penulis berhasil dihapus',
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Data penulis tidak berhasil dihapus',
            ]);
    }
}
