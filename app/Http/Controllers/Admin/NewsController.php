<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

use App\Models\News;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = News::all();

        return view('admin.news.index', compact('model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image_file = '';
        if ($request->file('image_url')) {
            $path = 'uploads/';
            $file = $request->file('image_url');
            
            $filename = \pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $image_file = 'news_' . Str::slug($filename, '-');
            $image_file .= '.' . $extension;
    
            $file->move($path, $image_file);
        }

        $download_file = '';
        if ($request->file('download_url')) {
            $path = 'uploads/';
            $file = $request->file('download_url');
            
            $filename = \pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $download_file = 'news_' . Str::slug($filename, '-');
            $download_file .= '.' . $extension;
    
            $file->move($path, $download_file);
        }

        $model = new News;
        $model->title = $request->input('title');
        $model->link = Str::slug($request->input('title'), '-');
        $model->body = $request->input('body');
        $model->status = $request->input('status');
        $model->image_url = $image_file;
        $model->download_url = $download_file;
        $model->save();

        return redirect()->route('admin.news.index')->with('success', 'Data iklan berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = News::find($id);

        return view('admin.news.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = News::find($id);

        return view('admin.news.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image_file = $request->input('is_image');

        if ($request->file('image_url') != null) {
            $path = 'uploads/';
            $file = $request->file('image_url');
            
            $filename = \pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $image_file = 'news_' . Str::slug($filename, '-');
            $image_file .= '.' . $extension;
    
            $file->move($path, $image_file);
        }

        $download_file = $request->input('is_download');

        if ($request->file('download_url')) {
            $path = 'uploads/';
            $file = $request->file('download_url');
            
            $filename = \pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $download_file = 'news_' . Str::slug($filename, '-');
            $download_file .= '.' . $extension;
    
            $file->move($path, $download_file);
        }

        $model = News::find($id);
        $model->title = $request->input('title');
        $model->link = Str::slug($request->input('title'), '-');
        $model->body = $request->input('body');
        $model->status = $request->input('status');
        $model->image_url = $image_file;
        $model->download_url = $download_file;
        $model->save();

        return redirect()->route('admin.news.index')->with('success', 'Data iklan berhasil diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = News::find($id);

        if ($model->delete())
            return response()->json([
                'success' => true,
                'message' => 'Data berita berhasil dihapus',
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Data berita tidak berhasil dihapus',
            ]);

    }
}
