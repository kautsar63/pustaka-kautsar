<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Book;
use App\Models\Order;
use App\Models\OrderDetail;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function index (Request $request)
    {
        $model = Order::all();
        
        return view('admin.order.index', compact('model'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function summary(Request $request)
    {
        $start_date = date('Y-m-d') . ' 00:00:00';
        $end_date = date('Y-m-d') . ' 23:59:59';
        
        if (!empty($request->get('start_date')))
            $start_date = date('Y-m-d', strtotime($request->get('start_date'))) . ' 00:00:00';

        if (!empty($request->get('end_date')))
            $end_date = date('Y-m-d', strtotime($request->get('end_date'))) . ' 23:59:59';

        $model = Order::select(\DB::raw('count(`id`) as total'), \DB::raw('DATE(`created_at`) as date'))
            ->whereBetween('created_at', [$start_date, $end_date])
            ->groupBy(\DB::raw('DATE(`created_at`)'))
            ->get();
            
        $orders = Order::whereBetween('created_at', [$start_date, $end_date])
            ->get();

        $labels = [];
        $datas = [];
        foreach ($model as $row) {
            array_push($labels, $row->date);
            array_push($datas, $row->total);
        }

        return view('admin.order.summary', compact('model', 'orders', 'start_date', 'end_date', 'labels', 'datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Order;
        $model->user_id = 1;
        $model->code = $request->input('code');
        $model->sub_total = $request->input('sub_total');
        $model->courier_budget = $request->input('courier_budget');
        $model->total = $request->input('total');
        $model->payment_status = $request->input('payment_status');
        $model->status = $request->input('status');
        $model->save();

        return redirect()->route('admin.order.index')->with('success', 'Data transaksi berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Order::find($id);

        return view('admin.order.show', compact('model'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request)
    {
        $id = $request->input('id');
        $value = $request->input('value');

        $model = \DB::table('orders')->where('id', $id)->update(['status' => $value]);

        if ($model) {
            // kurangi stok
            if ($value == 1) {
                $detail = OrderDetail::select(\DB::raw('sum(product_id) as total_product'), 'product_id')
                    ->where('order_id', $id)
                    ->groupBy('product_id')
                    ->get();
                    
                foreach ($detail as $det) {
                    $stockProduct = Book::find($det->product_id);
                    $stockProduct->stock -= 1;
                    $stockProduct->save();
                }
            }

            return response()->json([
                'success' => true,
                'message' => 'Data status berhasil dirubah',
                'data' => Order::find($id)
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Data status tidak berhasil dirubah',
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Order::find($id);

        return view('admin.order.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Order::find($id);
        $model->code = $request->input('code');
        $model->sub_total = $request->input('sub_total');
        $model->courier_budget = $request->input('courier_budget');
        $model->total = $request->input('total');
        $model->payment_status = $request->input('payment_status');
        $model->status = $request->input('status');
        $model->save();

        return redirect()->route('admin.order.index')->with('success', 'Data transaksi berhasil diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Order::find($id);

        if ($model->delete())
            return redirect()->route('admin.order.index')->with('success', 'Data transaksi berhasil dihapus');
        else
            return redirect()->route('admin.order.index')->with('error', 'Data transaksi tidak berhasil dihapus');
    }
}
