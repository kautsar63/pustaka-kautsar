<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Promo;
use App\Models\PromoProduct;
use App\Models\Book;

class PromoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = Promo::all();

        return view('admin.promo.index', compact('model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.promo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Promo;
        $model->title = $request->input('title');
        $model->link = Str::slug($request->input('title'), '-');
        $model->start_date = $request->input('start_date');
        $model->end_date = $request->input('end_date');
        $model->status = $request->input('status');
        $model->save();

        return redirect()->route('admin.promo.index')->with('success', 'Data iklan berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Promo::find($id);

        return view('admin.promo.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Promo::find($id);
        $books = Book::all();

        $data = [];
        foreach ($books as $row) {
            array_push($data, [
                'id' => $row->id,
                'title' => $row->title,
                'discount' => $row->discount
            ]);
        }

        return view('admin.promo.edit', compact('model', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \DB::beginTransaction();

        try {
            // save detail
            $promo = PromoProduct::where('promo_id', (int) $id);
            if ($promo->count()) {
                foreach ($promo->get() as $row) {
                    $book = Book::find($row->product_id);
                    $book->discount = 20;
                    $book->save();
                }

                $promo->delete();
            }

            if ($request->input('books')) {
                foreach ($request->input('books') as $key => $row) {
                    $detail = new PromoProduct;
                    $detail->promo_id = (int) $id;
                    $detail->product_id = (int) $row;
                    $detail->save();
    
                    $discountEdit = Book::find((int) $row);
                    $discountEdit->discount = $request->input('discount')[$key] ?? 0;
                    $discountEdit->save();
                }
            }

            $model = Promo::find($id);
            $model->start_date = $request->input('start_date');
            $model->end_date = $request->input('end_date');
            $model->status = $request->input('status');
            $model->save();

            \DB::commit();
        } catch (Exception $e) {
            \DB::rollBack();
            dd($e);
        }

        return redirect()->route('admin.promo.index')->with('success', 'Data iklan berhasil diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Promo::find($id);

        if ($model->delete())
            return response()->json([
                'success' => true,
                'message' => 'Data promo berhasil dihapus',
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Data promo tidak berhasil dihapus',
            ]);

    }
}
