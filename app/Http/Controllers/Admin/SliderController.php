<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Slider;

class SliderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = Slider::all();

        return view('admin.slider.index', compact('model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filename = '';
        if ($request->file('image_url')) {
            $path = 'uploads/';
            $file = $request->file('image_url');
            
            $filename = \pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $filename = 'slider_' . Str::slug($filename, '-');
            $filename .= '.' . $extension;
    
            $file->move($path, $filename);
        }

        $model = new Slider;
        $model->title = $request->input('title');
        $model->link = Str::slug($request->input('title'), '-');
        $model->sub_title = $request->input('sub_title');
        $model->description = $request->input('description');
        $model->image_url = $filename;
        $model->save();

        return redirect()->route('admin.slider.index')->with('success', 'Data iklan berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Slider::find($id);

        return view('admin.slider.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $filename = $request->input('is_image');

        if ($request->file('image_url')) {
            $path = 'uploads/';
            $file = $request->file('image_url');
            
            $filename = \pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $filename = 'slider_' . Str::slug($filename, '-');
            $filename .= '.' . $extension;
    
            $file->move($path, $filename);
        }

        $model = Slider::find($id);
        $model->title = $request->input('title');
        $model->link = $request->input('link');
        $model->sub_title = $request->input('sub_title');
        $model->description = $request->input('description');
        $model->image_url = $filename;
        $model->save();

        return redirect()->route('admin.slider.index')->with('success', 'Data slider berhasil diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Slider::find($id);

        if ($model->delete()) {
            return json_encode([
                'status' => 200,
                'message' => 'ok',
                'data' => null
            ]);
        } else {
            return json_encode([
                'status' => 400,
                'message' => 'error',
                'data' => null
            ]);
        }
    }
}
