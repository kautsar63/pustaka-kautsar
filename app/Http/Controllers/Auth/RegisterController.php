<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\Address;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'address' => ['required', 'string', 'min:8'],
            'city' => ['required', 'string', 'min:8'],
            'district' => ['required', 'string', 'min:4'],
            'region' => ['required', 'string', 'min:8'],
            'phone' => ['required', 'string', 'min:8'],
            'zipcode' => ['required', 'string', 'min:5'],
        ]);
    }

    public function showRegisterForm ()
    {
        $province = Province::get();

        return view('auth.register', compact('province'));
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // dd($data);
        $model = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'address' => $data['address'],
            'city' => $data['city'],
            'district' => $data['district'],
            'region' => $data['region'],
            'phone' => $data['phone'],
            'zipcode' => $data['zipcode']
        ]);

        $address = new Address;
        $address->user_id = $model->id;
        $address->name = $data['name'];
        $address->address = $data['address'];
        $address->city = $data['city'];
        $address->city_id = $data['city_id'];
        $address->district = $data['district'];
        $address->district_id = $data['district_id'];
        $address->postal_code = $data['zipcode'];
        $address->region = $data['region'];
        $address->phone = $data['phone'];
        $address->save();

        return true;
    }
    
    public function register (Request $request)
    {
        $this->validator($request->all());
        
        $model = User::where('email', $request->get('email'))->first();
        if (!$model) {
            $this->create($request->all());

            return redirect()->route('home')->with('status', 'Member berhasil terdaftar!');
        } else {
            return redirect()->route('login')->with('status', 'Member sudah terdaftar, silakan masukkan email dan password');
        }
    }
}
