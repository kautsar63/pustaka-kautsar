<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Models\Slider;
use App\Models\Ads;
use App\Models\Author;
use App\Models\Book;
use App\Models\Category;
use App\Models\News;
use App\Models\Catalog;
use App\Models\Event;
use App\Models\Message;
use App\Models\Promo;
use App\Models\PromoProduct;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Address;
use App\Models\Province;
use App\Models\City;
use App\Models\Wishlist;
use App\Mail\Marketing;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $ads = Ads::all();
        $sliders = Slider::all();
        $flashs = Promo::where('link', 'flash-sale')->first();
        $promo = PromoProduct::where('promo_id', $flashs->id);

        if (time() > strtotime($flashs->end_date) || $promo->count() == 0) {
            // remove products in flash sale
            $result = $promo->get();

            // mengembalikan diskon di buku buku
            foreach ($result as $row) {
                $book = Book::find($row->product_id);
                $book->discount = 20;
                $book->save();
            }

            $promo->delete();
        }

        $books = Book::limit(5)->get();
        $newBooks = Promo::where('link', 'buku-baru')->first();
        $bestSellers = Promo::where('link', 'best-sellers')->first();
        $childrens = Promo::where('link', 'buku-anak')->first();
        $news = News::limit(5)->get();

        return view('home', compact(
            'sliders',
            'ads',
            'flashs',
            'books',
            'promo',
            'newBooks',
            'bestSellers',
            'childrens',
            'news'
        ));
    }

    public function detail ($link)
    {
        $model = Book::where('link', $link)->first();

        return view('detail', compact('model'));
    }

    public function aboutUs ()
    {
        return view('about-us');
    }

    public function callUs ()
    {
        return view('call-us');
    }

    public function category ($link)
    {
        $category = Category::where('link', $link)
            ->first();

        $model = Book::where('category_id', $category->id);

        if (isset($_GET['sort'])) {
            switch ($_GET['sort']) {
                case 1: $model->orderBy('title', 'asc'); break;
                case 2: $model->orderBy('created_at', 'desc'); break;
                case 3: $model->orderBy('price', 'desc'); break;
                case 4: $model->orderBy('price', 'asc'); break;
                default: $model->orderBy('title', 'asc');
            }
        } else {
            $model->orderBy('title', 'asc');
        }

        $total = $model->count();
        $model = $model->paginate(20);

        $title = $category->title;

        return view('main', compact(
            'category',
            'model',
            'link',
            'title',
            'total'
        ));
    }

    public function event ()
    {
        $model = Event::all();

        return view('event', compact('model'));
    }

    public function catalog ()
    {
        $model = Catalog::all();

        return view('catalog', compact('model'));
    }

    public function sendDocument ()
    {
        return view('document');
    }

    public function sendIlustrator ()
    {
        return view('ilustrator');
    }

    public function saveDocument (Request $request)
    {
        $filename = '';
        if ($request->file('upload_url')) {
            $path = 'uploads/';
            $file = $request->file('upload_url');

            $filename = \pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $filename = 'document_' . Str::slug($filename, '-');
            $filename .= '.' . $extension;

            $file->move($path, $filename);
        }

        $model = new Document;
        $model->title = 'DOCUMENT_' . time();
        $model->email = $request->input('email');
        $model->upload_url = $filename;
        $model->save();

        return redirect()->route('send-document')->with('success', 'Jazakallahu khoiron telah mengirimkan naskah kepada kami, tunggu review dari kami!');
    }

    public function message (Request $request)
    {
        $model = new Message;
        $model->name = $request->input('name');
        $model->email = $request->input('email');
        $model->message = $request->input('message');
        $model->save();

        // send to email
        $headers = array(
            'From' => $model->email,
            'Reply-To' => $model->email,
            'X-Mailer' => 'PHP/' . phpversion()
        );

        $sendTo = $request->input('email');
        $subject = 'Kontak Kami';
        $message = 'Terima kasih kepada ' . $model->name . ' yang telah menghubungi kami';

        if (mail($sendTo, $subject, $message, $headers)) {
            echo "Message sent succesfully.";
        } else {
            echo "There was problem while sending E-Mail.";
        }

        return redirect()->route('call-us')->with('success', 'Pesan Anda Sudah Kami Terima, Terima Kasih');
    }

    public function search (Request $request)
    {
        $model = Book::select(
            \DB::raw('books.id AS id'),
            'books.title',
            'books.description',
            'books.image_url',
            'books.cover',
            'books.isbn',
            'books.link',
            'books.pages',
            'books.price',
            'books.discount',
            'books.size',
            'books.status',
            'books.stock',
            'books.summary',
            'books.weight'
        )
            ->leftJoin('authors', 'authors.id', '=', 'books.author_id')
            ->where('books.title', 'like', '%' . $request->input('search') . '%')
            ->orWhere('authors.name', 'like', '%' . $request->input('search') . '%')
            ->get();

        return view('search', compact('model'));
    }

    public function member ()
    {
        $address = null;
        if (\Auth::user())
            $address = Address::where('user_id', \Auth::user()->id)->first();

        $province = Province::all();

        $orders = null;
        if (\Auth::user())
            $orders = Order::where('user_id', \Auth::user()->id)->orderBy('created_at', 'desc')->get();

        return view('member', compact('address', 'orders', 'province'));
    }

    public function promo ($link)
    {
        $model = Promo::where('link', $link)->first();
        $books = $model->products()->paginate(20);
        $total = $model->products->count();

        return view('promo', compact('model', 'books', 'total'));
    }

    public function showNews ($link)
    {
        $model = News::where('link', $link)->first();

        return view('news-detail', compact('model'));
    }

    public function news ()
    {
        $model = News::all();

        return view('news', compact('model'));
    }

    public function cart ()
    {
        return view('cart');
    }

    public function saveCheckout (Request $request)
    {
        $ids = $request->get('id');
        $titles = $request->get('title');
        $links = $request->get('link');
        $prices = $request->get('real_price');
        $qtys = $request->get('qty');

        $code = substr(md5(time()), 0, 4);

        $items = [];
        $total_weight = 0;

        $qty4id = [];
        for ($i = 0; $i < count($ids); $i++) {
            $book = Book::find($ids[$i]);
            $weight = $book->weight * $qtys[$i];

            $items[$ids[$i]] = [
                'title' => $titles[$i],
                'real_link' => $links[$i],
                'price' => $prices[$i],
                'qty' => $qtys[$i],
            ];

            $total_weight += (int) $weight;
        }
        // dd($total_weight);

        $data = [
            'total_qty' => $request->get('total_qty'),
            'total_price' => $request->get('total_price'),
            'items' => $items
        ];

        $address = [];
        $city_id = 0;
        $district_id = 0;
        if (!empty(\Auth::user())) {
            $address = Address::where('user_id', \Auth::user()->id)->first();
            $district_id = $address->district_id;
            $city_id = City::where('name', $address->city)->first();
        }

        return view('checkout', compact('data', 'code', 'address', 'district_id', 'city_id', 'total_weight'));
    }

    public function courierCheck (Request $request)
    {
        $origin = $request->get('origin');
        $destination = $request->get('destination');
        $weight = $request->get('weight');
        $courier = $request->get('courier');

        // get province
        $curl = curl_init();

        $body = [
            "origin" => $origin,
            "originType" => 'city',
            "destination" => $destination,
            "destinationType" => 'subdistrict',
            "weight" => $weight,
            "courier" => $courier,
        ];
        $body = json_encode($body);

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => array(
                "key: 615700f1a3bd0916860c25e75f728a0c",
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $model = json_decode($response);
            $data = [];
            if ($model) {
                $model = $model->rajaongkir->results;

                foreach ($model as $row) {
                    $data['code'] = $row->code;
                    $data['name'] = $row->name;

                    $costs = [];
                    foreach ($row->costs as $cost) {
                        $costs[] = [
                            'name' => $cost->service,
                            'description' => $cost->description,
                            'value' => $cost->cost[0]->value,
                            'etd' => $cost->cost[0]->etd,
                            'note' => $cost->cost[0]->note,
                        ];
                    }

                    $data['costs'] = $costs;
                }
            }

            return json_encode($data);
        }
    }

    public function wishlist ()
    {
        if (\Auth::user()) {
            $model = Wishlist::select(
                \DB::raw('books.id as id'),
                'books.title',
                'books.image_url',
                'books.price',
                'books.discount',
                'books.link'
            )
                ->join('books', 'books.id', '=', 'wishlists.book_id')
                ->where('wishlists.user_id', '=', \Auth::user()->id)
                ->get();

            return response()->json($model);
        } else {
            return false;
        }
    }

    public function setWishlist (Request $request)
    {
        if (\Auth::user()) {
            $model = Wishlist::where('book_id', (int) $request->input('id'))
                ->where('user_id', \Auth::user()->id)
                ->first();

            if (empty($model))
                $model = new Wishlist;

            $model->book_id = (int) $request->input('id');
            $model->user_id = \Auth::user()->id;
            $model->save();

            return true;
        } else {
            return false;
        }
    }

    public function deleteWishlist (Request $request)
    {
        $model = Wishlist::where('book_id', $request->input('id'))
            ->where('user_id', \Auth::user()->id)
            ->first();

        if ($model->delete())
            return true;
        else
            return false;
    }

    public function getCity ($id)
    {
        $model = City::where('province_id', $id)->orderBy('type')->get();

        return response()->json($model);
    }

    public function getDistrict ($id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://pro.rajaongkir.com/api/subdistrict?city=" . $id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key: 615700f1a3bd0916860c25e75f728a0c"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function book ($id)
    {
        $model = Book::find($id);
        $promo = Promo::find(3);

        $data = [
            'title' => $model->title,
            'description' => $model->description,
            'author' => isset($model->author) ? $model->author->name : '',
            'price' => $model->price,
            'discounted' => $model->price - ($model->price * ($model->discount / 100)),
            'isbn' => $model->isbn,
            'stock' => $model->stock,
            'cover' => $model->cover,
            'image_url' => $model->image_url,
            'id' => $model->id,
            'link' => $model->link
        ];

        return response()->json($data);
    }

    public function summary (Request $request)
    {
        $model = $request->all();

        // save to order
        $courier = str_replace('Rp. ', '', $request->get('courier'));
        $courier = str_replace('.', '', $courier);

        $total = str_replace('Rp. ', '', $request->get('total'));
        $total = str_replace('.', '', $total);

        \DB::beginTransaction();

        try {
            $order = new Order;
            $order->code = $request->get('code');
            $order->user_id = \Auth::user()->id;
            $order->courier_budget = $courier;
            $order->courier_name = $request->get('courier_name');
            $order->weight = $request->get('weight');
            $order->total = $total;
            $order->save();

            for ($i = 0; $i < count($request->get('id')); $i++) {
                $product_id = $request->get('id')[$i];
                $qty = $request->get('qty')[$i];
                $price = $request->get('price')[$i];

                $detail = new OrderDetail;
                $detail->order_id = $order->id;
                $detail->product_id = $product_id;
                $detail->price = $price;
                $detail->qty = $qty;
                $detail->save();
            }

            \DB::commit();
        } catch (Exception $e) {
            \DB::rollBack();
        }

        return view('summary', compact('model'));
    }

    public function reseller ()
    {
        return view('reseller');
    }

    public function testimonial ()
    {
        return view('testimonial');
    }

    public function forgotPassword ()
    {
        return view('forgot-password');
    }

    public function sendForgotPassword (Request $request)
    {
        if ($request->input('email')) {
            $to = $request->input('email');
            $subject = 'Forgot Password';
            $message = 'Forgot Password';

            Mail::to($to)->send(new Marketing());

            // Always set content-type when sending HTML email
            // $headers = 'MIME-Version: 1.0' . "\r\n";
            // $headers .= 'Content-type:text/html;charset=UTF-8' . "\r\n";

            // // More headers
            // $headers .= 'From: <' . $request->input('email') . '>' . "\r\n";

            // mail($to, $subject, $message, $headers);

            return redirect()->route('home')->with('success', 'Anda telah mengirimkan lupa password');
        } else {
            return redirect()->route('forgot-password')->with('success', 'Anda telah mengirimkan lupa password');
        }
    }

    public function credit ()
    {
        return view('credit');
    }

    public function caraBelanja ()
    {
        return view('cara-belanja');
    }

    public function pembayaran ()
    {
        return view('pembayaran');
    }

    public function pengiriman ()
    {
        return view('pengiriman');
    }
}
