<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Models\Address;
use App\Models\Province;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function saveAddress (Request $request)
    {
        $model = Address::where('user_id', \Auth::user()->id)->first();

        if (empty($model))
            $model = new Address;
            
        $region = Province::find($request->input('region'));

        $model->user_id = \Auth::user()->id;
        $model->name = $request->input('name');
        $model->address = $request->input('address');
        $model->city_id = $request->input('city_id');
        $model->city = $request->input('city');
        $model->district_id = $request->input('district_id');
        $model->district = $request->input('district');
        $model->region = isset($region->name) ? $region->name : '';
        $model->postal_code = $request->input('postal_code');
        $model->phone = $request->input('phone');
        $model->save();

        return redirect()->route('member')->with('success', 'Alamat sudah terupdate');
    }

    public function editProfile (Request $request)
    {
        $model = User::find(\Auth::user()->id);
        $model->name = $request->input('name');

        if ($request->input('old_password') && Hash::check($request->input('old_password'), $model->password)) {
            if ($request->input('password') != $request->input('password_confirmation'))
                return redirect()->route('admin')->with('error', 'Password tidak sama');

            $model->password = Hash::make($request->input('password'));
            $model->save();
        }

        return redirect()->route('member')->with('success', 'Profile berhasil di update');
    }
}
