<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'address',
        'city',
        'phone',
        'latitude',
        'longitude',
    ];

    public function member ()
    {
        return $this->hasOne(\App\User::class, 'id', 'user_id');
    }
}
