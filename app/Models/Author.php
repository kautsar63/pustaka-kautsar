<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = [
        'name',
        'biography',
        'phone',
        'email',
        'whatsapp'
    ];
}
