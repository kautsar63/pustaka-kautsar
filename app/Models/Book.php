<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'title',
        'category_id',
        'description',
        'summary',
        'author_id',
        'isbn',
        'cover',
        'pages',
        'weight',
        'size',
        'price',
        'image_url',
    ];

    protected $casts = [
        'published_at' => 'datetime:Y-m-d'
    ];

    public function category ()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function author ()
    {
        return $this->hasOne(Author::class, 'id', 'author_id');
    }

    public function wishlist ()
    {
        return $this->hasOne(Wishlist::class, 'id', 'book_id');
    }
}
