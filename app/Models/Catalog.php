<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    protected $fillable = [
        'title',
        'sub_title',
        'link',
        'image_url',
        'download_url',
        'status',
    ];
}
