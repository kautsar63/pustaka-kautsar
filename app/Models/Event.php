<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'title',
        'place',
        'start_date',
        'end_date',
        'booth',
        'image_url',
    ];
}
