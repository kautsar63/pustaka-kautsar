<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'code',
        'user_id',
        'sub_total',
        'courier_budget',
        'total',
        'payment_status',
        'status',
    ];

    public function user ()
    {
        return $this->hasOne(\App\User::class, 'id', 'user_id');
    }

    public function detail ()
    {
        return $this->hasMany(\App\Models\OrderDetail::class, 'order_id', 'id');
    }
}
