<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = [
        'order_id',
        'product_id',
        'price',
        'qty',
    ];
    
    public function order ()
    {
        return $this->hasOne(Order::class, 'id', 'order_id');
    }

    public function product ()
    {
        return $this->hasOne(Book::class, 'id', 'product_id');
    }
}
