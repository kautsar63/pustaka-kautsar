<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'description',
        'email',
        'phone',
        'whatsapp',
        'facebook',
        'instagram',
        'twitter',
        'linkedin',
    ];

    public function user ()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
