<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    protected $fillable = [
        'title',
        'start_date',
        'end_date',
        'percentage',
        'fix_price',
        'status'
    ];

    public function products ()
    {
        return $this->hasMany(PromoProduct::class, 'promo_id', 'id')->orderBy('created_at', 'desc');
    }
}
