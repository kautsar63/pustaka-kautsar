<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoProduct extends Model
{
    protected $fillable = [
        'product_id'
    ];

    public function product ()
    {
        return $this->hasOne(Book::class, 'id', 'product_id');
    }
}
