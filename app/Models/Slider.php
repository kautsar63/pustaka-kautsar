<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
        'title',
        'sub_title',
        'description',
        'link',
        'image_url',
        'start_date',
        'end_date',
    ];
}
