<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $fillable = [
        'book_id',
        'user_id'
    ];

    public function book ()
    {
        return $this->hasOne(Book::class, 'id', 'book_id');
    }

    public function user ()
    {
        return $this->hasOne(User::class, 'id', 'book_id');
    }
}
