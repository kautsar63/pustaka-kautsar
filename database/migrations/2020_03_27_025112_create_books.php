<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->integer('category_id');
            $table->text('description')->nullable();
            $table->text('summary')->nullable();
            $table->integer('author_id')->nullable();
            $table->string('isbn')->nullable();
            $table->string('cover')->nullable();
            $table->integer('pages')->default(0);
            $table->integer('weight')->default(0);
            $table->string('size')->nullable();
            $table->integer('price')->default(0);
            $table->string('image_url')->nullable();
            $table->string('file_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
