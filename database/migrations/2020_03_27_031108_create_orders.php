<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->integer('user_id');
            $table->integer('sub_total')->default(0);
            $table->integer('courier_budget')->default(0);
            $table->integer('total')->default(0);
            $table->tinyInteger('payment_status')->default(0)->comment('0: pending, 1: accept, 2: reject');
            $table->tinyInteger('status')->default(0)->comment('0: pending, 1: packaging, 2: delivery, 3: member accepted, 4: finished');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
