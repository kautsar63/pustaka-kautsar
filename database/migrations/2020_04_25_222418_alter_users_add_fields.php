<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUsersAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('phone')->notNullable()->after('remember_token');
            $table->string('address')->notNullable()->after('remember_token');
            $table->string('district')->notNullable()->after('remember_token');
            $table->string('city')->notNullable()->after('remember_token');
            $table->string('zipcode')->notNullable()->after('remember_token');
            $table->string('region')->nullable()->after('remember_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('phone');
            $table->dropColumn('address');
            $table->dropColumn('district');
            $table->dropColumn('city');
            $table->dropColumn('zipcode');
            $table->dropColumn('region');
        });
    }
}
