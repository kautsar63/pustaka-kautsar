<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUsersAddBirthdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('sex')->nullable()->after('remember_token');
            $table->string('place_of_birth')->nullable()->after('remember_token');
            $table->date('date_of_birth')->nullable()->after('remember_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('age');
            $table->dropColumn('place_of_birth');
            $table->dropColumn('date_of_birth');
        });
    }
}
