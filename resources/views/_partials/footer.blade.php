<!--====================  footer area ====================-->
<div class="footer-area footer-area--light-bg">
    <div class="footer-navigation section-space--inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!--=======  footer navigation wrapper  =======-->
                    <div class="footer-navigation-wrapper">
                        <div class="row">
                            <div class="col-lg-5 col-md-9">
                              <div class="newsletter-wrapper">
                                <h3 class="single-footer-widget__title">Info Promo dan Diskon</h3>
                                  <p class="short-desc" style="text-align:justify;">Jadilah yang pertama tahu berita terbaru, informasi pameran dan penawaran spesial kami</p>

                                  <div class="newsletter-form">
                                      <form id="mc-form" class="mc-form">
                                          <input type="email" placeholder="Masukkan Email Anda..." required>
                                          <button type="submit" value="submit">Daftar</button>
                                      </form>

                                  </div>
                                  <!-- mailchimp-alerts Start -->
                                  <div class="mailchimp-alerts">
                                      <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                                      <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                                      <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                                  </div>
                                  <!-- mailchimp-alerts end -->
                              </div>
                              <div class="social-share-buttons mt-20">
                                  <h3 class="single-footer-widget__title" style="padding-top:5px;">Ikuti Kami</h3>
                                  <ul>
                                      <li><a class="facebook" href="https://www.facebook.com/Officialpustakaalkautsar"><i class="fa fa-facebook"></i></a></li>
                                      <li><a class="twitter" href="https://twitter.com/buku_alkautsar"><i class="fa fa-twitter"></i></a></li>
                                      <li><a class="instagram" href="https://www.instagram.com/pustaka.alkautsar/"><i class="fa fa-instagram"></i></a></li>
                                  </ul>
                              </div>
                            </div>
                            <div class="col-lg-2  offset-lg-1 col-sm-4">
                                <!--=======  single footer widget  =======-->
                                <div class="single-footer-widget">
                                    <h3 class="single-footer-widget__title">Pustaka Al-Kautsar</h3>
                                    <div class="single-footer-widget__content">
                                      <ul class="footer-navigation">
                                          <li><a href="{{ route('about-us') }}">Tentang Kami</a></li>
                                          <li><a href="{{ route('testimonial') }}">Apa Kata Mereka ?</a></li>
                                          <!--<li><a href="{{ route('credit') }}">Penghargaan</a></li>-->
                                          <li><a href="{{ route('reseller') }}">Jadi Reseller</a></li>
                                          <!--<li><a href="{{ route('events') }}">Info Pameran</a></li>-->
                                          <li><a href="{{ route('send-document') }}">Kirim Naskah</a></li>
                                      </ul>
                                    </div>
                                </div>
                                <!--=======  End of single footer widget  =======-->
                            </div>
                            <div class="col-lg-2 col-sm-4">
                                <!--=======  single footer widget  =======-->
                                <div class="single-footer-widget">
                                    <h3 class="single-footer-widget__title">Informasi</h3>
                                    <div class="single-footer-widget__content">
                                        <ul class="footer-navigation">
                                            <li><a href="{{ route('cara-belanja') }}">Cara Belanja</a></li>
                                            <li><a href="{{ route('pembayaran') }}">Pembayaran</a></li>
                                            <li><a href="{{ route('pengiriman') }}">Pengiriman</a></li>
                                            <!--<li><a href="#">Lacak Pesanan</a></li>-->
                                            <li><a href="#">Pengembalian Buku</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!--=======  End of single footer widget  =======-->
                            </div>
                            <div class="col-lg-2 col-sm-4">
                                <!--=======  single footer widget  =======-->
                                <div class="single-footer-widget">
                                    <h3 class="single-footer-widget__title">Chat via Whatsapp</h3>
                                    <div class="single-footer-widget__content">
                                        <ul class="footer-navigation">
                                            <li><a href="https://api.whatsapp.com/send?phone=+6281952380191">Sales Counter</a></li>
                                            <li><a href="https://api.whatsapp.com/send?phone=+6281546182064">Direct Selling - Wiwin</a></li>
                                            <li><a href="https://api.whatsapp.com/send?phone=+6287883055533">Direct Selling - Royani</a></li>
                                            <li><a href="https://api.whatsapp.com/send?phone=+62895620042100">Redaksi</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!--=======  End of single footer widget  =======-->
                            </div>
                        </div>
                    </div>
                    <!--=======  End of footer navigation wrapper  =======-->
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container wide">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-copyright-wrapper">
                        <div class="container">
                            <div class="row align-items-center no-gutters">
                                <div class="col-md-6">
                                    <div class="copyright-text">
                                        Copyright &copy; 2020 <a href="#">Pustaka Al-Kautsar</a>. All Rights Reserved.
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="copyright-navigation-wrapper">
                                        <ul class="copyright-nav">
                                            <li><a href="#">Syarat & Ketentuan</a></li>
                                            <li><a href="#">Kebijakan & Privasi</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of footer area  ====================-->
<!--=======  offcanvas mobile menu  =======-->

<div class="offcanvas-mobile-menu" id="offcanvas-mobile-menu">
    <a href="javascript:void(0)" class="offcanvas-menu-close" id="offcanvas-menu-close-trigger">
        <i class="ion-android-close"></i>
    </a>

    <div class="offcanvas-wrapper">

        <div class="offcanvas-inner-content">
            <div class="offcanvas-mobile-search-area">
                <form action="{{ route('search') }}" method="post">
                    @csrf
                    <input type="search" placeholder="Cari Judul Buku, Penulis ...">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <nav class="offcanvas-navigation">
              <ul>
                  <li><a href="{{ url('/') }}">Beranda</a>
                  </li>

                  <li><a href="{{ route('about-us') }}">Tentang Kami</a>
                  </li>

                  <li class="menu-item-has-children"><a href="#">Katalog</a>
                    <ul class="sub-menu">
                        @foreach ($categories as $cat)
                        <li><a href="{{ url('category/' . $cat->link) }}">{{ $cat->title }}</a></li>
                        @endforeach

                        <li><a href="{{ route('catalog') }}">Download Katalog (PDF)</a></li>
                    </ul>
                  </li>

                  <li class="menu-item-has-children"><a href="#">Redaksi</a>
                    <ul class="sub-menu">
                        <li><a href="#">Info Redaksi</a></li>
                        <li><a href="{{ route('send-document') }}">Kirim Naskah Buku</a></li>
                        <li><a href="{{ route('send-ilustrator') }}">Kirim Portfolio Ilustrator</a></li>
                    </ul>
                  </li>

                  <li><a href="{{ route('call-us') }}">Hubungi Kami</a>
                  </li>
              </ul>
            </nav>

            <div class="offcanvas-settings">
                <nav class="offcanvas-navigation">
                    <ul>
                        @if (Auth::user())
                        <li>
                            <a href="{{ route('member') }}" class="btn">{{ Auth::user()->name }}</a>
                        </li>
                        @else
                        <li class="menu-item-has-children"><a href="{{ route('register') }}"> LOGIN </a>
                        </li>
                        <li class="menu-item-has-children"><a href="{{ route('register') }}"> DAFTAR </a>
                        </li>
                        @endif
                    </ul>
                </nav>

                <div class="offcanvas-widget-area">
                    <div class="off-canvas-contact-widget">
                        <div class="header-contact-info">
                            <ul class="header-contact-info__list">
                                <li><i class="ion-android-phone-portrait"></i> <a href="tel://12452456012">(+62) 21000 000 </a></li>
                                <li><i class="ion-android-mail"></i> <a href="mailto:info@kautsar.co.id">info@kautsar.co.id</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<!--=======  End of offcanvas mobile menu  =======-->
<!--====================  search overlay ====================-->

<div class="search-overlay" id="search-overlay">
    <a href="javascript:void(0)" class="close-search-overlay" id="close-search-overlay">
        <i class="ion-ios-close-empty"></i>
    </a>

    <!--=======  search form  =======-->

    <div class="search-form">
        <form action="{{ route('search') }}" method="post">
            @csrf
            <input type="text" id="search" name="search" placeholder="Cari Judul Buku, Penulis ...">
            <button type="submit"><i class="ion-android-search"></i></button>
        </form>
    </div>

    <!--=======  End of search form  =======-->
</div>

<!--====================  End of search overlay  ====================-->
