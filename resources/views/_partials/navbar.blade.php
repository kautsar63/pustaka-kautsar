<div class="navigation-menu-wrapper">
    <nav>
        <ul>
            <li><a href="{{ url('/') }}">Beranda</a></li>

            <li><a href="{{ route('about-us') }}">Tentang Kami</a></li>

            <li class="menu-item-has-children"><a href="#">Katalog</a>
                <ul class="sub-menu">
                    @foreach ($categories as $cat)
                        <li><a href="{{ url('category/' . $cat->link) }}">{{ $cat->title }}</a></li>
                    @endforeach

                    <li><a href="https://drive.google.com/file/d/1vaUoe24SOE2i4--HYlJgc-B86phEGjzW/view" target="_blank">Download Katalog (PDF)</a></li>
                </ul>
            </li>

            <li class="menu-item-has-children"><a href="#">Redaksi</a>
                <ul class="sub-menu">
                    <li><a href="#">Info Redaksi</a></li>
                    <li><a href="{{ route('send-document') }}">Kirim Naskah Buku</a></li>
                    <li><a href="{{ route('send-ilustrator') }}">Kirim Portfolio Ilustrator</a></li>
                </ul>
            </li>

            <li><a href="{{ route('call-us') }}">Hubungi Kami</a></li>
        </ul>
    </nav>
</div>