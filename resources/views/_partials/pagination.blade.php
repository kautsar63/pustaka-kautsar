<!--@if ($paginator->lastPage() > 1)-->
<!--<ul class="pagination-section">-->
<!--    <li>-->
<!--        <a class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}" href="{{ $paginator->url(1) }}"><</a>-->
<!--    </li>-->
<!--    @for ($i = 1; $i <= $paginator->lastPage(); $i++)-->
<!--        <li>-->
<!--            <a class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}" href="{{ $paginator->url($i) }}">{{ $i }}</a>-->
<!--        </li>-->
<!--    @endfor-->
<!--    <li>-->
<!--        <a class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}" href="{{ $paginator->url($paginator->currentPage()+1) }}" >></a>-->
<!--    </li>-->
<!--</ul>-->
<!--@endif-->
@if (isset($_GET['sort']))
    {!! $paginator->appends(['sort' => $_GET['sort']])->links() !!}
@else
    {!! $paginator->links() !!}
@endif