<div class="row">
    <div class="col-lg-12">
        <!--=======  header wrapper  =======-->
        <div class="header-wrapper d-none d-lg-flex">
            <!-- logo -->
            <div class="logo text-left">
                <a href="{{ url('/') }}">
                    <img src="{{ asset('assets/img/logo.png') }}" class="img-fluid" alt="">
                </a>
            </div>

            <!-- search form -->
            <div class="settings-dropdown-wrapper">
                <div class="col-auto">
                    <form action="{{ route('search') }}" method="post">
                        @csrf
                        <div class="input-group mb-2">
                            <input type="text" class="form-control" id="search" name="search" placeholder="Cari Judul Buku, Penulis....." aria-label="Recipient's username" aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit" id="button-addon2" onclick="return (document.getElementById('search').value == '' ? false : true)">Cari</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <!-- header icon -->
            <div class="header-icon-wrapper">
                <ul class="icon-list">
                    <li>
                        <div class="header-cart-icon">
                            <a href="{{ route('cart') }}" id="minicart-trigger">
                                <i class="ion-bag"></i>
                                <span class="counter">0</span>
                            </a>
                            <!-- mini cart  -->
                            <div class="mini-cart" id="mini-cart">
                                <form action="{{ url('/checkout') }}" method="POST" id="sendCart">
                                    @csrf
                                    <div class="cart-items-wrapper ps-scroll ps">
                                        <div class="single-cart-item">
                                            <a href="javascript:void(0)" class="remove-icon"><i class="ion-android-close"></i></a>
                                            <div class="image">
                                                <a href="javascript:;">
                                                    <img src="" class="img-fluid" alt="">
                                                </a>
                                            </div>
                                            <div class="content">
                                                <p class="product-title"><a href="javascript:;"></a></p>
                                                <p class="count"><span></span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="total_qty_floating" name="total_qty">
                                    <input type="hidden" id="total_price_floating" name="total_price">
                                    <div class="cart-calculation">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td class="text-left">Sub-Total :</td>
                                                    <td class="text-right" id="total"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </form>
                                <div class="cart-buttons">
                                    <a href="{{ route('cart') }}">Lihat Keranjang Belanja</a>
                                    <a href="javascript:;" id="minicart-checkout">Checkout</a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                    </li>
                    @if (Auth::user())
                    <li>
                        <a href="{{ route('member') }}" class="btn" style="font-size: 18px">{{ Auth::user()->name }}</a>
                    </li>
                    @else
                    <li>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-outline-success btn-sm" data-toggle="modal" data-target="#exampleModalScrollable">
                            Login
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalScrollableTitle">Member Login</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="{{ route('login') }}" method="post">
                                        @csrf
                                        <div class="login-form">
                                            <div class="row">
                                                <div class="col-md-12 col-12">
                                                    <label>Email *</label>
                                                    <input type="email" name="email" placeholder="Masukkan Email Anda....." required>
                                                </div>
                                                <div class="col-12">
                                                    <label>Password *</label>
                                                    <input type="password" name="password" placeholder="Masukkan Password Anda....." required>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="check-box d-inline-block ml-0 ml-md-2">
                                                        <input type="checkbox" id="remember_me">
                                                        <label for="remember_me"><span style="color:#707070;">Ingatkan Saya</span></label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 text-left text-sm-right">
                                                    <a href="{{ route('forgot-password') }}" class="forget-pass-link"><span style="font-size:13px; color:#707070; font-weight: 500;">Lupa Password?</span></a>
                                                </div>
                                                <div class="col-md-12">
                                                    <button type="submit" class="register-button">Login</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="{{ route('register') }}">
                            <button type="button" class="btn btn-success btn-sm">Daftar</button>
                        </a>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
        <!--=======  End of header wrapper  =======-->

        <!--=======  mobile navigation area  =======-->
        <div class="header-mobile-navigation d-block d-lg-none">
            <div class="row align-items-center">
                <div class="col-6 col-md-6">
                    <div class="header-logo">
                        <a href="{{ url('/') }}">
                            <img src="{{ asset('assets/img/logo.png') }}" class="img-fluid" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-6">
                    <div class="mobile-navigation text-right">
                        <div class="header-icon-wrapper">
                            <ul class="icon-list justify-content-end">
                                <li>
                                    <a href="javascript:void(0)" id="search-overlay-trigger">
                                        <i class="ion-ios-search-strong"></i>
                                    </a>
                                </li>
                                <li>
                                    <div class="header-cart-icon">
                                        <a href="{{ route('cart') }}" id="minicart-trigger">
                                            <i class="ion-bag"></i>
                                            <span class="counter">0</span>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="mobile-menu-icon" id="mobile-menu-trigger"><i class="fa fa-bars"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--=======  End of mobile navigation area  =======-->

    </div>
</div>
