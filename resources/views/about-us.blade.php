@extends('layouts.app')

@section('content')
<div class="section-space" style="margin-top:-55px;">
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
          <div class="col-lg-12">
              <div class="about-top-content-wrapper">
                  <div class="row row-30">
                      <!-- About Image -->
                      <div class="col-lg-6">
                          <div class="about-image">
                              <img src="assets/img/img-tentang-kami.jpg" class="img-fluid" alt="" width="519" height="719">
                          </div>
                      </div>

                      <!-- About Content -->
                      <div class="about-content col-lg-6">
                          <div class="row">
                              <div class="col-12">
                                  <h3 style="color:#033147; text-transform:uppercase;">Profil Pustaka Al-Kautsar</h3>
                                  <p class="mb-3" style="font-size:13px; text-align:justify;">Segala puji bagi Allah yang telah memberikan kepada kami nikmat yang tak terhingga bilangannya.
                                     Salam dan shalawat bagi junjungan kami Nabi Muhammad Shallallahu Alaihi wa Sallam beserta segenap keluarga dan sahabat-sahabatnya, serta pengikutnya hingga akhir zaman.</p>

                                  <p class="mb-3" style="font-size:13px; text-align:justify;">Motto kami “<strong>PENERBIT BUKU ISLAM UTAMA</strong>” Kami berdiri untuk semua umat Islam, kami independen.
                                      Kami bukan milik suatu kelompok / golongan / aliran tertentu, dan tidak berafiliasi ke mana pun.
                                      Kami tidak disponsori juga tidak menjadi sponsor ataupun mewakili satu kelompok / golongan / aliran tertentu. Insya Allah kami berusaha komitmen memperjuangkan Izzul Islam Wal Muslimin, bersandar pada Kitabullah dan Sunnah Rasul. Kami adalah Ahlu Sunnah wal Jama’ah yang anti terhadap segala jenis bid’ah, kesesatan, dan penodaan agama maupun sikap berlebih-lebihan (ghuluw) dalam beragama.</p>

                                  <p class="mb-3" style="font-size:13px; text-align:justify;">Tema buku yang kami terbitkan pun sangat beragam. Ada akidah, tafsir, hadits, akhlak, sejarah Islam,
                                     Sirah Nabi, biografi sahabat dan para ulama, kisah-kisah Islami, pendidikan, ekonomi Islam, politik, kewanitaan dan keluarga, tips pengembangan diri, manajemen, maupun buku-buku pemikiran kontemporer.</p>

                                  <p class="mb-3" style="font-size:13px; text-align:justify;">Buku-buku kontemporer kami banyak menanggapi persoalan-persoalan yang dihadapi umat Islam Indonesia,
                                     terlebih dengan maraknya berbagai kelompok dakwah dan aliran keagamaan, serta paham menyimpang yang mengaku Islam.
                                     Sering kali pendapat kami tidak sejalan dengan aliran atau kelompok tersebut. Namun kritik yang kami sampaikan maupun melalui buku, insya Allah semata-mata didasari oleh semangat ilmu dan kebenaran, bukan semangat fanatisme kelompok.
                                     Insya Allah, dengan buku, kita akan kembangkan budaya kritis dan dialogis demi terwujudnya kemajuan dan kesadaran umat Islam.</p>

                                  <p class="mb-3" style="font-size:13px; text-align:justify;">Selain menerbitkan buku-buku terjemahan dari Timur Tengah, kami juga menerbitkan buku-buku karya penulis lokal. Saat ini,
                                    sudah sekitar 30-an penulis lokal yang buku karyanya kami terbitkan.
                                    Dan kurang lebih ada 60 orang penerjemah free lance yang setiap saat siap menerjemahkan naskah-naskah berbahasa Arab / Inggris yang kami sodorkan.</p>
                                  <p class="mb-3" style="font-size:13px; text-align:justify;">Dengan seleksi naskah yang cukup ketat, baik untuk karya penulis lokal maupun buku-buku terjemahan,
                                    insya Allah kami senantiasa berusaha untuk memberikan yang terbaik bagi pembaca setia buku-buku Pustaka Al-Kautsar.</p>
                                  <p class="mb-3" style="font-size:13px; text-align:justify;">Kini sudah lebih dari 500 judul buku berhasil diluncurkan dan sudah jutaan eksemplar buku-buku kami dibaca di seluruh penjuru tanah air,
                                    dari Sabang sampai Merauke. Bahkan hingga ke manca negara.</p>
                              </div>

                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection
