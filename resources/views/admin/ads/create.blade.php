@extends('layouts.admin')

@section('contents')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Tambah Iklan
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form" method="POST" action="{{ route('admin.ads.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Nama Iklan
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control m-input" name="title" placeholder="Masukkan Judul Iklan" required>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Upload Gambar
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="file" class="form-control m-input" name="image_url" placeholder="Upload Gambar Buku" accept=".jpg, .jpeg, .png, image/jpeg, image/png">
                                        <small>
                                            <i>Ukuran: Width 370px, Height 260px | Tipe File: jpeg, jpg dan png</i>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-success" id="m_sweetalert_demo_3_3">
                                            Simpan
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Batal
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
</div>
@endsection