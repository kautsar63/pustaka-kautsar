@extends('layouts.admin')

@section('contents')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Tambah Buku
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form" method="POST" action="{{ route('admin.book.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Judul Buku
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control m-input" name="title" placeholder="Masukkan Judul Buku" required>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Kategori
                                    </label>
                                    <div class="col-lg-6">
                                        <select class="form-control m-bootstrap-select m_selectpicker" name="category_id" required>
                                            @foreach ($categories as $cat)
                                                <option value="{{ $cat->id }}">{{ $cat->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Tanggal Terbit
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="date" class="form-control m-input" id="published_at" name="published_at" placeholder="Masukkan Ringkasan Buku"/>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Ringkasan
                                    </label>
                                    <div class="col-lg-6">
                                        <textarea class="form-control m-input" id="summary" name="summary" placeholder="Masukkan Ringkasan Buku" style="height: 100px"></textarea>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Deskripsi Buku
                                    </label>
                                    <div class="col-lg-6">
                                        <textarea class="form-control m-input" id="description" name="description" placeholder="Masukkan Deskripsi Buku" style="height: 100px"></textarea>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Penulis
                                    </label>
                                    <div class="col-lg-6">
                                        <select class="form-control m-bootstrap-select m_selectpicker" name="author_id" required>
                                            @foreach ($authors as $author)
                                                <option value="{{ $author->id }}">{{ $author->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        ISBN
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control m-input" name="isbn" placeholder="Masukkan ISBN Buku">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Cover
                                    </label>
                                    <div class="col-lg-6">
                                        <select class="form-control m-bootstrap-select m_selectpicker" name="cover">
                                            <option value="Hard Cover">Hard Cover</option>
                                            <option value="Soft Cover">Soft Cover</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Halaman
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="number" class="form-control m-input" name="pages" placeholder="Masukkan Jumlah Halaman Buku">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Berat
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="number" class="form-control m-input" name="weight" placeholder="Masukkan Berat Buku">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Ukuran
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control m-input" name="size" placeholder="Masukkan Ukuran Buku">
                                        <small><i>Contoh: 15.5 x 24.5 cm</i></small>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Harga
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="number" class="form-control m-input" name="price" placeholder="Masukkan Harga Buku" required>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Diskon (%)
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="number" class="form-control m-input" name="discount" placeholder="Masukkan Diskon Buku" required>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Stok
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="number" class="form-control m-input" name="stock" placeholder="Masukkan Stok Buku" required>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Publish
                                    </label>
                                    <div class="col-lg-6">
                                        <div class="m-radio-inline">
                                            <label class="m-radio">
                                                <input type="radio" name="status" value="1">
                                                Aktif
                                                <span></span>
                                            </label>
                                            <label class="m-radio">
                                                <input type="radio" name="status" value="0">
                                                Tidak Aktif
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Upload Image
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="file" class="form-control m-input" name="image_url" placeholder="Upload Gambar Buku" accept=".jpg, .jpeg, .png, image/jpeg, image/png">
                                        <small>
                                            <i>Ukuran Cover: Width 600px, Height 800px | Tipe File: jpeg, jpg dan png</i>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-success" id="m_sweetalert_demo_3_3">
                                            Simpan
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Batal
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
</div>
@endsection

@section('scripts')
<script>
    var quill = new Quill('#summary', {
        theme: 'snow'
    });

    const description = document.getElementById("description");
    CKEDITOR.replace(description);

    CKEDITOR.config.allowedContent = true;
</script>
@endsection
