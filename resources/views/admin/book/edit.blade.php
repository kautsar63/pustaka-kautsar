@extends('layouts.admin')

@section('contents')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Edit Buku
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form" method="post" action="{{ route('admin.book.update', $model->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Judul Buku
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control m-input" name="title" value="{{ old('title', $model->title) }}" placeholder="Masukkan Judul Buku" required>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Kategori
                                    </label>
                                    <div class="col-lg-6">
                                        <select class="form-control m-bootstrap-select m_selectpicker" name="category_id" required>
                                            @foreach ($categories as $cat)
                                                <option value="{{ $cat->id }}" {{ $model->category_id === $cat->id ? 'selected' : '' }}>{{ $cat->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Tanggal Terbit
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="date" class="form-control m-input" id="published_at" name="published_at" placeholder="Masukkan Ringkasan Buku" value="{{ old('published_at', date('Y-m-d', strtotime($model->published_at)) ) }}"/>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Ringkasan
                                    </label>
                                    <div class="col-lg-6">
                                        <textarea class="form-control m-input" id="summary" name="summary" placeholder="Masukkan Ringkasan Buku" style="height: 100px">{{ old('summary', $model->summary) }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Deskripsi Buku
                                    </label>
                                    <div class="col-lg-6">
                                        <textarea class="form-control m-input" id="description" name="description" placeholder="Masukkan Deskripsi Buku" style="height: 100px">{{ old('description', $model->description) }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Penulis
                                    </label>
                                    <div class="col-lg-6">
                                        <select class="form-control m-bootstrap-select m_selectpicker" name="author_id" required>
                                            @foreach ($authors as $author)
                                                <option value="{{ $author->id }}" {{ $model->author_id === $author->id ? 'selected' : '' }}>{{ $author->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        ISBN
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control m-input" name="isbn" value="{{ old('isbn', $model->isbn) }}" placeholder="Masukkan ISBN Buku">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Cover
                                    </label>
                                    <div class="col-lg-6">
                                        <select class="form-control m-bootstrap-select m_selectpicker" name="cover">
                                            <option value="Hard Cover"{{ $model->cover == 'Hard Cover' ? ' selected' : '' }}>Hard Cover</option>
                                            <option value="Soft Cover"{{ $model->cover == 'Soft Cover' ? ' selected' : '' }}>Soft Cover</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Halaman
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="number" class="form-control m-input" name="pages" value="{{ old('pages', $model->pages) }}" placeholder="Masukkan Jumlah Halaman Buku">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Berat
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="number" class="form-control m-input" name="weight" value="{{ old('weight', $model->weight) }}" placeholder="Masukkan Berat Buku">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Ukuran
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control m-input" name="size" value="{{ old('size', $model->size) }}" placeholder="Masukkan Ukuran Buku">
                                        <small><i>Contoh: 15.5 x 24.5 cm</i></small>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Harga
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="number" class="form-control m-input" name="price" value="{{ old('price', $model->price) }}" placeholder="Masukkan Harga Buku" required>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Diskon (%)
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="number" class="form-control m-input" name="discount" value="{{ old('discount', $model->discount) }}" placeholder="Masukkan Diskon Buku" required>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Stok
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="number" class="form-control m-input" name="stock" value="{{ old('stock', $model->stock) }}" placeholder="Masukkan Stok Buku" required>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Publish
                                    </label>
                                    <div class="col-lg-6">
                                        <div class="m-radio-inline">
                                            <label class="m-radio">
                                                <input type="radio" name="status" value="1"{{ $model->status == 1 ? ' checked' : '' }}>
                                                Aktif
                                                <span></span>
                                            </label>
                                            <label class="m-radio">
                                                <input type="radio" name="status" value="0"{{ $model->status == 0 ? ' checked' : '' }}>
                                                Tidak Aktif
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Upload Cover
                                    </label>
                                    <div class="col-lg-6">
                                        @if ($model->image_url)
                                        <img src="{{ asset('uploads/' . $model->image_url) }}" alt="{{ $model->title }}" width="240" height="320">
                                        <input type="hidden" name="is_image" value="{{ $model->image_url }}">
                                        @endif
                                        <input type="file" class="form-control m-input" name="image_url" placeholder="Upload Gambar Buku" accept=".jpg, .jpeg, .png, image/jpeg, image/png">
                                        <small>
                                            <i>Ukuran: Width 600px, Height 800px | Tipe file: jpeg, jpg dan png</i>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-success" id="m_sweetalert_demo_3_3">
                                            Simpan
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Batal
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
</div>
@endsection

@section('scripts')
<script>
    var quill = new Quill('#summary', {
        theme: 'snow'
    });
    // const summary = document.getElementById("summary")
    // CKEDITOR.replace(summary);
    $('#summary').val(`{{ $model->summary }}`);

    const description = document.getElementById("description");
    CKEDITOR.replace(description);

    CKEDITOR.config.allowedContent = true;
</script>
@endsection
