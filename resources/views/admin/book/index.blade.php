@extends('layouts.admin')

@section('contents')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Daftar Buku
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="m-section__content">
                                @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert"></button> 
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                @if ($message = Session::get('error'))
                                <div class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert"></button> 
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                <table class="table m-table m-table--head-bg-success table-hover" style="font-size:12px;">
                                    <thead>
                                        <tr>
                                            <th>&nbsp;&nbsp;No.</th>
                                            <th>Kategori</th>
                                            <th>Gambar</th>
                                            <th>Judul</th>
                                            <th>Penulis</th>
                                            <th>Cover</th>
                                            <th>Harga</th>
                                            <th>Diskon</th>
                                            <th>Stok</th>
                                            <th>Publish</th>
                                            <th>Edit</th>
                                            <!--<th>Hapus</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $inc = 1 @endphp
                                        @foreach ($model as $row)
                                        <tr>
                                            <th scope="row">&nbsp;&nbsp;&nbsp;&nbsp;{{ sprintf('%03d', $inc) }}</th>
                                            <td>{{ isset($row->category) ? $row->category->title : '' }}</td>
                                            <td>
                                                <img src="{{ asset('uploads/' . $row->image_url) }}" alt="{{ $row->title }}" width="22" height="32">
                                            </td>
                                            <td>{{ $row->title }}</td>
                                            <td>{{ isset($row->author) ? $row->author->name : '' }}</td>
                                            <td>{{ $row->cover }}</td>
                                            <td>Rp {{ number_format($row->price, 0, '', '.') }}</td>
                                            <td>{{ $row->discount }}</td>
                                            <td>{{ $row->stock }}</td>
                                            <td>{{ $row->status == 1 ? 'ON' : 'OFF'  }}</td>
                                            <td align="center">
                                                <a href="{{ route('admin.book.edit', $row->id) }}">
                                                    <span> <i class="fa flaticon-edit-1"></i> <span>
                                                </a>
                                            </td>
                                            <!--<td align="center">-->
                                            <!--    <a class="remove" data-id="{{ $row->id }}" href="javascript:;">-->
                                            <!--        <span>-->
                                            <!--            <i class="fa flaticon-delete-2"></i>-->
                                            <!--        </span>-->
                                            <!--    </a>-->
                                            <!--</td>-->
                                        </tr>
                                            @php $inc++ @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--end::Section-->
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
            <!--End::Section-->
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('.table').DataTable({
        pageLength: 20,
        dom: 'Bfrtip',
    });
</script>
@endsection