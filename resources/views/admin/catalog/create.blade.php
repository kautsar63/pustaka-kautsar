@extends('layouts.admin')

@section('contents')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Tambah Katalog
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form" method="POST" action="{{ route('admin.catalog.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Judul Katalog
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control m-input" name="title" placeholder="Masukkan Judul Katalog" required>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Sub Judul Katalog
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control m-input" name="sub_title" placeholder="Masukkan Sub Judul Katalog" required>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Upload Image
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="file" id="image_url" class="form-control m-input" name="image_url" placeholder="Upload Gambar Buku" accept=".jpg, .jpeg, .png, image/jpeg, image/png">
                                        <small>
                                            ukuran yang diterima: width 320 pixel, height 240 pixel, quality 80<br>
                                            ukuran file maksimal adalah 5MB<br>
                                            tipe file: jpeg, jpg dan png
                                        </small>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Upload Katalog atau Naskah
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="file" id="download_url" class="form-control m-input" name="download_url" placeholder="Upload Gambar Buku" accept=".pdf, .doc, .docx, .xls, .xlsx, .txt, application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf">
                                        <small>
                                            ukuran file maksimal adalah 10MB<br>
                                            tipe file: pdf, doc, docx, txt
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-success" id="m_sweetalert_demo_3_3">
                                            Simpan
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Batal
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
</div>
@endsection

@section('scripts')
<script>
    var uploadImage = document.getElementById("image_url");

    uploadImage.onchange = function() {
        alert(this.files[0].size)
        if(this.files[0].size > 5120000) {
           alert("File is too big!");
           this.value = "";
        };
    };

    var uploadFile = document.getElementById("download_url");

    uploadFile.onchange = function() {
        alert(this.files[0].size)
        if(this.files[0].size > 10240000) {
           alert("File is too big!");
           this.value = "";
        };
    };
</script>
@endsection