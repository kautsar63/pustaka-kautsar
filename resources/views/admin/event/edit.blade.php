@extends('layouts.admin')

@section('contents')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Edit Event
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form" method="post" action="{{ route('admin.event.update', $model->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Judul Event
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control m-input" name="title" value="{{ old('title', $model->title) }}" placeholder="Masukkan Judul Event" required>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Lokasi Event
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control m-input" name="place" value="{{ old('place', $model->place) }}" placeholder="Masukkan Lokasi Event" required>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Start Date
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="date" class="form-control m-input" name="start_date" value="{{ old('start_date', $model->start_date) }}" placeholder="Masukkan Start Date">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Expired
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="date" class="form-control m-input" name="end_date" value="{{ old('end_date', $model->end_date) }}" placeholder="Masukkan Expired Event">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Booth
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control m-input" name="booth" value="{{ old('booth', $model->booth) }}" placeholder="Masukkan Booth" required>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Upload Image
                                    </label>
                                    <div class="col-lg-6">
                                        @if ($model->image_url)
                                        <img src="{{ asset('uploads/' . $model->image_url) }}" alt="{{ $model->title }}" width="320" height="240">
                                        <input type="hidden" name="is_image" value="{{ $model->image_url }}">
                                        @endif
                                        <input type="file" class="form-control m-input" name="image_url" placeholder="Upload Gambar Buku" accept=".jpg, .jpeg, .png, image/jpeg, image/png">
                                        <small>
                                            ukuran yang diterima: width 320 pixel, height 240 pixel, quality 80<br>
                                            tipe file: jpeg, jpg dan png
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-success" id="m_sweetalert_demo_3_3">
                                            Simpan
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Batal
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
</div>
@endsection