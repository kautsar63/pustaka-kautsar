@extends('layouts.admin')

@section('contents')
<!--Begin::Section-->
<div class="row">
    <div class="col-xl-10">
        <!--begin:: Widgets/Blog-->
        <div class="m-widget19">
            <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides" style="min-height-: 286px">
                <img src="{{ asset('assets/app/media/img/img-page-admin.jpg') }}" alt="">
            </div>
        </div>
        <!--end:: Widgets/Blog-->
    </div>
</div>
<!--End::Section-->
@endsection