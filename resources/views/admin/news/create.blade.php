@extends('layouts.admin')

@section('contents')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Tambah Berita
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form" method="POST" action="{{ route('admin.news.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Judul Berita
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control m-input" name="title" placeholder="Masukkan Judul Berita" required>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Isi Berita
                                    </label>
                                    <div class="col-lg-6">
                                        <textarea class="form-control m-input" id="body" name="body" placeholder="Masukkan Isi Berita" style="height: 100px"></textarea>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Status
                                    </label>
                                    <div class="col-lg-6">
                                        <label class="radio-inline">
                                            <input type="radio" class="form-control m-input" name="status" value="0" checked>
                                            Inactive
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="form-control m-input" name="status" value="1">
                                            Active
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Upload Image
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="file" class="form-control m-input" name="image_url" placeholder="Upload Gambar Buku" accept=".jpg, .jpeg, .png, image/jpeg, image/png">
                                        <small>
                                            <i>Ukuran: Width 370px, Height 235px | Tipe File: jpeg, jpg dan png</i>
                                        </small>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Upload Dokumen atau Naskah
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="file" class="form-control m-input" name="download_url" placeholder="Upload Gambar Buku" accept=".pdf, .doc, .docx, .xls, .xlsx, .txt, application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf">
                                        <small>
                                            tipe file: pdf, doc, docx, txt
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-success" id="m_sweetalert_demo_3_3">
                                            Simpan
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Batal
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
</div>
@endsection

@section('scripts')
<script>
    const body = document.getElementById("body");
    CKEDITOR.replace(body);
    CKEDITOR.config.allowedContent = true;
</script>
@endsection