@extends('layouts.admin')

@section('contents')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Edit Berita
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form" method="post" action="{{ route('admin.news.update', $model->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Nama Berita
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control m-input" name="title" value="{{ old('title', $model->title) }}" placeholder="Masukkan Judul Berita" required>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Body
                                    </label>
                                    <div class="col-lg-6">
                                        <textarea class="form-control m-input" id="body" name="body" placeholder="Masukkan Isi Berita" style="height: 100px">{{ old('body', $model->body) }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Status
                                    </label>
                                    <div class="col-lg-6">
                                        <label class="radio-inline">
                                            <input type="radio" class="form-control m-input" name="status" value="0" {{ $model->status == 0 ? 'checked' : '' }}>
                                            Inactive
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="form-control m-input" name="status" value="1" {{ $model->status == 1 ? 'checked' : '' }}>
                                            Active
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Upload Image
                                    </label>
                                    <div class="col-lg-6">
                                        @if ($model->image_url)
                                        <img src="{{ asset('uploads/' . $model->image_url) }}" alt="{{ $model->title }}" width="320" height="240">
                                        <input type="hidden" name="is_image" value="{{ $model->image_url }}">
                                        @endif
                                        <input type="file" class="form-control m-input" name="image_url" placeholder="Upload Gambar Buku" accept=".jpg, .jpeg, .png, image/jpeg, image/png">
                                        <small>
                                            ukuran yang diterima: width 320 pixel, height 240 pixel, quality 80<br>
                                            tipe file: jpeg, jpg dan png
                                        </small>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Upload Dokumen atau Naskah
                                    </label>
                                    <div class="col-lg-6">
                                        @if ($model->download_url)
                                        <a href="{{ asset('uploads/' . $model->download_url) }}">{{ $model->download_url }}</a>
                                        <input type="hidden" name="is_download" value="{{ $model->download_url }}">
                                        @endif
                                        <input type="file" class="form-control m-input" name="download_url" placeholder="Upload Dokumen" accept=".pdf, .doc, .docx, .xls, .xlsx, .txt, application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf">
                                        <small>
                                            tipe file: pdf, doc, docx, xls, xlsx, txt
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-success" id="m_sweetalert_demo_3_3">
                                            Simpan
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Batal
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
</div>
@endsection

@section('scripts')
<script>
    const body = document.getElementById("body");
    CKEDITOR.replace(body);
    CKEDITOR.config.allowedContent = true;
</script>
@endsection