@extends('layouts.admin')

@section('contents')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Edit Transaksi
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form" method="post" action="{{ route('admin.order.update', $model->id) }}">
                        @csrf
                        @method('PUT')
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Kode Transaksi
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control m-input" name="code" value="{{ old('code', $model->code) }}" placeholder="Masukkan Kode Transaksi" readonly>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Sub Total
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="number" class="form-control m-input" name="sub_total" value="{{ old('sub_total', $model->sub_total) }}" placeholder="Masukkan Sub Total Transaksi">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Jasa Kurir
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="number" class="form-control m-input" name="courier_budget" value="{{ old('courier_budget', $model->courier_budget) }}" placeholder="Masukkan Jasa Kurir Transaksi">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Total
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="number" class="form-control m-input" name="total" value="{{ old('total', $model->total) }}" placeholder="Masukkan Total Transaksi">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Status Pembayaran
                                    </label>
                                    <div class="col-lg-6">
                                        <select class="form-control m-input" id="payment_status" name="payment_status">
                                            <option value="0">Pending</option>
                                            <option value="1">Lunas</option>
                                            <option value="2">Tolak</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Status Perjalanan Barang
                                    </label>
                                    <div class="col-lg-6">
                                        <select class="form-control m-input" id="status" name="status">
                                            <option value="0">Pengecekan</option>
                                            <option value="1">Pengepakan</option>
                                            <option value="2">Pengiriman</option>
                                            <option value="3">Diterima</option>
                                            <option value="4">Finish</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-success" id="m_sweetalert_demo_3_3">
                                            Simpan
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Batal
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('#payment_status').val({{ $model->payment_status }})
    $('#status').val({{ $model->status }})
</script>
@endsection