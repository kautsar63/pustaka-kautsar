@extends('layouts.admin')

@section('contents')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Daftar Transaksi
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="m-section__content">
                                @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert"></button> 
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                @if ($message = Session::get('error'))
                                <div class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert"></button> 
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif

                                <table class="datatable table m-table m-table--head-bg-success table-hover" style="font-size:12px;">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama</th>
                                            <th>Kode</th>
                                            <th>Tanggal</th>
                                            <th>Ongkir</th>
                                            <th>Ekspedisi</th>
                                            <th>Berat</th>
                                            <th>Rincian Pesanan</th>
                                            <th>Status</th>
                                            <th>Alamat</th>
                                            <th>Total</th>
                                            {{-- <th>Edit</th>
                                            <th>Hapus</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $inc = 1 @endphp
                                        @foreach ($model as $row)
                                        <tr>
                                            <td scope="row" align="center">{{ sprintf('%03d', $inc) }}</td>
                                            <td>{{ isset($row->user) ? $row->user->name : '' }}</td>
                                            <td>{{ $row->code }}</td>
                                            <td>{{ date('d M Y', strtotime($row->created_at)) }}</td>
                                            <td>Rp {{ number_format($row->courier_budget, 0, '', '.') }}</td>
                                            <td>{{ $row->courier_name }}</td>
                                            <td>{{ number_format($row->weight, 0, '', '.') }} gram</td>
                                            <td>
                                                @php
                                                $html = '';
                                                $total = 1;
                                                foreach ($row->detail as $detail) {
                                                    if ($detail->product) {
                                                        $html .= $detail->product->title . ' (' . $detail->qty . ')<br>';
                                                    }
                                                        
                                                    $total++;
                                                }

                                                echo $html;
                                                @endphp
                                            </td>
                                            <td>
                                                <select class="status" data-id="{{ $row->id }}">
                                                    <option value="0"{{ $row->status == 0 ? ' selected': '' }}>Belum Bayar</option>
                                                    <option value="1"{{ $row->status == 1 ? ' selected': '' }}>Pembayaran Diterima</option>
                                                    <option value="2"{{ $row->status == 2 ? ' selected': '' }}>Pesanan Diproses</option>
                                                    <option value="3"{{ $row->status == 3 ? ' selected': '' }}>Pesanan Dikirim</option>
                                                    <option value="4"{{ $row->status == 4 ? ' selected': '' }}>Pesanan Tiba</option>
                                                </select>
                                            </td>
                                            <td>{{ isset($row->user) ? $row->user->address : '' }}</td>
                                            <td>Rp {{ number_format($row->total, 0, '', '.') }}</td>
                                            {{-- <td align="center">
                                                <a href="{{ route('admin.order.edit', $row->id) }}" class="m-btn m-btn m-btn--icon">
                                                    <span> <i class="fa flaticon-edit-1"></i> <span>
                                                </a>
                                            </td>
                                            <td align="center">
                                                <a href="{{ route('admin.order.destroy', $row->id) }}" method="delete" class="m-btn m-btn m-btn--icon">
                                                    <span> <i class="fa flaticon-delete-2"></i> </span>
                                                </a>
                                            </td> --}}
                                        </tr>
                                            @php $inc++ @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--end::Section-->
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
            <!--End::Section-->
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('.table').DataTable({
        dom: 'Bfrtip',
        buttons: []
    });

    const CSRF_TOKEN = $('input[name="_token"]').val()

    $(document).on('change', '.status', function (e) {
        e.preventDefault()
        const url = '{{ route('admin.order.status') }}'

        $.ajax({
            method: 'PUT',
            url: url,
            headers: {
                'X-CSRF-TOKEN': jQuery('input[name="_token"]').val()
            },
            data: {
                _token: CSRF_TOKEN,
                id: $(this).data('id'),
                value: $(this).val(),
            },
            dataType:'json',
            success: function (data) {
                if( data.success == true ) {
                    location.reload();
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        })
    })
</script>
@endsection