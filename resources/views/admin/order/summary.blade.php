@extends('layouts.admin')

@section('contents')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Daftar Transaksi
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="m-section__content">
                                @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert"></button> 
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                @if ($message = Session::get('error'))
                                <div class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert"></button> 
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                @csrf

                                <div class="row">
                                    <div class="col-7"></div>
                                    <div class="col-5">
                                        <form id="form_summary" method="get">
                                            @csrf
                                            <div class="row">
                                                <div class="col-8">                                                
                                                    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                                        <i class="fa fa-calendar"></i>&nbsp;
                                                        <span></span> <i class="fa fa-caret-down"></i>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <input type="hidden" name="start_date" id="start_date">
                                                    <input type="hidden" name="end_date" id="end_date">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Cek</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="row" style="margin-bottom: 30px">
                                    <canvas id="myChart" width="100%" height="30"></canvas>
                                </div>

                                <table class="datatable table m-table m-table--head-bg-success table-hover" style="font-size:12px;">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Member</th>
                                            <th>Kode</th>
                                            <th>Tanggal</th>
                                            <th>Rincian Pesanan</th>
                                            <th>Status</th>
                                            <th>Ekspedisi</th>
                                            <th>No.Telepon</th>
                                            <th>Alamat</th>
                                            <th>Total</th>
                                            {{-- <th>Edit</th>
                                            <th>Hapus</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $inc = 1 @endphp
                                        @foreach ($orders as $row)
                                        <tr>
                                            <td scope="row" align="center">{{ sprintf('%03d', $inc) }}</td>
                                            <td>{{ isset($row->user) ? $row->user->name : '' }}</td>
                                            <td>{{ $row->code }}</td>
                                            <td>{{ date('d M Y', strtotime($row->created_at)) }}</td>
                                            <td>
                                                @php
                                                $html = '';
                                                $total = 1;
                                                foreach ($row->detail as $detail) {
                                                    if ($detail->product) {
                                                        $html .= $detail->product->title . ' (' . $total . ')<br>';
                                                    }
                                                        
                                                    $total++;
                                                }

                                                echo $html;
                                                @endphp
                                            </td>
                                            <td>
                                                @switch ($row->status)
                                                    @case(0)
                                                        Belum Bayar
                                                        @break
                                                    @case(1)
                                                        Pembayaran Diterima
                                                        @break
                                                    @case(2)
                                                        Pesanan Diproses
                                                        @break
                                                    @case(3)
                                                        Pesanan Dikirim
                                                        @break
                                                    @case(4)
                                                        Pesanan Tiba
                                                        @break
                                                @endswitch
                                            </td>
                                            <td>{{ $row->courier_name }}</td>
                                            <td>{{ isset($row->user) ? $row->user->phone : '' }}</td>
                                            <td>{{ isset($row->user) ? $row->user->address : '' }}</td>
                                            <td>Rp {{ number_format($row->total, 0, '', '.') }}</td>
                                            {{-- <td align="center">
                                                <a href="{{ route('admin.order.edit', $row->id) }}" class="m-btn m-btn m-btn--icon">
                                                    <span> <i class="fa flaticon-edit-1"></i> <span>
                                                </a>
                                            </td>
                                            <td align="center">
                                                <a href="{{ route('admin.order.destroy', $row->id) }}" method="delete" class="m-btn m-btn m-btn--icon">
                                                    <span> <i class="fa flaticon-delete-2"></i> </span>
                                                </a>
                                            </td> --}}
                                        </tr>
                                            @php $inc++ @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--end::Section-->
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
            <!--End::Section-->
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var ctx = document.getElementById('myChart');
    const labels = {!! json_encode($labels) !!}
    const datas = {!! json_encode($datas) !!}

    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: 'Total Penjualan',
                data: datas,
                lineTension: 0.3,
                borderWidth: 1,
                borderColor: 'orange',
                pointBorderWidth: 5,
                fill: false
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    $(function() {
        var start = moment('{{ $start_date }}');
        var end = moment('{{ $end_date }}');

        function cb(start, end) {
            $('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
            $('#start_date').val(start)
            $('#end_date').val(end)
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);
    });

    $('.datatable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    const CSRF_TOKEN = $('input[name="_token"]').val()
</script>
@endsection