@extends('layouts.admin')

@section('contents')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Edit Promo
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form" method="post" action="{{ isset($model) ? route('admin.promo.update', $model->id) : route('admin.promo.store') }}" enctype="multipart/form-data">
                        @csrf
                        @if (isset($model))
                        @method('PUT')
                        <input type="hidden" name="id" value="{{ $model->id }}">
                        @endif
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">
                                @if (isset($model))
                                @else
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Tipe
                                    </label>
                                    <div class="col-lg-6">
                                        <select class="form-control m-input" name="title">
                                            <option value="flash sale">FLASH SALE</option>
                                            <option value="promo bulan ini">PROMO BULAN INI</option>
                                            <option value="buku baru">BUKU BARU</option>
                                            <option value="best sellers">BEST SELLERS</option>
                                            <option value="buku anak">BUKU ANAK</option>
                                        </select>
                                    </div>
                                </div>
                                @endif
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Tanggal Mulai
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control datepicker m-input" name="start_date" value="{{ old('start_date', isset($model->start_date) ? $model->start_date : date('Y-m-d')) }}" placeholder="Masukkan Start Date">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Tanggal Berakhir
                                    </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control datepicker m-input" name="end_date" value="{{ old('end_date', isset($model->end_date) ? $model->end_date : date('Y-m-d')) }}" placeholder="Masukkan Expired Promo">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Publish
                                    </label>
                                    <div class="col-lg-6">
                                        <label class="radio-inline">
                                            <input type="radio" class="form-control m-input" name="status" value="0" {{ isset($model->status) && $model->status == 0 ? 'checked' : '' }}>
                                            Tidak Aktif
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="form-control m-input" name="status" value="1" {{ isset($model->status) && $model->status == 1 ? 'checked' : '' }}>
                                            Aktif
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-9">
                                        <button id="add_book" class="form-control btn btn-primary">Tambah Buku</button>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-9">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th style="width: 50%">Judul Buku</th>
                                                    <th style="width: 30%">Diskon (%)</th>
                                                    <th style="width: 20%">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody id="book_items">
                                                @if (isset($model->products))
                                                    @foreach ($model->products as $row)
                                                    <tr>
                                                        <td style="width: 50%">
                                                            <select name="books[]" class="book">
                                                                @foreach ($data as $key => $val)
                                                                <option value="{{ $val['id'] }}" {{ $row->product['id'] == $val['id'] ? 'selected' : '' }}>{{ $val['title'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td style="width: 30%"><input class="form-control m-input" type="number" name="discount[]" value="{{ $row->product['discount'] }}" class="discount"></td>
                                                        <td style="width: 20%"><button class="btn btn-primary btn-xs remove"><i class="fa flaticon-delete-2"></i> Hapus</button></td>
                                                    </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-success" id="m_sweetalert_demo_3_3">
                                            Simpan
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Batal
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
</div>
@endsection

@section('scripts')
<script>
    const base_url = '{{ url('/') }}'

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    })

    $('select').select2()

    let index = 1
    $('#add_book').on('click', function (e) {
        e.preventDefault()

        let html = `
            <tr>
                <td><select name="books[]" id="book_${index}" class="book"></select></td>
                <td><input type="number" name="discount[]" id="discount_${index}" class="discount"></td>
                <td><button class="btn btn-danger btn-xs remove">Hapus</button></td>
            </tr>
        `

        $('#book_items').append(html)

        loadBooks(index)
        index++
    })

    const loadBooks = function (i) {
        $book_id = $('#book_' + i)

        $.get(base_url + '/admin/book/select', function (data) {
            for (var i = 0; i < data.length; i++) {
                $book_id.append('<option value=' + data[i].id + '>' + data[i].title +' </option>');
            }
            $book_id.change();
        })
    }

    $(document).on('click', '.remove', function (e) {
        e.preventDefault()

        index--
        $(this).parent().parent().remove()
    })
</script>
@endsection