@extends('layouts.admin')

@section('contents')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Daftar Promo
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="m-section__content">
                                @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert"></button> 
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                @if ($message = Session::get('error'))
                                <div class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert"></button> 
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                <table class="table m-table m-table--head-bg-success table-hover" style="font-size:12px;">
                                    <thead>
                                        <tr>
                                            <th>&nbsp;&nbsp;No.</th>
                                            <th>Judul</th>
                                            <th>Link</th>
                                            <th>Start Date</th>
                                            <th>Expired</th>
                                            <th>Status</th>
                                            <th>Edit</th>
                                            <!--<th>Hapus</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $inc = 1 @endphp
                                        @foreach ($model as $row)
                                        <tr>
                                            <th scope="row">&nbsp;&nbsp;&nbsp;&nbsp;{{ sprintf('%03d', $inc) }}</th>
                                            <td>{{ $row->title }}</td>
                                            <td>{{ $row->link }}</td>
                                            <td>{{ $row->start_date }}</td>
                                            <td>{{ $row->end_date }}</td>
                                            <td>{{ $row->status == 1 ? 'Active' : 'Inactive' }}</td>
                                            <td align="center">
                                                <a href="{{ route('admin.promo.edit', $row->id) }}">
                                                    <span> <i class="fa flaticon-edit-1"></i> <span>
                                                </a>
                                            </td>
                                            <!--<td align="center">-->
                                            <!--    <a href="{{ route('admin.promo.destroy', $row->id) }}" class="delete" method="delete">-->
                                            <!--        <span> <i class="fa flaticon-delete-2"></i> </span>-->
                                            <!--    </a>-->
                                            <!--</td>-->
                                        </tr>
                                            @php $inc++ @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--end::Section-->
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
            <!--End::Section-->
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('.table').DataTable({
        dom: 'Bfrtip',
    });
    
    // $(document).on('click', '.delete', function (e) {
    //     e.preventDefault()
        
    //     const c = confirm('Anda yakin mau menghapus data ini?')
    //     if (c == true) {
    //         $.ajax({
    //             method: 'delete',
    //             url: $(this).attr('href'),
    //             data: {
    //                 '_token': '{{ csrf_token() }}',
    //             },
    //             cache: false,
    //             success: function (result) {
    //                 if (result.success == true)
    //                     location.href = '{{ route('admin.promo.index') }}'
    //             }
    //         })
    //     }
    // })
</script>
@endsection