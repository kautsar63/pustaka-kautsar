@extends('layouts.admin')

@section('contents')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Daftar Slider
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="m-section__content table-responsive">
                                @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert"></button> 
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                @if ($message = Session::get('error'))
                                <div class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert"></button> 
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                <table class="table m-table m-table--head-bg-success" style="font-size:12px;">
                                    <thead>
                                        <tr>
                                            <th>&nbsp;&nbsp;No.</th>
                                            <th>Image</th>
                                            <th>Judul</th>
                                            <th>SubJudul</th>
                                            <th>Deskripsi</th>
                                            <th>Link</th>
                                            <th>Edit</th>
                                            <th>Hapus</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $inc = 1 @endphp
                                        @foreach ($model as $row)
                                        <tr>
                                            <th scope="row">&nbsp;&nbsp;&nbsp;&nbsp;{{ sprintf('%03d', $inc) }}</th>
                                            <td>
                                                <img src="{{ asset('uploads/' . $row->image_url) }}" alt="{{ $row->title }}" width="32" height="22">
                                            </td>
                                            <td>{{ $row->title }}</td>
                                            <td>{{ $row->sub_title }}</td>
                                            <td>{{ $row->description }}</td>
                                            <td>{{ $row->link }}</td>
                                            <td align="center">
                                                <a href="{{ route('admin.slider.edit', $row->id) }}" type="button">
                                                    <span> <i class="fa flaticon-edit-1"></i> <span>
                                                </a>
                                            </td>
                                            <td align="center">
                                                <a href="{{ route('admin.slider.destroy', $row->id) }}" class="delete" method="delete">
                                                    <span> <i class="fa flaticon-delete-2"></i> </span>
                                                </a>
                                            </td>
                                        </tr>
                                            @php $inc++ @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--end::Section-->
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
            <!--End::Section-->
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('.table').DataTable({
        dom: 'Bfrtip',
    });
    
    $(document).on('click', '.delete', function (e) {
        e.preventDefault()
        
        const url = $(this).attr('href')
        
        $.ajax({
            url: url,
            data: {
                _token: '{{ csrf_token() }}'
            },
            cache: false,
            type: 'DELETE',
            success: function (result) {
                // if (result.status == 200)
                    window.location.href = '/admin/slider'
            }
        })
    })
</script>
@endsection