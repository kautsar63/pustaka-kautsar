@extends('layouts.app')

@section('content')
<!--====================  hero slider area ====================-->

<div class="section-space" style="margin-top:-55px;">
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-6">
                <!-- Login Form s-->
                <form action="{{ route('login') }}" method="POST">
                    @csrf

                    <div class="login-form">
                        <h4 class="login-title">Login Member</h4>

                        <div class="row">
                            <div class="col-md-12 col-12">
                                <label>Email*</label>
                                <input type="email" name="email" placeholder="Email Address" required>
                            </div>
                            <div class="col-12">
                                <label>Password</label>
                                <input type="password" name="password" placeholder="Password" required>
                            </div>

                            <div class="col-sm-6 text-left text-sm-left">
                                <a href="{{ route('forgot-password') }}" class="forget-pass-link"> Lupa password?</a>
                            </div>

                            <div class="col-md-12">
                                <button type="submit" class="register-button2">Login</button>
                            </div>

                            <div class="col-md-12">
                                <p style="font-size:13px; padding-top:15px; line-height:24px;">Kesulitan login? Hubungi kami:<br>
                                Telp: (021) 8507 590 / WA: 081-888-888-888</p>

                            </div>

                        </div>
                    </div>

                </form>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-6 col-xs-12">
                <form action="{{ route('register') }}" method="POST">
                    @csrf
                    <input type="hidden" id="city_id" name="city_id" value="">
                    <input type="hidden" id="district_id" name="district_id" value="">
                    <input type="hidden" id="region" name="region" value="">

                    <div class="login-form">
                        <h4 class="login-title">Registrasi Member</h4>

                        <div class="row">
                            <div class="col-md-12 mb-20">
                                <label>Nama</label>
                                <input type="text" name="name" placeholder="Masukkan Nama" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            </div>
                            <div class="col-md-12 mb-20">
                                <label>Email</label>
                                <input type="email" name="email" placeholder="Masukkan Alamat Email" value="{{ old('email') }}" required>
                            </div>
                            <div class="col-md-6 mb-20">
                                <label>Password</label>
                                <input type="password" name="password" placeholder="Masukkan Password" required>
                            </div>
                            <div class="col-md-6 mb-20">
                                <label>Konfirmasi Password</label>
                                <input type="password" name="password_confirmation" placeholder="Konfirmasi Password" required>
                            </div>
                            <div class="col-md-12 mb-20">
                                <label>Nomor Telepon</label>
                                <input type="text" name="phone" placeholder="Masukkan Nomor Telepon / HP" value="{{ old('phone') }}" required>
                            </div>
                            <div class="col-md-12 mb-20">
                                <label>Propinsi</label>
                                <select class="nice-select" name="region_id" id="region_id" required>
                                    @foreach ($province as $pro)
                                        <option value="{{ $pro->id }}" data-name="{{ $pro->name }}">{{ $pro->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12 mb-20">
                                <label>Kota</label>
                                <select class="nice-select" name="city" id="city" required></select>
                            </div>
                            <div class="col-md-12 mb-20">
                                <label>Kecamatan</label>
                                <select class="nice-select" name="district" id="district" required></select>
                            </div>
                            <div class="col-md-12 mb-20">
                                <label>Kode Pos</label>
                                <input type="text" id="zipcode" name="zipcode" placeholder="Masukkan Kode Pos" value="{{ old('zipcode') }}">
                            </div>
                            <div class="col-md-12 mb-20">
                                <label>Alamat Lengkap</label>
                                <input type="text" name="address" placeholder="Masukkan Alamat Lengkap" value="{{ old('address') }}" required>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="register-button2 mt-0">Daftar</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection

@section('scripts')
<script>
    $('#region_id').on('change', function () {
        $('#city').html('')
        let city_id = 0
        $.getJSON(base_url + '/get-city/' + $(this).val(), function (items) {
            let html = ''
            for (let i = 0; i < items.length; i++)
                html += '<option value="' + items[i].name + '" data-zipcode="' + items[i].postal_code + '" data-id="' + items[i].id + '">' + items[i].type + ' - ' + items[i].name + '</option>'

            city_id = items[0].id
            setCityId(city_id)
            $('#city').append(html)
            
            $.getJSON(base_url + '/get-district/' + city_id, function (items) {
                const data = items.rajaongkir.results
                console.log('items', data)
                let html = ''
                let district_id = 0
                for (let i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].subdistrict_name + '" data-id="' + data[i].subdistrict_id + '">' + data[i].subdistrict_name + '</option>'
                }
    
                district_id = data[0].subdistrict_id
                setDistrictId(district_id)
                $('#district').append(html)
            })
        })

        setCityId($(this).val())
        $('#region').val($(this).find(':selected').data('name'))
    }).trigger('change')

    function setCityId (value) {
        $(document).find('#city_id').val(value)
    }

    function setDistrictId (value) {
        console.log('district', value)
        $(document).find('#district_id').val(value)
    }

    $('#city').on('change', function () {
        const zipcode = $(this).find(':selected').data('zipcode')
        const city_id = $(this).find(':selected').data('id')

        $('#district').html('')
        $.getJSON(base_url + '/get-district/' + city_id, function (items) {
            const data = items.rajaongkir.results
            console.log('items', data)
            let html = ''
            let district_id = 0
            for (let i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].subdistrict_name + '" data-id="' + data[i].subdistrict_id + '">' + data[i].subdistrict_name + '</option>'
            }

            district_id = data[0].subdistrict_id
            setDistrictId(district_id)
            $('#district').append(html)
        })

        $('#zipcode').val(zipcode)
    }).trigger('change')
    
    $('#district').on('change', function () {
        document.getElementById('district_id').value = $(this).find(':selected').data('id')
    }).trigger('change')
</script>
@endsection