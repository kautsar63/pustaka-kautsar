@extends('layouts.app')

@section('content')
<div class="section-space" style="margin-top:-55px;">
</div>

<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
          <div class="col-lg-12">

            <!--=============================================
            =            google map container         =
            =============================================-->

            <div class="google-map-container">
                <div id="google-map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1983.1372659077554!2d106.8884477!3d-6.22749!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3428f1b5e93%3A0x433e81cef5580a8b!2sJl.%20Cipinang%20Muara%20Raya%20No.63%2C%20RT.14%2FRW.3%2C%20Cipinang%20Muara%2C%20Kecamatan%20Jatinegara%2C%20Kota%20Jakarta%20Timur%2C%20Daerah%20Khusus%20Ibukota%20Jakarta%2013420!5e0!3m2!1sid!2sid!4v1592321168490!5m2!1sid!2sid" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>

            <!--=====  End of google map container  ======-->

            <div class="row">
                <div class="col-lg-5 offset-lg-1 col-md-12 order-1 order-lg-2">
                    <!--=======  contact page side content  =======-->

                    <div class="contact-page-side-content">
                        <h3 class="contact-page-title">Hubungi Kami di:</h3>
                        <!--=======  single contact block  =======-->

                        <div class="single-contact-block">
                            <h4><i class="fa fa-fax"></i> Alamat</h4>
                            <p>Jalan Cipinang Muara Raya No. 63, Jakarta Timur 13420, Indonesia</p>
                        </div>

                        <!--=======  End of single contact block  =======-->

                        <!--=======  single contact block  =======-->

                        <div class="single-contact-block">
                            <h4><i class="fa fa-phone"></i> Telepon dan Fax</h4>
                            <p>Telepon: +6221 850 7590 / 850 6702</p>
                            <p>Fax: +6221 8591 2403</p>
                        </div>

                        <!--=======  End of single contact block  =======-->

                        <!--=======  single contact block  =======-->

                        <div class="single-contact-block">
                            <h4><i class="fa fa-envelope-o"></i> Email</h4>
                            <p>marketing@kautsar.co.id</p>
                            <p>redaksi@kautsar.co.id</p>
                        </div>

                        <!--=======  End of single contact block  =======-->
                    </div>

                    <!--=======  End of contact page side content  =======-->

                </div>
                <div class="col-lg-6 col-md-12 order-2 order-lg-1">
                    <!--=======  contact form content  =======-->

                    <div class="contact-form-content">
                        @if (Session::has('success'))
                        <div style="background: #f2f2f2; padding: 10px; width: 100%; font-weight: bold">
                            {{ Session::get('success') }}
                        </div>
                        @endif
                        <h3 class="contact-page-title">Beritahu kami pesan Anda:</h3>

                        <div class="contact-form">
                            <form id="contact-form" action="{{ route('message') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>Nama <span class="required">*</span></label>
                                    <input type="text" name="name" id="name" required>
                                </div>
                                <div class="form-group">
                                    <label>Email <span class="required">*</span></label>
                                    <input type="email" name="email" id="email" required>
                                </div>

                                <div class="form-group">
                                    <label>Pesan</label>
                                    <textarea name="message" id="message"></textarea>
                                </div>
                                <div class="form-group mb-0">
                                    <button type="submit" value="submit" id="submit" class="contact-button" name="submit">Kirim Pesan</button>
                                </div>
                            </form>
                        </div>
                        <p class="form-messege"></p>
                    </div>

                    <!--=======  End of contact form content =======-->
                </div>
            </div>


          </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection
