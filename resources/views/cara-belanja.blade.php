@extends('layouts.app')

@section('content')
<div class="section-space" style="margin-top:-55px;">
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
          <div class="col-lg-12"><br>
            <div class="shop-header2">
              <div class="shop-header__left">
                  <div class="shop-header__left__message">
                      <span style="font-size:20px; font-weight:600; color:#033147;">Cara Belanja</span>
                  </div>
              </div>
            </div>
              <div class="about-top-content-wrapper">
                  <div class="row row-30">

                      <!-- About Content -->
                      <div class="about-content col-lg-12">
                          <div class="row">
                              <div class="col-12">
                                  <p class="mb-3" style="font-size:13px; text-align:justify;">Selamat datang di website Pustaka Al-Kautsar. Kami sangat gembira Anda bersedia meluangkan waktu melakukan pemesanan produk kami melalui layanan yang sudah kami perbaharui.
                                    Kami mengupayakan layanan pemesanan melalui website ini menjadi lebih mudah dan cepat.</p>

                                  <p class="mb-3" style="font-size:13px; text-align:justify;">Untuk melakukan pemesanan buku, Anda dapat mengikuti petunjuk berikut :</p>

                                  <p class="mb-3" style="font-size:13px; text-align:justify;">1. Pilih judul buku yang ingin Anda pesan. Pemesanan buku dapat dilakukan di halaman "BERANDA atau "KATALOG" dengan cara mengarahkan kursor / klik ke gambar buku kemudian klik icon Keranjang disebelah kiri atau klik judul kemudian pilih "Masukan Keranjang" maka otomatis buku berada dalam keranjang belanja.</p>

                                  <p class="mb-3" style="font-size:13px; text-align:justify;">2. Periksa keranjang belanja dengan cara meng-klik “ICON KERANJANG BELANJA” yang ada di bagian kanan atas website.
                                    Periksa judul yang muncul dalam daftar keranjang belanja apakah sesuai dengan judul yang Anda pilih sebelumnya.
                                     transaksi dapat dilanjutkan dengan meng-klik tombol “LIHAT KERANJANG BELANJA ATAU CHECKOUT” yang berada di bagian bawah daftar buku pesanan anda.
                                     Jika terdapat judul buku yang tidak Anda kehendaki, Anda dapat menghapus dengan mengklik tombol hapus di bagian paling kanan daftar keranjang yang terdapat di Keranjang Belanja anda</p>

                                  <p class="mb-3" style="font-size:13px; text-align:justify;">3. Cek kembali pemesanan dihalaman keranjang belanja. Jika sudah selesai, klik "LANJUTKAN PEMBAYARAN".</p>
                                  <p class="mb-3" style="font-size:13px; text-align:justify;">4. Jika ANDA sudah memiliki akun, silakan login.
                                    Isi data dirimu dengan lengkap. Jangan lupa pilih metode pengiriman dan metode Pembayaran.
                                    Selanjutnya silakan lakukan pengecekan ulang pesanan mulai dari item hingga alamat dan pastikan data yang kamu masukkan sudah sesuai. Jika sudah klik Pilih "BAYAR SEKARANG".</p>
                                  <p class="mb-3" style="font-size:13px; text-align:justify;">5. Bayar biaya pesanan buku sesuai jumlah yang tertera dalam pesanan melalui transfer ke rekening: <br>
                                    BCA No. 005.3409.153 A/N DRS. TOHIR BAWAZIR,<br>
                                    atau BNI SYARIAH No. 00924.95.620 A/N PUSTAKA ALKAUTSAR,<br>
                                    atau MANDIRI No. 006008.0000080 A/N DRS. TOHIR BAWAZIR.
                                  </p>
                                  <p class="mb-3" style="font-size:13px; text-align:justify;">6. Konfirmasi bukti transfer dengan alternatif cara berikut : <br>
                                    1. Faks bukti transfer ke nomor : 021-85912403, atau <br>
                                    2. Kirim balik (reply) email pemberitahuan pesanan dengan mencantumkan nomor resi transfer, jumlah nominal transfer dan nomor rekening transfer. atau <br>
                                    3. Telepon ke layanan pelanggan di nomor 021-8507590 pada  jam kantor. <br>
                                    4. SMS nomor resi transfer, nomor pesanan dan nominal transfer ke nomor 0819 5238 0191,082112024909
                                  </p>
                                  <p class="mb-3" style="font-size:13px; text-align:justify;">Terima kasih sudah berbelanja diPustaka Al-Kautsar! Selamat membaca :)
                                  </p>

                              </div>

                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection
