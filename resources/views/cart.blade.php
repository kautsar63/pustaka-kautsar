@extends('layouts.app')

@section('content')
<!--====================  hero slider area ====================-->

<div class="section-space" style="margin-top:-55px;">
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><br>
                <div class="shop-header2">
                    <div class="shop-header__left">
                        <div class="shop-header__left__message">
                            <span style="font-size:20px; font-weight:600; color:#033147;">KERANJANG BELANJA</span>
                        </div>
                    </div>
                </div>

                <div class="about-top-content-wrapper">
                    <div class="row row-30">

                        <div class="col-lg-8">

                            <form action="{{ url('/checkout') }}" method="POST" id="postCart">
                                @csrf
                                <input type="hidden" id="total_qty" name="total_qty">
                                <input type="hidden" id="total_price" name="total_price">
                                <!--=======  cart table  =======-->
                                <div class="cart-table table-responsive">
                                    <table class="table">
                                        <tbody id="carts"></tbody>
                                    </table>
                                </div>
                                <!--=======  End of cart table  =======-->
                            </form>
                        </div>

                        <div class="col-lg-4">
                            <div class="row">
                                <!-- Cart Total -->
                                <div class="col-12">
                                    <div class="checkout-cart-total">

                                        <h5>Total <span class="counter">0</span> Jumlah Buku 
                                            <span style="float:right; color:#e33; font-weight:600;"><span class="total_cart">0</span></span>
                                        </h5>
                                        <br>
                                        <a href="javascript:;" class="checkout btn btn-primary btn-lg btn-block" role="button" aria-disabled="true">Lanjutkan Pembayaran</a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection

@section('scripts')
<script>
    $('.checkout').on('click', function (e) {
        e.preventDefault()

        $('#postCart').submit()
    })
</script>
@endsection