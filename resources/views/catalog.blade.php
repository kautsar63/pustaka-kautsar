@extends('layouts.app')

@section('content')
<!--====================  single row slider ====================-->
<div class="single-row-slider-area section-space">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--=======  shop page header  =======-->
                <div class="shop-header">
                    <div class="shop-header__left">
                        <div class="shop-header__left__message">
                            <span style="font-size:20px; font-weight:600; color:#033147;">KATALOG</span>
                        </div>
                    </div>
                </div>
                <!--=======  End of shop page header  =======-->
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <!--=======  shop page content  =======-->
                <div class="shop-page-content">
                    <div class="row shop-product-wrap grid five-column">
                        @foreach ($model as $row)
                        <div class="col">
                            <!--=======  single grid product  =======-->
                            <div class="single-grid-product grid-view-product">
                                <div class="single-grid-product__image">
                                    <a href="#"><img src="{{ asset('uploads/' . $row->image_url) }}" class="img-fluid" alt=""></a>
                                </div>
                                <div class="single-grid-product__content">
                                    <h3 class="single-grid-product__title"> <a href="#">{{ $row->title }}</a></h3>
                                    <p>{{ $row->sub_title }}</p>
                                    <a href="{{ asset('uploads/' . $row->download_url) }}" class="btn btn-success btn-sm">Download Katalog (PDF)</a>
                                </div>
                            </div>
                            <!--=======  End of single grid product  =======-->
                        </div>
                        @endforeach
                    </div>
                </div>

                <!--=======  pagination area =======-->
                {{-- <div class="pagination-area">
                    <div class="pagination-area__left">
                        Menampilkan 1-10 dari 50 Katalog
                    </div>
                    <div class="pagination-area__right">
                        <ul class="pagination-section">
                        <li><a href="#"><</a></li>
                            <li><a class="active" href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">></a></li>
                        </ul>
                    </div>
                </div> --}}
                <!--=======  End of pagination area  =======-->
                <!--=======  End of shop page content  =======-->
            </div>
                </div>
                <!--=======  End of single row slider wrapper  =======-->
            </div>
        </div>
<!--====================  End of single row slider  ====================-->

<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection
