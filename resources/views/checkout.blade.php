@extends('layouts.app')

@section('content')
<!--====================  hero slider area ====================-->

<div class="section-space" style="margin-top:-55px;">
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="about-top-content-wrapper">
                    @if (empty(\Auth::user()))
                    <h1>Silakan login terlebih dahulu</h1>
                    @else
                    <form action="{{ route('summary') }}" method="post" class="checkout-form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                        <input type="hidden" name="payment_status" value="0">
                        <input type="hidden" name="status" value="0">
                        <div class="row row-30">

                            <div class="col-lg-6">

                                <!-- Billing Address -->
                                <div id="billing-form">
                                    <h4 class="checkout-title">Pengiriman</h4>

                                    <div class="row">

                                        <div class="col-12">

                                            <div class="checkout-cart-total">
                                                <h4>Tujuan Pengiriman</h4>
                                                <ul>
                                                    <li><strong>{{ isset($address->member) ? $address->member->name : '' }}</strong></li>
                                                    <li>{{ isset($address->name) ? $address->name : '' }}</li>
                                                    <li>{{ isset($address->address) ? $address->address : '' }}</li>
                                                    <li>{{ isset($address->district) ? $address->district : '' }}</li>
                                                    <li>{{ isset($address->city) ? $address->city : '' }}{{ isset($address->region) ? ', ' . $address->region : '' }}{{ isset($address->postal_code) ? ', ' . $address->postal_code : '' }}</li>
                                                    <li>{{ isset($address->phone) ? $address->phone : '' }}</li>
                                                </ul>
                                                <br>
                                                <input type="hidden" id="district_id" value="{{ $address->district_id }}">
                                                <h4>Metode Pengiriman</h4><br>
                                                <div id="loading">
                                                    <img src="{{ asset('uploads/loading.gif') }}" width="128">
                                                </div>
                                                <div id="courier_check"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="row">

                                    <div class="col-12">

                                        <h4 class="checkout-title">Ringkasan Belanja</h4>

                                        <div class="checkout-cart-total">
                                            <input type="text" 
                                                name="code" 
                                                class="form-control" 
                                                id="code"
                                                value="{{ strtoupper($code) }}"
                                                readonly
                                                style="
                                                    font-size: 24px;
                                                    font-weight: bold;
                                                    text-align: center;
                                                    background: white;
                                                    border: none;
                                                "
                                            />

                                            <ul id="cart-items">
                                                @foreach ($data['items'] as $key => $val)
                                                <li>
                                                    <a href="{{ url('/' . $val['real_link']) }}">{{ $val['title'] }}</a>
                                                    <br>
                                                    Jumlah: {{ $val['qty'] }} <span>Rp. {{ number_format($val['price'], 0, '', '.') }}</span>
                                                    <input type="hidden" name="id[]" value="{{ $key }}" />
                                                    <input type="hidden" name="title[]" value="{{ $val['title'] }}" />
                                                    <input type="hidden" name="qty[]" value="{{ $val['qty'] }}" />
                                                    <input type="hidden" name="price[]" value="{{ $val['price'] }}" />
                                                </li>
                                                @endforeach
                                            </ul>

                                            <p>Sub Total <span class="temp_sub_total" style="color:#ee3356;">Rp. {{ number_format($data['total_price'], 0, '', '.') }}</span></p>
                                            <input type="hidden" name="sub_total" id="sub_total" value="{{ $data['total_price'] }}">
                                            <p>Biaya Pengiriman <span class="temp_courier" style="color:#ee3356;">0</span></p>
                                            <p>Berat <span class="temp_weight">0</span></p>
                                            <input type="hidden" name="weight" id="temp_weight">
                                            <input type="hidden" name="courier" id="temp_courier">
                                            <input type="hidden" name="courier_code" id="temp_courier_code">
                                            <input type="hidden" name="courier_name" id="temp_courier_name">

                                            <br><h4>Total Harga <span class="temp_total" style="color:#ee3356;">0</span></h4> <br>
                                            <input type="hidden" name="total" id="temp_total">

                                            <button type="submit"
                                                class="pay-now btn btn-primary btn-lg btn-block" 
                                            >Bayar Sekarang</button>
                                        </div>
                                    </div>
                                    
                                    <div class="col-12">
                                        <h4 class="checkout-title">Metode Pembayaran</h4>
    
                                        <div class="checkout-payment-method">
    
                                            <div class="single-method">
                                                <input type="radio" id="payment_bca" name="payment-method" value="bca">
                                                <label for="payment_bca">Transfer Bank BCA</label>
                                                <p data-method="bca" style="line-height:50px;">
                                                    <img src="assets/img/icons/icon-bca.png" class="img-fluid" alt="">  BCA No. 005 3409 153 A/N TOHIR BAWAZIR<br>
                                                </p>
                                            </div>
                                            <div class="single-method">
                                                <input type="radio" id="payment_mandiri" name="payment-method" value="mandiri">
                                                <label for="payment_mandiri">Transfer Bank Mandiri</label>
                                                <p data-method="mandiri" style="line-height:50px;">
                                                    <img src="assets/img/icons/icon-mandiri.png" class="img-fluid" alt="">  MANDIRI No. 006 008 0000 080 A/N TOHIR BAWAZIR <br>
                                                </p>
                                            </div>
                                            <div class="single-method">
                                                <input type="radio" id="payment_bsm" name="payment-method" value="bsm">
                                                <label for="payment_bsm">Transfer Bank Mandiri Syariah</label>
                                                <p data-method="bsm" style="line-height:50px;">
                                                    <img src="assets/img/icons/icon-bsm.jpg" class="img-fluid" alt="">  MANDIRI SYARIAH No. 7000 0483 12 A/N TOHIR BAWAZIR <br>
                                                </p>
                                            </div>
                                            <div class="single-method">
                                                <input type="radio" id="payment_bni" name="payment-method" value="bni">
                                                <label for="payment_bni">Transfer Bank BNI Syariah</label>
                                                <p data-method="bni" style="line-height:50px;">
                                                    <img src="assets/img/icons/icon-bni.png" class="img-fluid" alt="">  BNI SYARIAH No. 009 2495 620 A/N PUSTAKA ALKAUTSAR
                                                </p>
                                            </div>
                                            <!--<div class="single-method">
                                                <input type="radio" id="payment_cc" name="payment-method" value="cc">
                                                <label for="payment_cc">Kartu Kredit</label>
                                                <p data-method="cc">Visa, Mastercard, American Express</p>
                                            </div>
                                            <div class="single-method">
                                                <input type="radio" id="payment_virtual" name="payment-method" value="virtual">
                                                <label for="payment_virtual">Transfer Virtual Account</label>
                                                <p data-method="virtual">BCA Virtual Account, Mandiri Virtual Account, BNI Virtual Account</p>
                                            </div>
                                            <div class="single-method">
                                                <input type="radio" id="payment_cash" name="payment-method" value="cash">
                                                <label for="payment_cash">Direct Debit</label>
                                                <p data-method="cash">BCA Kli Pay, Mandiri Click Pay, CIMB Clicks</p>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection

@section('scripts')
@parent
<script>
    const city_id = {!! isset($city_id) ? $city_id : 0 !!}
    const district_id = $('#district_id').val()
    const total_weight = {{ isset($total_weight) ? $total_weight : 0 }}

    function loadCourier (origin, destination, weight, courier) {
        $.ajax({
            url: '{{ route('courier-check') }}',
            method: 'get',
            data: {
                origin: origin,
                destination: destination,
                weight: weight,
                courier: courier
            },
            success: function (data) {
                const result = JSON.parse(data)
                const name = result.name
                const costs = result.costs

                let html = '<h4>' + courier.toUpperCase() + '</h4>'
                for (let i = 0; i < costs.length; i++) {
                    html += `
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" data-weight="${weight}" data-code="${courier}" data-name="${courier.toUpperCase()} - ${costs[i].name}" value="${costs[i].value}" id="courier_${i}">
                            <label class="jne form-check-label" for="defaultCheck1">
                                ${costs[i].name} <span style="float:right; color:#e33;">${formatRupiah(costs[i].value)} (${costs[i].etd} Hari Kerja)</span>
                            </label>
                        </div>
                    `
                }

                html += '<hr>'

                $('#courier_check').append(html)
                $('#loading').html('')
            }
        })
    }

    loadCourier(154, district_id, total_weight, 'jne')
    loadCourier(154, district_id, total_weight, 'pos')
    loadCourier(154, district_id, total_weight, 'wahana')

    $('.temp_weight').html(total_weight + ' gr')
    $('#temp_weight').val(total_weight)

    $(document).on('click', '.form-check-input', function (e) {
        const courier_cost = parseInt($(this).val())
        const sub_total = parseInt($('#sub_total').val().replace('Rp. ', '').replace('.', ''))
        const code = $(this).data('code')
        const name = $(this).data('name')

        total = courier_cost + sub_total

        $('#temp_courier').val(formatRupiah(courier_cost))
        $('#temp_courier_code').val(code)
        $('#temp_courier_name').val(name)
        $('#temp_total').val(formatRupiah(total))

        $('.temp_courier').html(formatRupiah(courier_cost))
        $('.temp_total').html(formatRupiah(total))
    })
    
    $(document).on('click', '.pay-now', function (e) {
        e.preventDefault()
        
        if ($('#temp_courier').val() == 0) {
            alert('Anda belum menentukan jasa kurir pengiriman, silakan lengkapi form anda')
            return false
        }
        
        if ($('input[name="payment-method"]').is(':checked')) {
        } else {
            alert('Anda belum menentukan metode pembayaran, silakan lengkapi form anda')
            return false
        }
        
        const $form = $(this).closest('form')
        
        $form.submit()
    })
</script>
@endsection