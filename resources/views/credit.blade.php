@extends('layouts.app')

@section('content')
<!--====================  hero slider area ====================-->

<div class="section-space" style="margin-top:-40px;">
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="single-row-slider-area section-space">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <div class="shop-header2">
                <div class="shop-header__left">
                    <div class="shop-header__left__message">
                        <span style="font-size:20px; font-weight:600; color:#033147;">PENGHARGAAN</span>
                    </div>
                </div>
            </div>
            <!--=======  blog page content  =======-->
            <div class="blog-page-content">
                <div class="row">

                    <div class="col-md-4">
                        <!--=======  single blog post  =======-->
                        <div class="single-blog-post">
                            <div class="single-blog-post__image">
                                <a href="#">
                                    <img src="assets/img/pameran/islamic-book-fair-2020.jpg" class="img-fluid" alt="">
                                </a>

                            </div>
                            <div class="single-blog-post__content">
                                <h3 class="title"><a href="#">Islamic Book Fair 2020 : “Literasi Islam Cahaya Untuk Negeri“</a></h3>
                            </div>
                        </div>
                        <!--=======  End of single blog post  =======-->
                    </div>

                    <div class="col-md-4">
                        <!--=======  single blog post  =======-->
                        <div class="single-blog-post">
                            <div class="single-blog-post__image">
                                <a href="#">
                                    <img src="assets/img/pameran/malang-book-fair-2020.jpg" class="img-fluid" alt="">
                                </a>

                            </div>
                            <div class="single-blog-post__content">
                                <h3 class="title"><a href="#">Malang Islamic Book Fair 2020</a></h3>
                            </div>
                        </div>
                        <!--=======  End of single blog post  =======-->
                    </div>

                    <div class="col-md-4">
                        <!--=======  single blog post  =======-->
                        <div class="single-blog-post">
                            <div class="single-blog-post__image">
                                <a href="#">
                                    <img src="assets/img/pameran/islamic-book-fair-2020.jpg" class="img-fluid" alt="">
                                </a>

                            </div>
                            <div class="single-blog-post__content">
                                <h3 class="title"><a href="#">Islamic Book Fair 2020 : “Literasi Islam Cahaya Untuk Negeri“</a></h3>
                            </div>
                        </div>
                        <!--=======  End of single blog post  =======-->
                    </div>

                    <div class="col-md-4">
                        <!--=======  single blog post  =======-->
                        <div class="single-blog-post">
                            <div class="single-blog-post__image">
                                <a href="#">
                                    <img src="assets/img/pameran/islamic-book-fair-2020.jpg" class="img-fluid" alt="">
                                </a>

                            </div>
                            <div class="single-blog-post__content">
                                <h3 class="title"><a href="#">Islamic Book Fair 2020 : “Literasi Islam Cahaya Untuk Negeri“</a></h3>
                            </div>
                        </div>
                        <!--=======  End of single blog post  =======-->
                    </div>

                    <div class="col-md-4">
                        <!--=======  single blog post  =======-->
                        <div class="single-blog-post">
                            <div class="single-blog-post__image">
                                <a href="#">
                                    <img src="assets/img/pameran/islamic-book-fair-2020.jpg" class="img-fluid" alt="">
                                </a>

                            </div>
                            <div class="single-blog-post__content">
                                <h3 class="title"><a href="#">Islamic Book Fair 2020 : “Literasi Islam Cahaya Untuk Negeri“</a></h3>
                            </div>
                        </div>
                        <!--=======  End of single blog post  =======-->
                    </div>

                    <div class="col-md-4">
                        <!--=======  single blog post  =======-->
                        <div class="single-blog-post">
                            <div class="single-blog-post__image">
                                <a href="#">
                                    <img src="assets/img/pameran/islamic-book-fair-2020.jpg" class="img-fluid" alt="">
                                </a>

                            </div>
                            <div class="single-blog-post__content">
                                <h3 class="title"><a href="#">Islamic Book Fair 2020 : “Literasi Islam Cahaya Untuk Negeri“</a></h3>
                            </div>
                        </div>
                        <!--=======  End of single blog post  =======-->
                    </div>
                </div>
            </div>

            <!--=======  pagination area =======-->
            <div class="pagination-area">
                <div class="pagination-area__left">
                    Menampilkan 1-6 dari 10 Penghargaan
                </div>
                <div class="pagination-area__right">
                    <ul class="pagination-section">
                    <li><a href="#"><</a></li>
                        <li><a class="active" href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">></a></li>
                    </ul>
                </div>
            </div>
            <!--=======  End of pagination area  =======-->
            <!--=======  End of blog page content  =======-->
            </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection