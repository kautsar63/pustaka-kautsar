@extends('layouts.app')

@section('content')
<div class="section-space" style="margin-top:-55px;"></div>

<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--=======  single product main content area  =======-->
                <div class="single-product-main-content-area section-space">
                    <div class="row">
                        <div class="col-lg-6">
                            <!--=======  product details slider area  =======-->

                            <div class="product-details-slider-area">
                                <div class="big-image-wrapper">
                                    <div class="single-image">
                                        <img src="{{ asset('uploads/' . $model->image_url) }}" class="img-fluid" alt="{{ $model->title }}">
                                    </div>
                                </div>
                            </div>

                            <!--=======  End of product details slider area  =======-->
                        </div>
                        <div class="col-lg-6">
                            <!--=======  single product content description  =======-->
                            <div class="single-product-content-description">
                                
                                <h4 class="product-title">{{ $model->title }}</h4>

                                @php $discount_price = $model->price - ($model->price * ($model->discount / 100)) @endphp
                                <p class="single-grid-product__price"><span class="discounted-price">Rp. {{ number_format($discount_price, 0, '.', ',') }}</span> <span class="main-price discounted">{{ number_format($model->price, 0, '.', ',') }}</span></p>

                                <p class="single-info">Penulis : <span class="value">{{ isset($model->author) ? $model->author->name : '' }}</span> </p>
                                <p class="single-info">ISBN : <span class="value">{{ $model->isbn }}</span> </p>
                                <p class="single-info">Cover : <span class="value">{{ $model->cover }}</span> </p>
                                <p class="single-info">Halaman : <span class="value">{{ $model->pages }} Halaman</span> </p>
                                <p class="single-info">Berat : <span class="value">{{ $model->weight }} gr</span> </p>
                                <p class="single-info">Ukuran : <span class="value">{{ $model->size }}</span> </p>
                                <p class="product-description">{!! $model->summary !!}</p>

                                <div class="product-actions">
                                    @if ($model->stock == 0)
                                    <button type="button" class="btn btn-danger">HABIS</button>
                                    @else
                                    <div class="quantity-selection">
                                        <input type="number" class="qty" value="1" min="1">
                                    </div>
                                    <div class="product-buttons">
                                        <a 
                                            id="send_cart"
                                            class="add_to_cart cart-btn" 
                                            href="javascript:;"
                                            data-id="{{ $model->id }}"
                                            data-title="{{ $model->title }}"
                                            data-image_url="{{ $model->image_url }}"
                                            data-price="{{ $discount_price }}"
                                            data-qty="1"
                                            data-link="{{ $model->link }}"
                                        > Tambahkan Ke Keranjang</a>
                                    </div>
                                    @endif
                                </div>

                                <div class="social-share-buttons mt-20">
                                    <h5>Bagikan</h5>
                                    <ul>
                                        <li><a class="facebook" href="https://www.facebook.com/Officialpustakaalkautsar"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="twitter" href="https://twitter.com/buku_alkautsar"><i class="fa fa-twitter"></i></a></li>
                                        <li><a class="instagram" href="https://www.instagram.com/pustaka.alkautsar/"><i class="fa fa-instagram"></i></a></li>
                                    </ul>
                                </div>

                            </div>
                            <!--=======  End of single product content description  =======-->
                        </div>
                    </div>
                </div>
                <!--=======  End of single product main content area  =======-->

                <!--=======  product description review   =======-->

                <div class="product-description-review-area">
                    <div class="row">
                        <div class="col-lg-12">
                            <!--=======  product description review container  =======-->

                            <div class="tab-slider-wrapper product-description-review-container  section-space--inner">
                                <nav>
                                    <div class="nav nav-tabs justify-content-center" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="description-tab" data-toggle="tab" href="#product-description" role="tab" aria-selected="true">Deskripsi</a>
                                        <a class="nav-item nav-link" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-selected="false">Biografi Penulis</a>
                                    </div>
                                </nav>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="product-description" role="tabpanel" aria-labelledby="description-tab">
                                        <!--=======  product description  =======-->

                                        <div class="product-description" style="text-align:justify;">{!! $model->description !!}</div>

                                        <!--=======  End of product description  =======-->
                                    </div>
                                    <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">

                                        <div class="product-description" style="text-align:justify;">{!! isset($model->author) ? $model->author->biography : '' !!}</div>

                                    </div>
                                </div>
                            </div>

                            <!--=======  End of product description review container  =======-->
                        </div>
                    </div>
                </div>

                <!--=======  End of product description review   =======-->

            </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection

@section('scripts')
<script>
    $(document).on('change blur', '.qty', function (e) {
        e.preventDefault()
        
        $(document).find('#send_cart').attr('data-qty', $(this).val())
    })
</script>
@endsection
