@extends('layouts.app')

@section('content')
<!--====================  hero slider area ====================-->

<div class="section-space" style="margin-top:-40px;">
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="shop-header2">
                    <div class="shop-header__left">
                        <div class="shop-header__left__message">
                            <span style="font-size:20px; font-weight:600; color:#033147;">KIRIM NASKAH BUKU TERJEMAHAN DAN PRIBADI</span>
                        </div>
                    </div>
                </div>
                <div class="about-top-content-wrapper">
                    <div class="row row-30">
                        <!-- About Image -->
                        <div class="col-lg-6">
                            <p class="mb-3" style="font-size:13px; text-align:justify;">Pustaka Al-Kautsar menerima kiriman naskah dari para penulis Indonesia berbakat dengan pembagian lini:</p>
                            <p class="mb-3" style="font-size:13px; text-align:justify; line-height:25px;">
                            • Naskah buku referensi Islam dewasa diterbitkan melalui lini <strong>Pustaka Al-Kautsar</strong>.<br>
                            • Naskah Komik, novel, non fiksi remaja dan dewasa diterbitkan melalui lini <strong>Salsabila</strong>.<br>
                            • Naskah buku anak diterbitkan melalui lini <strong>Al-Kautsar Kids</strong>.<br>
                            • Buku digital (eBook) melalui <strong>eBook Al-Kautsar</strong>.<br>
                            Untuk mengetahui tema dan judul yang sudah diterbitkan, Silakan Anda kunjungi website kami di www.kautsar.co.id
                            </p>

                            <h4 style="color:#033147;">BAGAIMANA CARA MENGIRIMKAN NASKAH KE PENERBIT PUSTAKA AL-KAUTSAR?</h4>

                            <p class="mb-3">
                            <img src="assets/img/naskah/naskah1.jpg" class="img-fluid">
                        </p>

                            <p class="mb-3" style="font-size:13px; text-align:justify; line-height:25px;">
                            Pustaka Al-Kautsar menerima naskah dalam bentuk softcopy (file) maupun Hardcopy (print out). Naskah yang dikirim dalam bentuk hardcopy harus dikirim dalam amplop coklat ke alamat
Pustaka Al-Kautsar di: Jl.Cipinang Muara Raya No.63, Jatinegara, Jakarta Timur 13420, Indonesia. Telepon: 021-8507590 / 8506702.
                            </p>
                            <p class="mb-3" style="font-size:13px; text-align:justify; line-height:25px;">
                            Sedangkan naskah yang dikirim dalam bentuk softcopy dikirim ke email redaksi@kautsar.co.id
dengan Subject : <br> Naskah_*Judul Buku*_*Nama.Penulis*_(*Nama Penerjemah*).
                            </p>
                            <p class="mb-3" style="font-size:13px; text-align:justify; line-height:25px;">
                            Untuk pengiriman naskah dalam bentuk softcopy maupun hardcopy, naskah harus di kirimkan seluruhannya dan lampirkan Surat pengantar (cover leter) dan Riwayat hidup singkat (cv).
                            </p>
                            <p class="mb-3" style="font-size:13px; text-align:justify; line-height:25px;">
                            Seluruh naskah yang di serahkan akan dinilai terlebih dahulu dalam rapat redaksi. Lama penilaian memakan waktu maksimal 3 bulan. Hasilnya akan diinformasikan oleh tim redaksi
kami via email ataupun whatsapp.
                            </p>
                            <p class="mb-3" style="font-size:13px; text-align:justify; line-height:25px;">
                            Konfirmasi atau pertanyaan terkait naskah, dapat menghubungi Redaksi melalui telepon 021-8507590, 8506702, atau melalui pesan singkat ke nomor Whatsapp +62895620042100. Jam
kerja pukul 08.00 – 16.00 (istirahat pukul 12.00 – 13.00).
                            </p>
                            <p class="mb-3" style="font-size:13px; text-align:justify; line-height:25px;">
                            NOTE: Bahan naskah yang dikirimkan ke Redaksi tidak dapat dikembalikan.
                            </p>
                            <br>
                            <h4 style="color:#033147;">KIRIM NASKAH ANDA KE ALAMAT</h4>
                                    <p class="mb-3" style="font-size:13px; text-align:justify; line-height:25px;">
                                        <strong>Penerbit Pustaka Al-Kautsar</strong><br>
                                        Jalan Cipinang Muara Raya No.63, Jakarta Timur 13420, Indonesia.<br>
                                        Telepon: 021-8507590 / 8506702 — Fax: 021-85912403<br>
                                        Email: <a href="mailto:redaksi@kautsar.co.id">redaksi@kautsar.co.id</a>
                                    </p>
                        </div>

                        <!-- About Content -->
                        <div class="about-content col-lg-6">
                            <div class="row">
                                <div class="col-12">
                                    <h4 style="color:#033147;">STANDAR NASKAH TERJEMAHAN</h4>
                                    <p class="mb-3" style="font-size:13px; text-align:justify; line-height:25px;">
                                        1.  Isi naskah harus sejalan dengan visi dan misi Pustaka Al-Kautsar.<br>
                                        2.  Diutamakan naskah yang belum pernah diterbitkan.<br>
                                        3.  Bagus dan menarik, ada hal-hal baru yang bisa dijual kepada masyarakat.<br>
                                        4.  Naskah harus sesuatu yang dibutuhkan umat.<br>
                                        5.  Tidak menjelek-jelekkan dan memojokkan ulama Ahlu sunnah.<br>
                                        6.  Diutamakan naskah yang baru/tidak terlalu lama (kecuali turats).<br>
                                        7.  Jika kitab turats, diutamakan yang sudah ditahqiq.<br>
                                        8.  Sistematika penulisan yang runtut dengan pembagian bab dan judul yang teratur. (Bermetode penulisan yang sistematis)<br>
                                        9.  Isi mudah dipahami, diterima, dan tidak membingungkan.<br>
                                        10. Untuk buku anak, diutamakan untuk menyertakan beberapa contoh halaman berilustrasi.<br>
                                        11. Menyertakan daftar isi dan sinopsis.<br>
                                        12. Naskah harus lengkap, tidak boleh berupa sample isi.<br>
                                        13. dan melampirkan surat pengantar <i>(cover letter)</i> dan riwayat hidup singkat <i>(curriculum vitae)</i>.
                                    </p>
                                    
                                    <h4 style="color:#033147;">STANDAR NASKAH PRIBADI</h4>
                                    <p class="mb-3" style="font-size:13px; text-align:justify; line-height:25px;">
                                        1.  Naskah harus karya Original.<br>
                                        2.  Isi naskah harus sejalan dengan visi dan misi Pustaka Al-Kautsar.<br>
                                        3.  Naskah belum pernah diterbitkan di penerbit lain.<br>
                                        4.  Naskah Ilmiah dan bisa dipertanggungjawabkan.<br>
                                        5.  Naskah ditulis dengan rapi, logis dan sistematis.<br>
                                        6.  Menarik dan ada hal-hal baru yang bisa dijual kepada masyarakat.<br>
                                        7.  Menggunakan referensi yang jelas.<br>
                                        8.  Menyertakan daftar isi dan sinopsis.<br>
                                        9.  Tulisan untuh dan padu (bukan kumpulan tulisan).<br>
                                        10. Sistematika penulisan yang runtut dengan pembagian bab dan judul yang teratur. (Bermetode penulisan yang sistematis)<br>
                                        11. Menyebutkan sumber dan <i>footnote jika ada kutipan</i>.<br>
                                        12. Memakai bahasa yang komunikatif dan mudah dipahami.<br>
                                        13. Naskah harus lengkap, tidak boleh berupa sample isi.<br>
                                        14. dan melampirkan surat pengantar <i>(cover letter)</i> dan riwayat hidup singkat <i>(curriculum vitae)</i>.
                                    </p>

                                    <h4 style="color:#033147;">FORMAT DAN TEKNIS NASKAH</h4>
                                    <p class="mb-3">
                                        <img src="assets/img/naskah/naskah2.jpg" class="img-fluid">
                                    </p>
                                    <p class="mb-3" style="font-size:13px; text-align:justify; line-height:25px;">
                                        • Naskah di tulis di kertas ukuran A4<br>
                                        • Font menggunakan “Times New Roman”<br>
                                        • Ukuran font 12pt<br>
                                        • Naskah ditulis dengan spasi 1,5<br>
                                        • Naskah dikirim dalam bentuk Print out atau dalam format PDF atau .doc/.docx untuk softcopy<br>
                                        • Sertakan gambar bila diperlukan.
                                    </p>

                                    <!--<form action="{{ route('save-document') }}" method="post">
                                        @csrf
                                        <div class="input-group mb-3">
                                            <div class="custom-file">
                                                <input type="email" class="form-control" placeholder="Isi email anda" name="email" required>
                                            </div>
                                        </div>
                                        <div class="input-group mb-3">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="inputGroupFile02">
                                                <label class="custom-file-label" for="inputGroupFile02">Pilih file naskah akan dikirim</label>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-success btn-sm">Upload Naskah</button>
                                    </form>-->
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection