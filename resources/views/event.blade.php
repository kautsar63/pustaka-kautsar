@extends('layouts.app')

@section('content')
<!--====================  hero slider area ====================-->
<div class="section-space" style="margin-top:-40px;">
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="single-row-slider-area section-space">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="shop-header2">
                    <div class="shop-header__left">
                        <div class="shop-header__left__message">
                            <span style="font-size:20px; font-weight:600; color:#033147;">INFO PAMERAN</span>
                        </div>
                    </div>
                </div>
                <!--=======  blog page content  =======-->
                <div class="blog-page-content">
                    <div class="row">
                        @foreach ($model as $row)
                        <div class="col-md-4">
                            <!--=======  single blog post  =======-->
                            <div class="single-blog-post">
                                <div class="single-blog-post__image">
                                    <a href="javascript:;">
                                        <img src="{{ asset('uploads/' . $row->image_url) }}" class="img-fluid" alt="{{ $row->title }}">
                                    </a>
                                </div>
                                <div class="single-blog-post__content">
                                    <h3 class="title"><a href="javascript:;">{{ $row->title }}</a></h3>
                                    <p class="short-desc">Tanggal: {{ date('d M Y', strtotime($row->start_date)) }} sampai dengan {{ date('d M Y', strtotime($row->end_date)) }}</p>
                                    <p class="short-desc">Tempat: {{ $row->place }}</p>
                                    <p class="short-desc"><strong>Booth: {{ $row->booth }}</strong></p>
                                </div>
                            </div>
                            <!--=======  End of single blog post  =======-->
                        </div>
                        @endforeach
                    </div>
                </div>

                <!--=======  pagination area =======-->
                {{-- <div class="pagination-area">
                    <div class="pagination-area__left">
                        Menampilkan 1-6 dari 50 Info Pameran
                    </div>
                    <div class="pagination-area__right">
                        <ul class="pagination-section">
                            <li><a href="#"><</a></li>
                            <li><a class="active" href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">></a></li>
                        </ul>
                    </div>
                </div> --}}
                <!--=======  End of pagination area  =======-->
                <!--=======  End of blog page content  =======-->
            </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection
