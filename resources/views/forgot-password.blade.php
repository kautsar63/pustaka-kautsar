@extends('layouts.app')

@section('content')
<!--====================  hero slider area ====================-->

<div class="section-space" style="margin-top:-55px;">
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-8">
                <!-- Login Form s-->
                <form action="{{ route('send-forgot-password') }}" method="POST">
                    @csrf
                    <div class="login-form">
                        <h4>Masukan Alamat Email Anda Untuk Melakukan Reset Password</h4>

                        <div class="row">
                            <div class="col-md-12 mb-20">
                                <input type="email" name="email" placeholder="Masukkan Email">
                            </div>
                            <div class="col-12">
                                <button type="submit" class="register-button2 mt-0">Reset Password</button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-4 col-xs-12">
            </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection