@extends('layouts.app')

@section('content')
<div class="hero-slider-area section-space--30">
    <!--=======  hero slider wrapper  =======-->

    <div class="hero-slider-wrapper">
        <div class="ht-slick-slider" data-slick-setting='{
        "slidesToShow": 1,
        "slidesToScroll": 1,
        "arrows": true,
        "dots": true,
        "autoplay": true,
        "autoplaySpeed": 5000,
        "speed": 1000,
        "fade": true,
        "infinite": true,
        "prevArrow": {"buttonClass": "slick-prev", "iconClass": "ion-chevron-left" },
        "nextArrow": {"buttonClass": "slick-next", "iconClass": "ion-chevron-right" }
    }' data-slick-responsive='[
        {"breakpoint":1501, "settings": {"slidesToShow": 1} },
        {"breakpoint":1199, "settings": {"slidesToShow": 1, "arrows": false} },
        {"breakpoint":991, "settings": {"slidesToShow": 1, "arrows": false} },
        {"breakpoint":767, "settings": {"slidesToShow": 1, "arrows": false} },
        {"breakpoint":575, "settings": {"slidesToShow": 1, "arrows": false} },
        {"breakpoint":479, "settings": {"slidesToShow": 1, "arrows": false} }
    ]'>

            <!--=======  single slider item  =======-->
            @foreach ($sliders as $slider)
            <div class="single-slider-item">
                <a href="{{ $slider->link }}" title="{{ $slider->title }}" style="width: 100%">
                    <div class="hero-slider-item-wrapper hero-slider-item-wrapper--fullwidth--fixed-height hero-slider-bg-16" style="background-image: url(./uploads/{{ $slider->image_url }}) !important">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="hero-slider-content">
                                        <p class="slider-title slider-title--big-light">{{ $slider->title }}</p>
                                        <p class="slider-title slider-title--big-light">{{ $slider->sub_title }}</p>
                                        <p class="slider-title slider-title--small">{{ $slider->description }}</p>
                                        <!-- <a class="hero-slider-button" href="{{ $slider->link }}"> <i class="ion-ios-plus-empty"></i> Beli Sekarang</a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
            <!--=======  End of single slider item  =======-->

        </div>
    </div>

    <!--=======  End of hero slider wrapper  =======-->
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--=======  category wrapper  =======-->
                <div class="category-wrapper category-wrapper--style2">
                    <div class="row">
                        @foreach ($ads as $ad)
                        <div class="col-md-4">
                            <!--=======  single category item  =======-->
                            <div class="single-category-item single-category-item--style1">
                                <div class="single-category-item__image">
                                    <a href="{{ $ad->link }}">
                                        <img src="{{ asset('uploads/' . $ad->image_url) }}" class="img-fluid" alt="" width="370" height="240" style="width: 370px; height: 240px">
                                    </a>
                                </div>
                            </div>
                            <!--=======  End of single category item  =======-->
                        </div>
                        @endforeach
                    </div>
                </div>
                <!--=======  End of category wrapper  =======-->
            </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@if (time() <= strtotime($flashs->end_date) && $promo->count() > 0)
<!--====================  single row slider ====================-->
<div class="single-row-slider-area section-space footer-area--flash-bg">
    <div class="container ">
        <div class="row">
            <div class="col-lg-12">
              <!--=======  shop page header  =======-->
              <div class="shop-header">
                <div class="shop-header__left">
                    <div class="d-flex">
                        <span style="font-family: 'Racing Sans One', cursive; font-size:36px; font-weight:600; color:#ce2f29; padding-top:2px; margin-right:20px;">FLASH SALE</span>
                        <div class="product-countdown" data-countdown="{{ str_replace('-', '/', isset($flashs->end_date) ? $flashs->end_date : 0) }}"></div>
                    </div>
                </div>
                  <div class="shop-header__right">
                      <div class="single-select-block d-inline-block">
                          <span style="font-size:14px; font-weight:400; color:#0f8949;"><a href="{{ isset($flashs->link) ? url('promo/' . $flashs->link) : '#' }}">Lihat Semua >></a></span>
                      </div>
                  </div>
              </div>
              <!--=======  End of shop page header  =======-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!--=======  single row slider wrapper  =======-->
                <div class="single-row-slider-wrapper" >
                    <div class="ht-slick-slider" data-slick-setting='{
                    "slidesToShow": 5,
                    "slidesToScroll": 1,
                    "arrows": true,
                    "autoplay": true,
                    "autoplaySpeed": 5000,
                    "speed": 1000,
                    "infinite": true,
                    "prevArrow": {"buttonClass": "slick-prev", "iconClass": "ion-chevron-left" },
                    "nextArrow": {"buttonClass": "slick-next", "iconClass": "ion-chevron-right" }
                }' data-slick-responsive='[
                    {"breakpoint":1501, "settings": {"slidesToShow": 4} },
                    {"breakpoint":1199, "settings": {"slidesToShow": 4, "arrows": false} },
                    {"breakpoint":991, "settings": {"slidesToShow": 3, "arrows": false} },
                    {"breakpoint":767, "settings": {"slidesToShow": 2, "arrows": false} },
                    {"breakpoint":575, "settings": {"slidesToShow": 2, "arrows": false} },
                    {"breakpoint":479, "settings": {"slidesToShow": 2, "arrows": false} }
                ]'>

                    @if (isset($flashs->products))
                        @foreach ($flashs->products as $flash)
                            @widget('bookWidget', ['parent' => $flashs, 'data' => $flash->product])
                        @endforeach
                    @endif

                    </div>
                </div>
                <!--=======  End of single row slider wrapper  =======-->
            </div>
        </div>
    </div>
</div>
<!--====================  End of single row slider  ====================-->
@endif

<!--====================  single row slider ====================-->
<div class="single-row-slider-area section-space">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
              <!--=======  shop page header  =======-->
              <div class="shop-header2">
                  <div class="shop-header__left">
                      <div class="shop-header__left__message">
                          <span style="font-size:24px; font-weight:600; color:#033147;">BUKU BARU</span>
                      </div>
                  </div>
                  <div class="shop-header__right">
                      <div class="single-select-block d-inline-block">
                          <span style="font-size:14px; font-weight:400; color:#033147;"><a href="{{ url('promo/buku-baru') }}">Lihat Semua >></a></span>
                      </div>
                  </div>
              </div>
              <!--=======  End of shop page header  =======-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!--=======  single row slider wrapper  =======-->
                <div class="single-row-slider-wrapper">
                    <div class="ht-slick-slider" data-slick-setting='{
                    "slidesToShow": 5,
                    "slidesToScroll": 1,
                    "arrows": true,
                    "autoplay": true,
                    "autoplaySpeed": 5000,
                    "speed": 1000,
                    "infinite": true,
                    "prevArrow": {"buttonClass": "slick-prev", "iconClass": "ion-chevron-left" },
                    "nextArrow": {"buttonClass": "slick-next", "iconClass": "ion-chevron-right" }
                }' data-slick-responsive='[
                    {"breakpoint":1501, "settings": {"slidesToShow": 4} },
                    {"breakpoint":1199, "settings": {"slidesToShow": 4, "arrows": false} },
                    {"breakpoint":991, "settings": {"slidesToShow": 3, "arrows": false} },
                    {"breakpoint":767, "settings": {"slidesToShow": 2, "arrows": false} },
                    {"breakpoint":575, "settings": {"slidesToShow": 2, "arrows": false} },
                    {"breakpoint":479, "settings": {"slidesToShow": 2, "arrows": false} }
                ]'>

                    @if (isset($newBooks->products))
                        @foreach ($newBooks->products as $book)
                            @widget('bookWidget', ['parent' => null, 'data' => $book->product])
                        @endforeach
                    @endif

                    </div>
                </div>
                <!--=======  End of single row slider wrapper  =======-->
            </div>
        </div>
    </div>
</div>
<!--====================  End of single row slider  ====================-->

<!--====================  single row slider ====================-->
<div class="single-row-slider-area section-space">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
              <!--=======  shop page header  =======-->
              <div class="shop-header2">
                  <div class="shop-header__left">
                      <div class="shop-header__left__message">
                          <span style="font-size:24px; font-weight:600; color:#033147;">BUKU BEST SELLERS</span>
                      </div>
                  </div>
                  <div class="shop-header__right">
                      <div class="single-select-block d-inline-block">
                          <span style="font-size:14px; font-weight:400; color:#033147;"><a href="{{ url('promo/best-sellers') }}">Lihat Semua >></a></span>
                      </div>
                  </div>
              </div>
              <!--=======  End of shop page header  =======-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!--=======  single row slider wrapper  =======-->
                <div class="single-row-slider-wrapper">
                    <div class="ht-slick-slider" data-slick-setting='{
                    "slidesToShow": 5,
                    "slidesToScroll": 1,
                    "arrows": true,
                    "autoplay": true,
                    "autoplaySpeed": 5000,
                    "speed": 1000,
                    "infinite": true,
                    "prevArrow": {"buttonClass": "slick-prev", "iconClass": "ion-chevron-left" },
                    "nextArrow": {"buttonClass": "slick-next", "iconClass": "ion-chevron-right" }
                }' data-slick-responsive='[
                    {"breakpoint":1501, "settings": {"slidesToShow": 4} },
                    {"breakpoint":1199, "settings": {"slidesToShow": 4, "arrows": false} },
                    {"breakpoint":991, "settings": {"slidesToShow": 3, "arrows": false} },
                    {"breakpoint":767, "settings": {"slidesToShow": 2, "arrows": false} },
                    {"breakpoint":575, "settings": {"slidesToShow": 2, "arrows": false} },
                    {"breakpoint":479, "settings": {"slidesToShow": 2, "arrows": false} }
                ]'>

                    @if (isset($bestSellers->products))
                        @foreach ($bestSellers->products as $bestSeller)
                            @widget('bookWidget', ['parent' => $bestSellers, 'data' => $bestSeller->product])
                        @endforeach
                    @endif

                    </div>
                </div>
                <!--=======  End of single row slider wrapper  =======-->
            </div>
        </div>
    </div>
</div>
<!--====================  End of single row slider  ====================-->

<!--====================  single row slider ====================-->
<div class="single-row-slider-area section-space">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
              <!--=======  shop page header  =======-->
              <div class="shop-header2">
                  <div class="shop-header__left">
                      <div class="shop-header__left__message">
                          <span style="font-size:24px; font-weight:600; color:#033147;">BUKU ANAK</span>
                      </div>
                  </div>
                  <div class="shop-header__right">
                      <div class="single-select-block d-inline-block">
                          <span style="font-size:14px; font-weight:400; color:#033147;"><a href="{{ url('category/buku-anak') }}">Lihat Semua >></a></span>
                      </div>
                  </div>
              </div>
              <!--=======  End of shop page header  =======-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!--=======  single row slider wrapper  =======-->
                <div class="single-row-slider-wrapper">
                    <div class="ht-slick-slider" data-slick-setting='{
                    "slidesToShow": 5,
                    "slidesToScroll": 1,
                    "arrows": true,
                    "autoplay": true,
                    "autoplaySpeed": 5000,
                    "speed": 1000,
                    "infinite": true,
                    "prevArrow": {"buttonClass": "slick-prev", "iconClass": "ion-chevron-left" },
                    "nextArrow": {"buttonClass": "slick-next", "iconClass": "ion-chevron-right" }
                }' data-slick-responsive='[
                    {"breakpoint":1501, "settings": {"slidesToShow": 4} },
                    {"breakpoint":1199, "settings": {"slidesToShow": 4, "arrows": false} },
                    {"breakpoint":991, "settings": {"slidesToShow": 3, "arrows": false} },
                    {"breakpoint":767, "settings": {"slidesToShow": 2, "arrows": false} },
                    {"breakpoint":575, "settings": {"slidesToShow": 2, "arrows": false} },
                    {"breakpoint":479, "settings": {"slidesToShow": 2, "arrows": false} }
                ]'>

                    @if (isset($childrens->products))
                        @foreach ($childrens->products as $children)
                            @widget('bookWidget', ['parent' => $childrens, 'data' => $children->product])
                        @endforeach
                    @endif

                    </div>
                </div>
                <!--=======  End of single row slider wrapper  =======-->
            </div>
        </div>
    </div>
</div>
<!--====================  End of single row slider  ====================-->

<!--====================  blog post slider area ====================-->
<div class="blog-post-slider-area">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <!--=======  blog post slider border wrapper  =======-->
                <div class="blog-post-slider-border-wrapper section-space--inner">
                    <div class="row">
                        <div class="col-lg-12">
                            <!--=======  section title  =======-->
                            <div class="section-title-wrapper text-left section-space--half">
                                <span style="font-family: 'Great Vibes', cursive; font-size:44px; color:#033147; line-height:36px; text-align:left; font-weight:400;">Spesial Dari Redaksi</span>
                                <span style="font-size:15px; font-weight:400; color:#0f8949; float:right; padding-top:10px;"><a href="{{ route('news-all') }}">Lihat Semua >></a></span>
                            </div>
                            <!--=======  End of section title  =======-->
                        </div>
                        <div class="col-lg-12">
                            <!--=======  blog post slider wrapper  =======-->
                            <div class="blog-post-slider-wrapper">
                                <div class="ht-slick-slider" data-slick-setting='{
                                "slidesToShow": 3,
                                "slidesToScroll": 1,
                                "arrows": false,
                                "autoplay": false,
                                "autoplaySpeed": 5000,
                                "speed": 1000,
                                "infinite": true
                            }' data-slick-responsive='[
                                {"breakpoint":1501, "settings": {"slidesToShow": 3} },
                                {"breakpoint":1199, "settings": {"slidesToShow": 3} },
                                {"breakpoint":991, "settings": {"slidesToShow": 2} },
                                {"breakpoint":767, "settings": {"slidesToShow": 1} },
                                {"breakpoint":575, "settings": {"slidesToShow": 1} },
                                {"breakpoint":479, "settings": {"slidesToShow": 1} }
                            ]'>

                                @if (isset($news))
                                    @foreach ($news as $new)
                                        @widget('newsWidget', ['data' => $new])
                                    @endforeach
                                @endif

                                </div>
                            </div>
                            <!--=======  End of blog post slider wrapper  =======-->
                        </div>
                    </div>
                </div>
                <!--=======  End of blog post slider border wrapper  =======-->
            </div>
        </div>

    </div>
</div>
<br>
<!--====================  End of blog post slider area  ====================-->
@endsection