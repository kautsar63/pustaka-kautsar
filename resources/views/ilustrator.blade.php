@extends('layouts.app')

@section('content')
<!--====================  hero slider area ====================-->

<div class="section-space" style="margin-top:-55px;">
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
          <div class="col-lg-12"><br>
            <div class="shop-header2">
              <div class="shop-header__left">
                  <div class="shop-header__left__message">
                      <span style="font-size:20px; font-weight:600; color:#033147;">Kirim Portfolio Ilustrator</span>
                  </div>
              </div>
            </div>
              <div class="about-top-content-wrapper">
                  <div class="row row-30">

                      <!-- About Content -->
                      <div class="about-content col-lg-12">
                          <div class="row">
                              <div class="col-12">
                                  <p class="mb-3" style="font-size:13px; text-align:justify;">Pustaka Al-Kautsar menerima pengajuan freelance Illustrator untuk buku anak maupun komik
                                    bagi kalian Illustrator berbakat yang berminat untuk membuat buku. Kamu bisa juga mengajukan naskah dan illustrasi sekaligus yang sudah kamu miliki.</p>

                                  <p class="mb-3" style="font-size:13px; text-align:justify;">Cukup dengan mengirimkan Surat pengantar (cover leter), Riwayat hidup singkat (cv) dan
                                    Portofolio kamu ke alamat email kami di <a href="mailto:redaksi@kautsar.co.id" target="_blank">redaksi@kautsar.co.id</a> dengan <strong>Subject : Illustrator_ *Nama kamu*.</strong></p>
                                  <p class="mb-3" style="font-size:13px; text-align:justify;">Setelah mengirim seluruh ketentuannya, nantinya akan kami nilai terlebih dahulu naskah dan
                                    segmen mana yang sesuai dengan gaya illustrasi kamu, penilaian ini membutuhkan waktu
                                    maksimal 2 bulan. Untuk hasilnya baik diterima atau ditolak, tim redaksi kami akan mengabari
                                    via email ataupun whatsapp. Untuk konfirmasi atau pertanyaan terkait naskah bisa dilakukan
                                    pada jam kerja Senin-Jumat via chat ke nomor whatsapp +62895620042100 atau menelepon ke
                                    nomor 021-8507590, 8506702 ke redaksi pada pukul 08.00 – 16.00 (istirahat pukul 12.00 –13.00).
                                  </p>

                              </div>

                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection