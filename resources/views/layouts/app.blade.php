<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pustaka Al-Kautsar - Penerbit Buku Islam Utama</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="icon" href="{{ asset('assets/img/favicon.ico') }}">
    <link href="https://fonts.googleapis.com/css?family=Great+Vibes|Racing+Sans+One&display=swap" rel="stylesheet">

    <!--=============================================
    =            CSS  files       =
    =============================================-->

    <!-- Vendor CSS -->
    <link href="{{ asset('assets/css/vendors.css') }}" rel="stylesheet">
    <!-- Main CSS -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <style>
        .grid {
            display: grid;
        }
        .five-column {
            grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
        }
        
        /* minicart */
        .mini-cart {
        	position: relative;
        	z-index: 9999;
        	display: none;
        	width: 350px;
        	padding: 30px;
        	background-color: #fff;
        }
        
        .mini-cart.active {
        	display: block;
        }
        
        .mini-cart .cart-items-wrapper {
        	position: relative;
        	overflow: hidden;
        	max-height: 280px;
        	margin: 0 -30px;
        	padding: 0 30px;
        }
        
        .mini-cart .single-cart-item {
        	position: relative;
        	display: -webkit-box;
        	display: -webkit-flex;
        	display: -ms-flexbox;
        	display: flex;
        	padding-top: 20px;
        	padding-bottom: 20px;
        	border-bottom: 1px solid #f0f0f0;
        	-webkit-box-align: center;
        	-webkit-align-items: center;
        	-ms-flex-align: center;
        	align-items: center;
        }
        
        .mini-cart .single-cart-item .remove-icon {
        	position: absolute;
        	top: 10px;
        	right: 0;
        }
        
        .mini-cart .single-cart-item .remove-icon i {
        	font-size: 16px;
        	color: #bababa;
        }
        
        .mini-cart .single-cart-item .remove-icon:hover i {
        	color: #e33;
        }
        
        .mini-cart .single-cart-item:first-child {
        	padding-top: 0;
        }
        
        .mini-cart .single-cart-item:last-child {
        	border-bottom: 0;
        }
        
        .mini-cart .single-cart-item .image {
        	-webkit-flex-basis: 80px;
        	-ms-flex-preferred-size: 80px;
        	flex-basis: 80px;
        }
        
        .mini-cart .single-cart-item .content {
        	padding-left: 10px;
        	-webkit-flex-basis: calc(100% - 80px);
        	-ms-flex-preferred-size: calc(100% - 80px);
        	flex-basis: calc(100% - 80px);
        }
        
        .mini-cart .single-cart-item .content .product-title {
        	margin-bottom: 8px;
        }
        
        .mini-cart .single-cart-item .content .product-title a {
        	font-size: 12px;
        	font-weight: 400;
        	line-height: 18px;
        	color: #343434;
        }
        
        .mini-cart .single-cart-item .content .product-title a:hover {
        	color: #e33;
        }
        
        .mini-cart .single-cart-item .content .count {
        	font-size: 13px;
        	font-weight: 500;
        	line-height: 18px;
        	color: #242424;
        }
        
        .mini-cart .single-cart-item .content .count span {
        	font-weight: 400;
        	line-height: 18px;
        	color: #747474;
        }
        
        .mini-cart .cart-calculation {
        	margin-bottom: 20px;
        	padding-top: 20px;
        	padding-bottom: 20px;
        	border-top: 1px solid #f0f0f0;
        	border-bottom: 1px solid #f0f0f0;
        }
        
        .mini-cart .cart-calculation table {
        	margin-bottom: 0;
        	border-spacing: 0;
        	border-collapse: collapse;
        }
        
        .mini-cart .cart-calculation table tbody tr td {
        	font-size: 14px;
        	padding: 5px 0;
        	vertical-align: middle;
        	text-transform: capitalize;
        	color: #242424;
        	border: none;
        }
        
        .mini-cart .cart-calculation table tbody tr td:last-child {
        	font-family: 'Poppins', sans-serif;
        	font-weight: 700;
        }
        
        .mini-cart .cart-buttons a {
        	font-size: 13px;
        	font-weight: 500;
        	line-height: 40px;
        	display: block;
        	margin: 0;
        	margin-bottom: 20px;
        	padding: 0 5px;
        	-webkit-transition: .3s;
        	transition: .3s;
        	text-align: center;
        	/*text-transform: uppercase;*/
        	color: #242424;
        	border: 2px solid #033147;
        	border-radius: 2px;
        	background: #fff;
        }
        
        .mini-cart .cart-buttons a:last-child {
        	margin-bottom: 0;
        }
        
        .mini-cart .cart-buttons a:hover {
        	color: #fff;
        	border-color: #033147;
        	background: #033147;
        }
    </style>

</head>

<body>
    <!--====================  header area ====================-->
    <div class="header-area header-area--multi-row header-area--multi-row--separate-navigation header-sticky">
        <!--=======  header top wrapper  =======-->
        <div class="header-top-wrapper">

            <div class="container">

                @include('_partials/top-navbar')

            </div>
        </div>
        <!--=======  End of header top wrapper  =======-->

        <div class="navigation-menu-area d-none d-lg-block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        
                        @include('_partials/navbar')

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of header area  ====================-->
    <!--====================  hero slider area ====================-->
    
    @if(session('status'))
    <div class="alert alert-success alert-dismissible fade show" role="alert" id="success-alert">
        {{ session('status') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    @yield('content')

    <!--====================  quick view ====================-->
    <div class="modal fade quick-view-modal-container" id="quick-view-modal-container" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-xl-12 col-lg-12">
                        <!--=======  single product main content area  =======-->
                        <div class="single-product-main-content-area">
                            <div class="row">
                                <div class="col-xl-5 col-lg-6">
                                    <!--=======  product details slider area  =======-->

                                    <div class="product-details-slider-area">

                                        <div class="big-image-wrapper">

                                            <div class="product-details-big-image-slider-wrapper-quick product-details-big-image-slider-wrapper--bottom-space ht-slick-slider" data-slick-setting='{
                                            "slidesToShow": 1,
                                            "slidesToScroll": 1,
                                            "arrows": false,
                                            "autoplay": false,
                                            "autoplaySpeed": 5000,
                                            "fade": true,
                                            "speed": 500,
                                            "prevArrow": {"buttonClass": "slick-prev", "iconClass": "fa fa-angle-left" },
                                            "nextArrow": {"buttonClass": "slick-next", "iconClass": "fa fa-angle-right" }
                                            }' data-slick-responsive='[
                                            {"breakpoint":1501, "settings": {"slidesToShow": 1, "arrows": false} },
                                            {"breakpoint":1199, "settings": {"slidesToShow": 1, "arrows": false} },
                                            {"breakpoint":991, "settings": {"slidesToShow": 1, "arrows": false, "slidesToScroll": 1} },
                                            {"breakpoint":767, "settings": {"slidesToShow": 1, "arrows": false, "slidesToScroll": 1} },
                                            {"breakpoint":575, "settings": {"slidesToShow": 1, "arrows": false, "slidesToScroll": 1} },
                                            {"breakpoint":479, "settings": {"slidesToShow": 1, "arrows": false, "slidesToScroll": 1} }
                                            ]'>
                                                <div class="single-image">
                                                    <img id="single-image" src="" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--=======  End of product details slider area  =======-->
                                </div>
                                <div class="col-xl-7 col-lg-6">
                                    <!--=======  single product content description  =======-->
                                    <div class="single-product-content-description">
                                        <h4 class="product-title"></h4>

                                        <p class="single-grid-product__price"><span class="discounted-price">Rp. 0</span> <span class="main-price discounted">Rp. 0</span></p>

                                        <p class="single-info">Penulis: <span class="author value"></span> </p>
                                        <p class="single-info">ISBN: <span class="isbn value"></span> </p>
                                        <p class="single-info">Cover: <span class="hard-cover value"></span> </p>

                                        <p class="product-description" style="text-align:justify;"></p>

                                        <div class="product-actions product-actions--quick-view">
                                            <div class="quantity-selection">
                                                <label>Qty</label>
                                                <input type="number" class="view_qty" value="1" min="1">
                                            </div>

                                            <div class="product-buttons">
                                                <a href="javascript:;"
                                                    class="add_to_cart cart-btn"
                                                    data-id=""
                                                    data-title=""
                                                    data-image_url=""
                                                    data-mainprice=""
                                                    data-price=""
                                                    data-qty=""
                                                    data-link=""
                                                ><i class="ion-bag"></i> Masukkan Keranjang</a>
                                                <span class="wishlist-compare-btn">
                                                    <a> <i class="ion-heart"></i></a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--=======  End of single product content description  =======-->
                                </div>
                            </div>
                        </div>
                        <!--=======  End of single product main content area  =======-->
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--====================  End of quick view  ====================-->

    @include('_partials/footer')
    
    <!-- scroll to top  -->
    <!--<div id="scroll-top">
        <span>Kembali Ke Atas</span><i class="ion-chevron-right"></i><i class="ion-chevron-right"></i>
    </div>-->
    <!-- end of scroll to top -->
    <!--=============================================
    =            JS files        =
    =============================================-->

    <!-- Vendor JS -->
    <script src="{{ asset('assets/js/vendors.js') }}"></script>

    <!-- Active JS -->
    <script src="{{ asset('assets/js/active.js') }}"></script>
    <script>
        function formatRupiah (num) {
            if (num != undefined) {
                var p = num.toFixed(2).split(".");
                return "Rp. " + p[0].split("").reverse().reduce(function(acc, num, i, orig) {
                    return  num=="-" ? acc : num + (i && !(i % 3) ? "." : "") + acc;
                }, "");
            }
        }

        const base_url = '{{ url('/') }}'

        $(document).on('change', '.view_qty', function () {
            $modal = $(this).closest('#quick-view-modal-container')

            $modal.find('.add_to_cart').attr('data-qty', $(this).val())
        })
        
        function changeStock (stock) {
            $action = $('.product-actions--quick-view')
            if (stock == 0)
                $action.html('<button type="button" class="btn btn-danger">HABIS</button>')
            else {
                $action.html(`
                    <div class="quantity-selection">
                        <label>Qty</label>
                        <input type="number" class="view_qty" value="1" min="1">
                    </div>

                    <div class="product-buttons">
                        <a href="javascript:;"
                            class="add_to_cart cart-btn"
                            data-id=""
                            data-title=""
                            data-image_url=""
                            data-mainprice=""
                            data-price=""
                            data-qty=""
                            data-link=""
                        ><i class="ion-bag"></i> Masukkan Keranjang</a>
                        <span class="wishlist-compare-btn">
                            <a> <i class="ion-heart"></i></a>
                        </span>
                    </div>
                `)
            }
        }

        $(document).on('click', '.view-book', function (e) {
            const id = $(this).data('id')

            $.getJSON(base_url + '/book/' + id, function (items) {
                const $modal = $('#quick-view-modal-container')
                
                $modal.find('.product-title').html(items.title)
                $modal.find('.discounted').html(formatRupiah(items.price))
                $modal.find('.discounted-price').html(formatRupiah(items.discounted))
                $modal.find('.author').html(items.author)
                $modal.find('.isbn').html(items.isbn)
                $modal.find('.hard-cover').html(items.cover)
                $modal.find('.product-description').html(items.description)
                $modal.find('.view_qty').val(1)
                $modal.find('#single-image').attr('src', base_url + '/uploads/' + items.image_url)
                
                changeStock(parseInt(items.stock))

                // remove element button
                $modal.find('.add_to_cart').remove()

                let button = document.createElement('a')
                button.classList.add('add_to_cart')
                button.classList.add('cart-btn')
                button.setAttribute('href', 'javascript:;')
                button.setAttribute('data-id', id)
                button.setAttribute('data-title', items.title)
                button.setAttribute('data-image_url', items.image_url)
                button.setAttribute('data-mainprice', items.price)
                button.setAttribute('data-price', items.discounted)
                button.setAttribute('data-link', items.link)
                button.setAttribute('data-qty', 1)
                
                let icon = document.createElement('i')
                icon.className = 'ion-bag'
                button.appendChild(icon)
                
                let text = document.createTextNode(' Masukkan Keranjang')
                button.appendChild(text)

                $modal.find('.product-buttons').prepend(button)
            })

            e.preventDefault()
        })
        
        $(document.body).on('hidden.bs.modal', function () {
            $('#quick-view-modal-container').removeData('bs.modal')
        });

        $(document).on('click', '.add_to_cart', function (e) {
            e.preventDefault()
            
            const id = $(this).data('id')
            const title = $(this).data('title')
            const image_url = $(this).data('image_url')
            const main_price = $(this).data('mainprice')
            const price = $(this).data('price')
            const link = $(this).data('link')
            let qty = parseInt($(this).data('qty'))

            if (localStorage.getItem('book_' + id) != undefined) {
                qty++
                $(this).data('qty', qty)
            }

            localStorage.setItem(
                'book_' + id,
                JSON.stringify({
                    id: id,
                    title: title,
                    image_url: image_url,
                    price: price,
                    main_price: main_price,
                    link: link,
                    qty: qty
                })
            )

            loadCart()
        })

        $(document).on('click', '.remove-cart', function (e) {
            e.preventDefault()

            const id = $(this).data('id')
            const $tr = $(this).closest('tr')

            // remove book in cart
            localStorage.removeItem(id)
            $tr.remove()

            loadCart()
        })
        
        $(document).on('click', '#minicart-checkout', function (e) {
            e.preventDefault()

            const $form = $(document).find('#sendCart')

            $form.submit()
        })

        let loadCart = function () {
            $cart = $(document).find('.cart-items-wrapper')
            $cart.html('')

            $total = $(document).find('#total')
            $total.html('')

            const total_items = localStorage.length
            const keys = Object.keys(localStorage)

            let total = 0
            let total_badge = 0
            
            for (let i = 0; i < total_items; i++) {
                const book = JSON.parse(localStorage.getItem(keys[i]))
                const id = book['id']
                const title = book['title']
                const image_url = book['image_url']
                const main_price = book['main_price']
                const price = book['price']
                const link = book['link']
                let qty = book['qty']

                total_badge += qty
                total += (qty * price)
                
                let cartTemplate = `
                    <div class="single-cart-item" style="position: relative; display: flex; padding-bottom: 20px; border-bottom: 1px solid #f0f0f0; -webkit-box-align: center; align-items: center;">
                        <a href="javascript:void(0)" data-id="book_${id}" class="remove-cart remove-icon"><i class="ion-android-close"></i></a>
                        <div class="image" style="flex-basis: 80px;">
                            <a href="${base_url}/${link}">
                                <img src="{{ asset('uploads/${image_url}') }}" class="img-fluid" alt="${title}">
                            </a>
                        </div
                        <div class="content" style="padding-left: 10px; flex-basis: calc(100% - 80px);">
                            <p class="product-title" style="margin-bottom: 8px;">
                                <a href="${base_url + link}" style="font-size: 12px; font-weight: 400; line-height: 18px; color: #343434;">${title}</a>
                            </p>
                            <p class="count" style="font-size: 13px; font-weight: 500; line-height: 18px; color: #242424;">
                                <span style="font-weight: 400; line-height: 18px; color: #747474;">${qty} x </span> ${formatRupiah(price)}
                            </p>
                        </div>

                        <input type="hidden" name="id[]" value="${id}"/>
                        <input type="hidden" name="title[]" class="title" value="${title}"/>
                        <input type="hidden" name="link[]" class="link" value="${link}"/>
                        <input type="hidden" name="main_price[]" class="main_price" value="${main_price}"/>
                        <input type="hidden" name="real_price[]" class="real_price" value="${price}"/>
                        <input type="hidden" name="sub_price[]" class="sub_price" value="${total}"/>
                        <input type="hidden" name="qty[]" class="qty" value="${qty}"/>

                    </div>
                `

                $cart.append(cartTemplate)
            }

            $('#total_qty_floating').val(total_badge)
            $('#total_price_floating').val(total)

            $(document).find('span.counter').html(total_badge)
            $total.html(formatRupiah(total))
        }

        loadCart()

        // function all for cart
        let totalQty = 0
        let totalCart = 0
        let loadCartTable = function () {
            $cart = $(document).find('#carts')
            $cart.html('')

            $total = $(document).find('#total_carts')
            $total.html('')

            const total_items = localStorage.length            
            const keys = Object.keys(localStorage)

            let total = 0
            let total_badge = 0
            
            for (let i = 0; i < total_items; i++) {
                const book = JSON.parse(localStorage.getItem(keys[i]))
                const id = book['id']
                const title = book['title']
                const image_url = book['image_url']
                const main_price = book['main_price']
                const price = book['price']
                const link = book['link']
                let qty = book['qty']

                total_badge += qty
                total += qty * price
                
                let cartTemplate = `
                    <tr>
                        <td class="pro-thumbnail">
                            <a href="${base_url}/detail/${link}">
                                <img src="{{ asset('uploads/${image_url}') }}" class="img-fluid" alt="${title}">
                            </a>
                        </td>
                        <td class="pro-title">
                            <input type="hidden" name="id[]" value="${id}"/>
                            <input type="hidden" name="title[]" class="title" value="${title}"/>
                            <input type="hidden" name="link[]" class="link" value="${link}"/>
                            <input type="hidden" name="main_price[]" class="main_price" value="${main_price}"/>
                            <input type="hidden" name="real_price[]" class="real_price" value="${price}"/>
                            <input type="hidden" name="sub_price[]" class="sub_price" value="${total}"/>
                            <a href="${base_url}/${link}">${title}</a>
                            <p class="single-grid-product__price">
                                <span class="discounted-price">${formatRupiah(price)}</span><br>
                                <span class="main-price discounted">${formatRupiah(main_price)}</span>
                            </p>
                        </td>
                        <td class="pro-quantity">
                            <div class="quantity-selection">
                                <input type="number" name="qty[]" autocomplete="off" class="qty-cart" value="${qty}" min="1">
                            </div>
                        </td>
                        <td class="pro-remove">
                            <a href="javascript:void(0)" data-id="book_${id}" class="remove-cart">
                                <i class="fa fa-trash-o" style="padding-left:6px;"></i><br>
                                <small>Hapus</small>
                            </a>
                        </td>
                    </tr>
                `

                $cart.append(cartTemplate)
            }

            totalQty = total_badge
            totalCart = total

            reCount()
        }

        // change qty to price
        $(document).on('change', '.qty-cart', function (e) {
            e.preventDefault()

            const value = $(this).val()
            const $tr = $(this).closest('tr')
            const priceNow = $tr.find('.real_price').val()

            let subtotal = value * priceNow

            $tr.find('.discounted-price').html(formatRupiah(subtotal))
            $tr.find('.sub_price').val(subtotal)

            reCount()
        }).trigger('change')

        let reCount = function () {
            const allPrice = $('#carts').find('.real_price')
            const allQty = $('#carts').find('.qty-cart')

            let subQty = 0
            let subPrice = 0
            let subTotal = 0
            for (let i = 0; i < allQty.length; i++) {
                const qty = parseInt(allQty[i].value)
                const price = parseInt(allPrice[i].value)

                subQty += qty
                subPrice += price
                subTtl = qty * price
                subTotal += parseInt(subTtl)
            }

            $('#total_qty').val(subQty)
            $('#total_price').val(subTotal)

            // $('.counter').html(subQty)
            $('.total_cart').html(formatRupiah(subTotal))
        }

        loadCartTable()

        $('.add_to_wishlist').each(function (i, obj) {
            // $(this).html('<i class="ion-heart"></i>')
        })

        $(document).on('click', '.add_to_wishlist', function (e) {
            e.preventDefault()

            const id = $(this).data('id')
            const $this = $(this)

            $.ajax({
                method: 'post',
                url: base_url + '/wishlist',
                data: {
                    '_token': '{{ csrf_token() }}',
                    id: id
                },
                success: function (result) {
                    console.log(result)
                    if (result == 1)
                        alert('wishlist sudah tersimpan!')
                    else
                        alert('wishlist tidak tersimpan, silakan login dulu!')
                }
            })
        })

        $(document).on('click', '.remove-wishlist', function (e) {
            e.preventDefault()

            const id = $(this).data('id')
            const $this = $(this)
            
            $.post(base_url + '/delete-wishlist', { _token: '{{ csrf_token() }}', id: id }, function (result) {
                if (result == 1)
                    $this.parent().parent().remove()
                else
                    return false
            })
        })
    </script>
    
    @yield('scripts')

    <!--=====  End of JS files ======-->

</body>

</html>
