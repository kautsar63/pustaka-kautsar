<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			Pustaka Al-Kautsar Admin Page | Login
		</title>
		<meta name="description" content="Pustaka Al-Kautsar Admin Page | Dashboard">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
            WebFont.load({
                google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
                active: function() {
                    sessionStorage.fonts = true;
                }
            });
		</script>
		<!--end::Web font -->
        <!--begin::Base Styles -->
        <!--begin::Page Vendors -->
		<link href="{{ asset('admin-assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors -->
		<link href="{{ asset('admin-assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('admin-assets/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Base Styles -->
		<link rel="shortcut icon" href="{{ asset('admin-assets/media/img/logo/favicon.ico') }}" />
		<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
		<style>
			.payment_status {
				border-radius: 4px;
				padding: 3px 0 4px 0;
				border: 1px solid #ccc;
				color: white;
				text-align: center;
				font-weight: bold;
				vertical-align: middle;
				margin-bottom: 0;
			}
			.info { background: #5bc0de; }
			.default { background: #fff; color: #333 }
			.warning { background: #f0ad4e; }
			.success { background: #5cb85c; }
			.primary { background: #337ab7; }
		</style>
	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
            
            @include('admin/_partials/top-navbar')

            <!-- begin::Body -->
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

                <div class="m-grid__item m-grid__item--fluid m-wrapper">
                    <div class="m-content">

                        @yield('contents')

                    </div>
                </div>
            </div>
            <!-- end:: Body -->            

            @include('admin/_partials/footer')

		</div>
		<!-- end:: Page -->

	    <!-- begin::Scroll Top -->
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>
		<!-- end::Scroll Top -->
    	<!--begin::Base Scripts -->
		<script src="{{ asset('admin-assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ asset('admin-assets/base/scripts.bundle.js') }}" type="text/javascript"></script>
		<!--end::Base Scripts -->
        <!--begin::Page Vendors -->
		<script src="{{ asset('admin-assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
		<!--end::Page Vendors -->
        <!--begin::Page Snippets -->
		<script src="{{ asset('admin-assets/app/js/dashboard.js') }}" type="text/javascript"></script>
        <!--end::Page Snippets -->
		<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
		<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        @yield('scripts')
	</body>
	<!-- end::Body -->
</html>
