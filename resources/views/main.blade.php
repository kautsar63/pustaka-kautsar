@extends('layouts.app')

@section('content')
<!--====================  single row slider ====================-->
<div class="single-row-slider-area section-space">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--=======  shop page header  =======-->
                <div class="shop-header">
                    <div class="shop-header__left">
                        <div class="shop-header__left__message">
                            <span style="font-size:20px; font-weight:600; color:#033147;">{{ strtoupper($title) }}</span>
                        </div>
                    </div>
                
                    <div class="shop-header__right">
                        <div class="single-select-block d-inline-block">
                            <span class="select-title">Urutkan Berdasarkan:</span>
                            <form method="get" style="display: inline">
                                <select class="pr-4" name="sort" onchange="this.form.submit()">
                                    <option value="1" {{ isset($_GET['sort']) && $_GET['sort'] == 1 ? 'selected' : '' }}>Nama (A-Z)</option>
                                    <option value="2" {{ isset($_GET['sort']) && $_GET['sort'] == 2 ? 'selected' : '' }}>Buku Terbaru</option>
                                    <option value="3" {{ isset($_GET['sort']) && $_GET['sort'] == 3 ? 'selected' : '' }}>Harga Tertinggi</option>
                                    <option value="4" {{ isset($_GET['sort']) && $_GET['sort'] == 4 ? 'selected' : '' }}>Harga Terendah</option>
                                </select>
                            </form>
                        </div>
                    </div>
                </div>
                <!--=======  End of shop page header  =======-->
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
              <!--=======  shop page content  =======-->
              <div class="shop-page-content">
                  <div class="row shop-product-wrap grid five-column">
                    @foreach ($model as $row)
                        @widget('bookWidget', ['parent' => $category, 'data' => $row])
                    @endforeach
                        <!--=======  End of single grid product  =======-->
                  </div>
              </div>
              </div>
              </div>

              <!--=======  pagination area =======-->
            <div class="pagination-area">
                <div class="pagination-area__left">
                    Menampilkan {{ isset($_GET['page']) ? $_GET['page'] : 1 }}-20 dari {{ $total }} Buku
                </div>
                <div class="pagination-area__right">
                    @include('_partials/pagination', ['paginator' => $model])
                </div>
            </div>
            <!--=======  End of pagination area  =======-->
              <!--=======  End of pagination area  =======-->
              <!--=======  End of shop page content  =======-->
          </div>
        </div>
        <!--=======  End of single row slider wrapper  =======-->
    </div>
</div>
 @endsection
 
 @section('scripts')
 @parent
 <script>
    let pagination = $(document).find('.pagination-area__right').find('ul')

    pagination.removeClass('pagination')
    pagination.addClass('pagination-section')
 </script>
 @endsection