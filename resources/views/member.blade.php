@extends('layouts.app')

@section('content')
@if (Auth::user())
<!--====================  hero slider area ====================-->

<div class="section-space" style="margin-top:-55px;">
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><br>
                <div class="shop-header2">
                    <div class="shop-header__left">
                        <div class="shop-header__left__message">
                            <span style="font-size:20px; font-weight:600; color:#033147;">Halaman Member</span>
                        </div>
                    </div>
                </div>
                <div class="about-top-content-wrapper">
                    <div class="row row-30">
                        <div class="col-lg-12">
                            <div class="row">
                                <!-- My Account Tab Menu Start -->
                                <div class="col-lg-3 col-12">
                                    <div class="myaccount-tab-menu nav" role="tablist">
                                        <a href="#dashboad" class="active" data-toggle="tab"><i class="fa fa-dashboard"></i> Dasbor</a>
                                        <a href="#orders" data-toggle="tab"><i class="fa fa-cart-arrow-down"></i> Pesanan Anda</a>
                                        <a href="#wishlist" data-toggle="tab"><i class="fa ion-heart"></i> Daftar Keinginan</a>
                                        <a href="#address-edit" data-toggle="tab"><i class="fa fa-map-marker"></i> Alamat</a>
                                        <a href="#account-info" data-toggle="tab"><i class="fa fa-user"></i> Akun Anda</a>
                                        <a href="javascript:;" onclick="document.querySelector('#form-logout').submit()"><i class="fa fa-sign-out"></i> Keluar</a>
                                        <form action="{{ route('logout') }}" id="form-logout" method="post">
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                                <!-- My Account Tab Menu End -->

                                <!-- My Account Tab Content Start -->
                                <div class="col-lg-9 col-12">
                                    <div class="tab-content" id="myaccountContent">
                                        <!-- Single Tab Content Start -->
                                        <div class="tab-pane fade show active" id="dashboad" role="tabpanel">
                                            <div class="myaccount-content">
                                                <h3>Dasbor</h3>

                                                <div class="welcome mb-20" style="line-height:25px;">
                                                    <p>
                                                        Hello, <strong>{{ Auth::user()->name }}</strong>
                                                        (Jika Bukan <strong> {{ Auth::user()->name }} </strong>
                                                        <a href="javascript:;" onclick="document.querySelector('#form-logout').submit()" class="logout">
                                                            Silakan Keluar Disini
                                                        </a>)
                                                    </p>
                                                </div>

                                                <p class="mb-0" style="line-height:25px;">Dari Halaman ini, Anda dapat dengan mudah memeriksa & melihat
                                                    pesanan terakhir Anda, mengelola alamat pengiriman,
                                                    serta mengedit password dan detail akun Anda.
                                                </p>
                                            </div>
                                        </div>
                                        <!-- Single Tab Content End -->

                                        <!-- Single Tab Content Start -->
                                        <div class="tab-pane fade" id="orders" role="tabpanel">
                                            <div class="myaccount-content">
                                                <h3>Pesanan Anda</h3>

                                                <div class="myaccount-table table-responsive text-left">
                                                    <table class="table table-bordered">
                                                        <thead class="thead-light">
                                                            <tr style="font-size:13px; text-align:center;">
                                                                <th>No.</th>
                                                                <th>No.Pesanan</th>
                                                                <th>Judul Buku</th>
                                                                <th>Tanggal</th>
                                                                <th>Status</th>
                                                                <th>Total</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody style="font-size:13px;">
                                                            @php $inc = 1 @endphp
                                                            @foreach ($orders as $order)
                                                            <tr>
                                                                <td>{{ $inc }}.</td>
                                                                <td>{{ $order->code }}</td>
                                                                <td>
                                                                    @if ($order->detail)
                                                                    @foreach ($order->detail as $det)
                                                                        - {{ isset($det->product) ? $det->product->title : '' }}<br>
                                                                    @endforeach
                                                                    @else
                                                                    No detail
                                                                    @endif
                                                                </td>
                                                                <td>{{ date('d M Y', strtotime($order->created_at)) }}</td>
                                                                <td>
                                                                    @switch($order->status)
                                                                        @case(0)
                                                                            Belum Bayar
                                                                            @break
                                                                        @case(1)
                                                                            Pembayaran Diterima
                                                                            @break
                                                                        @case(2)
                                                                            Pesanan Diproses
                                                                            @break
                                                                        @case(3)
                                                                            Pesanan Dikirim
                                                                            @break
                                                                        @case(4)
                                                                            Pesanan Tiba
                                                                            @break
                                                                        @default
                                                                    @endswitch
                                                                </td>
                                                                <td>Rp. {{ number_format($order->total, 0, '', '.') }}</td>
                                                            </tr>
                                                                @php $inc++ @endphp
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Single Tab Content End -->

                                        <!-- Single Tab Content Start -->
                                        <div class="tab-pane fade" id="wishlist" role="tabpanel">
                                            <div class="myaccount-content">
                                                <h3>Daftar Keinginan</h3>

                                                <div class="myaccount-table table-responsive text-left">
                                                    <table class="table table-bordered">
                                                        <thead class="thead-light">
                                                            <tr style="font-size:13px; text-align:center;">
                                                                <th>No.</th>
                                                                <th>Sampul</th>
                                                                <th>Judul Buku</th>
                                                                <th>Harga</th>
                                                                <th>Beli Sekarang</th>
                                                                <th class="pro-remove">Hapus</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody style="font-size:13px; text-align:center;" id="wishlist-table"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Single Tab Content End -->

                                        <!-- Single Tab Content Start -->
                                        <div class="tab-pane fade" id="payment-method" role="tabpanel">
                                            <div class="myaccount-content">
                                                <h3>Metode Pembayaran</h3>

                                                <p class="saved-message">Anda Belum Menyimpan Metode Pembayaran Anda.</p>
                                            </div>
                                        </div>
                                        <!-- Single Tab Content End -->

                                        <!-- Single Tab Content Start -->
                                        <div class="tab-pane fade" id="address-edit" role="tabpanel">
                                            <div class="myaccount-content">
                                                <h3>Alamat Pengiriman</h3>

                                                <address>
                                                    <p><strong>{{ isset($address->member) ? $address->member->name : '' }}</strong></p>
                                                    <p>
                                                        {{ isset($address->name) ? $address->name : '' }}<br>
                                                        {{ isset($address->address) ? $address->address : '' }}<br>
                                                        {{ isset($address->district) ? $address->district : '' }}<br>
                                                        {{ isset($address->city) ? $address->city : '' }}<br>
                                                        {{ isset($address->region) ? $address->region : '' }}<br>
                                                    </p>
                                                    <p>{{ isset($address->phone) ? $address->phone : '' }}</p>
                                                </address>

                                                <!-- Button trigger modal -->
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                                                    Ubah Alamat
                                                </button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLongTitle">Edit Alamat</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{{ route('save-address') }}" method="post">
                                                                    @csrf
                                                                    <input type="hidden" id="district_id" name="district_id" value="{{ $address->district_id }}">
                                                                    <div class="login-form">
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-12">
                                                                                <label>Nama</label>
                                                                                <input type="text" name="name" value="{{ $address->name }}" placeholder="Masukkan Nama ...">
                                                                            </div>
                                                                            <div class="col-md-12 col-12">
                                                                                <label>Propinsi</label>
                                                                                <select class="nice-select" name="region" id="province">
                                                                                    @foreach ($province as $pro)
                                                                                        <option value="{{ $pro->id }}"{{ $address->region == $pro->name ? ' selected' : '' }}>{{ $pro->name }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-12">
                                                                                <label>Kota</label>
                                                                                <select class="nice-select" name="city" id="city"></select>
                                                                                <input type="hidden" id="city_id" name="city_id" value="{{ $address->city_id }}">
                                                                            </div>
                                                                            <div class="col-12">
                                                                                <label>Kecamatan</label>
                                                                                <select class="nice-select" name="district" id="district"></select>
                                                                            </div>
                                                                            <div class="col-12">
                                                                                <label>Kode Pos</label>
                                                                                <input type="text" id="postal_code" name="postal_code" value="{{ $address->postal_code }}" placeholder="Masukkan Nomor Kode Pos ...">
                                                                            </div>
                                                                            <div class="col-12">
                                                                                <label>Alamat Lengkap</label>
                                                                                <input type="text" name="address" value="{{ $address->address }}" placeholder="Masukkan Alamat Lengkap ...">
                                                                            </div>
                                                                            <div class="col-12">
                                                                                <label>No Telepon</label>
                                                                                <input type="number" name="phone" value="{{ $address->phone }}" placeholder="Masukkan Nomor Telepon ...">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <button type="submit" class="register-button3">Simpan Alamat Baru</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Single Tab Content End -->

                                        <!-- Single Tab Content Start -->
                                        <div class="tab-pane fade" id="account-info" role="tabpanel">
                                            <div class="myaccount-content">
                                                <h3>Akun Anda</h3>

                                                <div class="account-details-form">
                                                    <form action="{{ route('edit-profile') }}" method="post">
                                                        @csrf
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <input id="first-name" placeholder="Nama" name="name" type="text" required>
                                                            </div>

                                                            <div class="col-12">
                                                                <input id="email" placeholder="Email" type="email" value="{{ \Auth::user()->email }}" readonly>
                                                            </div>

                                                            <div class="col-12 mb-2">
                                                                <h4>Ubah Password</h4>
                                                            </div>

                                                            <div class="col-12">
                                                                <input id="current-pwd" name="old_password" placeholder="Password Lama" type="password">
                                                            </div>

                                                            <div class="col-lg-6 col-12">
                                                                <input id="new-pwd" name="password" placeholder="Password Baru" type="password">
                                                            </div>

                                                            <div class="col-lg-6 col-12">
                                                                <input id="confirm-pwd" name="password_confirmation" placeholder="Konfirmasi Password" type="password">
                                                            </div>

                                                            <div class="col-12">
                                                                <button type="submit" class="save-change-btn">Simpan</button>
                                                            </div>

                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Single Tab Content End -->
                                    </div>
                                </div>
                                <!-- My Account Tab Content End -->
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@else
<div class="category-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12" style="margin-top: 80px"><br>
                <h1 style="text-align: center">Space untuk Member Area</h1>
                <h4 style="text-align: center">Silakan masuk terlebih dahulu</h4>
            </div>
        </div>
    </div>
</div>

<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endif
@endsection

@section('scripts')
@parent
<script>
    function loadWishlist () {
        url = base_url + '/wishlist'
        $wishlist = $('#wishlist-table')

        $.getJSON(url, function (items) {
            $wishlist.html('')

            for (let i = 0; i < items.length; i++) {
                const discount = items[i].price - (items[i].price * (items[i].discount / 100))
                const template = `
                    <tr>
                        <td>${i + 1}</td>
                        <td align="center">
                            <a href="${base_url}/detail/${items[i].link}" style="width: 64px">
                                <img src="${base_url}/uploads/${items[i].image_url}" class="img-fluid" alt="${items[i].title}">
                            </a>
                        </td>
                        <td><a href="${base_url}/detail/${items[i].link}">${items[i].title}</a></td>
                        <td><p class="single-grid-product__price"><span class="discounted-price">${formatRupiah(discount)}</span>
                        <br><span class="main-price discounted">${formatRupiah(items[i].price)}</span></p></td>
                        <td>
                            <a href="javascript:;"
                                class="add_to_cart btn btn-primary"
                                data-id="${items[i].id}"
                                data-title="${items[i].title}"
                                data-image_url="${items[i].image_url}"
                                data-price="${discount}"
                                data-qty="1"
                                data-link="${items[i].link}"
                            >Beli Sekarang</a>
                        </td>
                        <td class="pro-remove">
                            <a href="javascript:void(0)" data-id="${items[i].id}" class="remove-wishlist">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr>
                `

                $wishlist.append(template)
            }
        })
    }

    loadWishlist()

    const city_selected = '{{ isset($address->city) ? $address->city : '' }}'
    const district_selected = '{{ isset($address->district) ? $address->district : '' }}'
    const district_id = '{{ isset($address->district_id) ? $address->district_id : '' }}'

    document.getElementById('district_id').value = district_id

    $('#province').on('change', function () {
        $('#city').html('')
        $.getJSON(base_url + '/get-city/' + $(this).val(), function (items) {
            let html = ''
            for (let i = 0; i < items.length; i++)
                html += '<option value="' + items[i].name + '" data-id="' + items[i].id + '" data-zipcode="' + items[i].postal_code + '">' + items[i].type + ' - ' + items[i].name + '</option>'

            $('#city').append(html)
            $('#city').val(city_selected)
        })
    }).trigger('change')

    $('#city').on('change', function () {
        const zipcode = $(this).find(':selected').data('zipcode')
        let city_id = $(this).find(':selected').data('id')

        if (city_id == undefined)
            city_id = $('#city_id').val()

        $('#district').html('')
        $.getJSON(base_url + '/get-district/' + city_id, function (items) {
            const data = items.rajaongkir.results
            let html = ''
            for (let i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].subdistrict_name + '" data-id="' + data[i].subdistrict_id + '">' + data[i].subdistrict_name + '</option>'
            }

            $('#district').append(html)
            $('#district').val(district_selected)
        })

        $('#city_id').val(city_id)
        $('#zipcode').val(zipcode)
    }).trigger('change')

    $('#district').on('change', function () {
        document.getElementById('district_id').value = $(this).find(':selected').data('id')
    })
</script>
@endsection
