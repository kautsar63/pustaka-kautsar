@extends('layouts.app')

@section('content')
<!--====================  hero slider area ====================-->

<div class="section-space" style="margin-top:-55px;">
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
          <div class="col-lg-12"><br>
            <div class="shop-header2">
              <div class="shop-header__left">
                  <div class="shop-header__left__message">
                      <span style="font-family: 'Great Vibes', cursive; font-size:30px; color:#033147;">Spesial Dari Redaksi</span>
                  </div>
              </div>
            </div>
              <div class="about-top-content-wrapper">
                  <div class="row row-30">
                      <!-- About Image -->
                      <div class="col-lg-4">
                          <div class="about-image2">
                              <img src="{{ asset('uploads/' . $model->image_url) }}" class="img-fluid" alt="" width="370" height="235">
                          </div>
                      </div>

                      <!-- About Content -->
                      <div class="about-content col-lg-8">
                          <div class="row">
                              <div class="col-12">
                                  <h3 style="color:#033147; text-transform:uppercase;">{{ $model->title }}</h3>
                                  <!-- <p class="post-meta"><i>Oleh <a href="javascript:;">Redaksi</a></i></p> -->
                                  <p>
                                      {!! $model->body !!}
                                  </p>
                              </div>

                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection