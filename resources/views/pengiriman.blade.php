@extends('layouts.app')

@section('content')
<div class="section-space" style="margin-top:-55px;">
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
          <div class="col-lg-12"><br>
            <div class="shop-header2">
              <div class="shop-header__left">
                  <div class="shop-header__left__message">
                      <span style="font-size:20px; font-weight:600; color:#033147;">Informasi Pengiriman</span>
                  </div>
              </div>
            </div>
              <div class="about-top-content-wrapper">
                  <div class="row row-30">

                      <!-- About Content -->
                      <div class="about-content col-lg-12">
                          <div class="row">
                              <div class="col-12">
                                  <p class="mb-3" style="font-size:13px; line-height: 30px; text-align:justify;">
                                    Kami bekerjasama dengan jasa logistik partner (Standar, Ekspres, SameDay, atau Instant). Jasa logistik partner yang bekerja sama dengan Pustaka Al-Kautsar adalah JNE, Tiki, Pos Indonesia, GoSend, Grab Express, Lion Parcel, dan SiCepat.
                                  </p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection