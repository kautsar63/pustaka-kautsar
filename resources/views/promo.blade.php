@extends('layouts.app')

@section('content')
<!--====================  single row slider ====================-->
<div class="single-row-slider-area section-space">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--=======  shop page header  =======-->
                <div class="shop-header">
                    <div class="shop-header__left">
                        <div class="shop-header__left__message">
                            <span style="font-size:20px; font-weight:600; color:#033147;">{{ strtoupper($model->title) }}</span>
                        </div>
                    </div>
                </div>
                <!--=======  End of shop page header  =======-->
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <!--=======  shop page content  =======-->
                <div class="shop-page-content">
                    <div class="row shop-product-wrap grid five-column">
                        @foreach ($books as $row)
                            @widget('bookWidget', ['parent' => $model, 'data' => $row->product])
                        @endforeach
                    </div>
                </div>

                <!--=======  pagination area =======-->
                <div class="pagination-area">
                    <div class="pagination-area__left">
                        Menampilkan {{ isset($_GET['page']) ? $_GET['page'] : 0 }}-20 dari {{ $total }} Buku
                    </div>
                    <div class="pagination-area__right">
                        @include('_partials/pagination', ['paginator' => $books])
                    </div>
                </div>
                <!--=======  End of pagination area  =======-->
                <!--=======  End of shop page content  =======-->
            </div>
        </div>
        <!--=======  End of single row slider wrapper  =======-->
    </div>
</div>
<!--====================  End of single row slider  ====================-->

<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection
