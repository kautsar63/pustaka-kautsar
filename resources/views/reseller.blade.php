@extends('layouts.app')

@section('content')
<!--====================  hero slider area ====================-->

<div class="section-space" style="margin-top:-55px;">
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><br>
            <div class="shop-header2">
                <div class="shop-header__left">
                    <div class="shop-header__left__message">
                        <span style="font-size:20px; font-weight:600; color:#033147;">Jadi Reseller dan Dropshipper</span>
                    </div>
                </div>
            </div>
                <div class="about-top-content-wrapper">
                    <div class="row row-30">
                        <!-- About Image -->

                        <!-- About Content -->
                        <div class="about-content col-lg-12">
                            <div class="row">
                                <div class="col-12">
                                    <h3 style="color:#033147; text-transform:uppercase;">Bagiamana Menjadi Reseller Pustaka Al-Kautsar?</h3>
                                    <p class="mb-3" style="font-size:13px; text-align:justify;">1. Pendaftaran Gratis dengan cara mengisi form yang sudah disediakan.</p>

                                    <p class="mb-3" style="font-size:13px; text-align:justify;">2. Setelah disetujui calon Reseller wajib menandatangani Surat Perjanjian Kerjasama penjualan dengan Pustaka Al-Kautsar.</p>

                                    <p class="mb-3" style="font-size:13px; text-align:justify;">3. Pembelian pertama minimal senilai Rp. 1.500.000,-/Netto untuk semua buku.</p>

                                    <p class="mb-3" style="font-size:13px; text-align:justify;">4. Reseller berhak mendapatkan discount awal sebesar 35% yang nantinya akan di evaluasi dalam kurun waktu 6 bulan.</p>

                                    <p class="mb-3" style="font-size:13px; text-align:justify;">5. Untuk pembelian selanjutnya minimal sebesar Rp. 200.000,- (Netto), atau 10 exp Buku
(Semua Buku) per satu kali transaksi.</p>
                                    <p class="mb-3" style="font-size:13px; text-align:justify;">6. Reseller dapat mengajukan perubahan discount dengan syarat dan ketentuan berlaku.
Hak untuk menyetujui atau menolak sepenuhnya berada ditangan Pustaka Al-Kautsar.</p>
                                    <p class="mb-3" style="font-size:13px; text-align:justify;">7. Apabila dalam masa enam bulan, Reseller tidak aktif atau jumlah pembelian tidak
memenuhi syarat kesepakatan sesuai dengan Surat Perjanjian Kerjasama Penjualan maka Pustaka Al-Kautsar akan menurunkan grade Reseller dan Droshipper menjadi pembeli perorangan biasa (Pemberlakuan discount 20% ).</p>
                                    <p class="mb-3" style="font-size:13px; text-align:justify;">8. Reseller berhak mendapatkan konten promosi publikasi dan ikut serta dalam program-
program penjualan yang diadakan oleh Pustaka Al-Kautsar Grup.</p>
                                </div>
                                
                                <div class="col-12">
                                    <h3 style="color:#033147; text-transform:uppercase; padding-top:20px;">Bagiamana Menjadi Dropshipper Pustaka Al-Kautsar?</h3>
                                    <p class="mb-3" style="font-size:13px; text-align:justify;">1. Pendaftaran Gratis dengan cara mengisi form yang sudah disediakan.</p>

                                    <p class="mb-3" style="font-size:13px; text-align:justify;">2. Setelah disetujui calon Dropshipper wajib menandatangani Surat Perjanjian Kerjasama
penjualan dengan Pustaka Al-Kautsar.</p>

                                    <p class="mb-3" style="font-size:13px; text-align:justify;">3. Dropshipper berhak mendapatkan discount sebesar 30% untuk pembelian minimal
sebesar Rp. 200.000,- (Netto), atau 10 exp Buku (Semua Buku) per satu kali transaksi.</p>

                                    <p class="mb-3" style="font-size:13px; text-align:justify;">4. Dropshipper dapat mengajukan perubahan discount dengan syarat dan ketentuan
berlaku. Hak untuk menyetujui atau menolak sepenuhnya berada ditangan Pustaka Al-Kautsar.</p>

                                    <p class="mb-3" style="font-size:13px; text-align:justify;">5. Apabila dalam masa enam bulan, Dropshipper tidak aktif atau jumlah pembelian tidak
memenuhi syarat kesepakatan sesuai dengan Surat Perjanjian Kerjasama Penjualan maka Pustaka Al-Kautsar akan menurunkan grade Reseller dan Droshipper menjadi
pembeli perorangan biasa (Pemberlakuan discount 20% ).</p>
                                    <p class="mb-3" style="font-size:13px; text-align:justify;">6. Dropshipper berhak mendapatkan konten promosi publikasi dan ikut serta dalam
program-program penjualan yang diadakan oleh Pustaka Al-Kautsar Grup.</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection