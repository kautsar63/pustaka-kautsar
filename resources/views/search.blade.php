@extends('layouts.app')

@section('content')
<!--====================  hero slider area ====================-->
<div class="section-space" style="margin-top:-40px;">
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="single-row-slider-area section-space">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="shop-header2">
                    <div class="shop-header__left">
                        <div class="shop-header__left__message">
                            <span style="font-size:20px; font-weight:600; color:#033147;">HASIL PENCARIAN</span>
                        </div>
                    </div>
                </div>
                <!--=======  shop page content  =======-->
                @if (count($model) == 0)
                Tidak Menemukan Buku Yang Anda Cari
                @else
                <div class="row">
                    <div class="col-lg-12">
                        <div class="shop-page-content">
                            <div class="row shop-product-wrap grid five-column">
                                @foreach ($model as $row)
                                    @if (count($model) == 1)
                                        <div class="col-12 col-lg-3 col-md-4 col-sm-6 col-lg-is-4">
                                            <div class="single-grid-product grid-view-product">
                                                @widget('bookWidget', ['parent' => null, 'data' => $row])
                                            </div>
                                        </div>
                                    @else
                                        @widget('bookWidget', ['parent' => null, 'data' => $row])
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <!--=======  End of blog page content  =======-->
            </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
@endsection
