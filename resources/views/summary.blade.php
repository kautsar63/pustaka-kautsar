@extends('layouts.app')

@section('content')
<div class="section-space" style="margin-top:-55px;">
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="about-top-content-wrapper">
                    <div class="row row-30">
                        <!-- About Content -->
                        <div class="about-content col-lg-12">
                            <div class="row">
                                <div class="col-12">
                                    <table class="table">
                                        <tr>
                                            <td>Nama</td>
                                            <td>{{ \Auth::user()->name }}</td>
                                        </tr>
                                        <tr>
                                            <td>Transfer ke {{ strtoupper($model['payment-method']) }}</td>
                                            <td>
                                                @if ($model['payment-method'] == 'bca')
                                                BCA No. 005 3409 153 A/N TOHIR BAWAZIR
                                                @elseif ($model['payment-method'] == 'mandiri')
                                                MANDIRI No. 006 008 0000 080 A/N TOHIR BAWAZIR
                                                @elseif ($model['payment-method'] == 'bsm')
                                                MANDIRI SYARIAH No. 7000 0438 12 A/N TOHIR BAWAZIR
                                                @elseif ($model['payment-method'] == 'bni')
                                                BNI SYARIAH No. 009 2495 620 A/N PUSTAKA ALKAUTSAR
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Kode Transaksi</td>
                                            <td>{{ $model['code'] }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Buku</th>
                                                            <th>Jumlah</th>
                                                            <th>Harga</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @for ($i = 0; $i < count($model['id']); $i++)
                                                        <tr>
                                                            <td>{{ $model['title'][$i] }}</td>
                                                            <td>{{ $model['qty'][$i] }}</td>
                                                            <td>Rp. {{ number_format($model['price'][$i], 0, '', '.') }}</td>
                                                        </tr>
                                                        @endfor
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Jasa {{ strtoupper($model['courier_code']) }} - {{ $model['courier_name'] }}</td>
                                            <td>{{ $model['courier'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Sub Total</td>
                                            <td>Rp. {{ number_format($model['sub_total'], 0, '', '.') }}</td>
                                        </tr>
                                        <tr>
                                            <td>Total</td>
                                            <td>{{ $model['total'] }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <h1></h1>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@parent
<script>
    $(document).ready(function () {
        localStorage.clear();
        
        loadCart()
    })
</script>
@endsection