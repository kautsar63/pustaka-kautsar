@extends('layouts.app')

@section('content')
<!--====================  hero slider area ====================-->

<div class="section-space" style="margin-top:-55px;">
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><br>
            <div class="shop-header2">
                <div class="shop-header__left">
                    <div class="shop-header__left__message">
                        <span style="font-size:20px; font-weight:600; color:#033147;">Apa Kata Mereka?</span>
                    </div>
                </div>
            </div>
                <div class="about-top-content-wrapper">
                        <!-- About Content -->
                        <div class="about-content col-lg-12">
                            <div class="row">
                                <div class="col-12">
                                    <h4 style="color:#033147;">Usama - <span style="font-size:16px; font-style: italic; font-weight:400;">Manajer</span></h4>
                                    <p class="mb-3" style="font-size:13px; text-align:justify;">"Buku-bukunya sangat bagus dan menambah ilmu yang bermanfaat bagi saya."</p>
                                </div>

                            </div>
                        </div>
                    <br>
                                    <!--=======  pagination area =======-->
                                    <div class="pagination-area">
                                    <div class="pagination-area__left">
                                        Menampilkan 1-5 dari 10 Testimonial
                                    </div>
                                        <div class="pagination-area__right">
                                        <ul class="pagination-section">
                                            <li><a href="#"><</a></li>
                                            <li><a class="active" href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">></a></li>
                                        </ul>
                                        </div>
                                    </div>
                                    <!--=======  End of pagination area  =======-->
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->
@endsection