@extends('layouts.app')

@section('content')
<div class="hero-slider-area section-space--30">
    <!--=======  hero slider wrapper  =======-->

    <div class="hero-slider-wrapper">
        <div class="ht-slick-slider" data-slick-setting='{
        "slidesToShow": 1,
        "slidesToScroll": 1,
        "arrows": true,
        "dots": true,
        "autoplay": true,
        "autoplaySpeed": 5000,
        "speed": 1000,
        "fade": true,
        "infinite": true,
        "prevArrow": {"buttonClass": "slick-prev", "iconClass": "ion-chevron-left" },
        "nextArrow": {"buttonClass": "slick-next", "iconClass": "ion-chevron-right" }
    }' data-slick-responsive='[
        {"breakpoint":1501, "settings": {"slidesToShow": 1} },
        {"breakpoint":1199, "settings": {"slidesToShow": 1, "arrows": false} },
        {"breakpoint":991, "settings": {"slidesToShow": 1, "arrows": false} },
        {"breakpoint":767, "settings": {"slidesToShow": 1, "arrows": false} },
        {"breakpoint":575, "settings": {"slidesToShow": 1, "arrows": false} },
        {"breakpoint":479, "settings": {"slidesToShow": 1, "arrows": false} }
    ]'>

            <!--=======  single slider item  =======-->

            <div class="single-slider-item">
                <div class="hero-slider-item-wrapper hero-slider-item-wrapper--fullwidth--fixed-height hero-slider-bg-16">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="hero-slider-content">
                                    <p class="slider-title slider-title--big-light">Promo Hari Ini!</p>
                                    <p class="slider-title slider-title--big-light">Fiqih Wanita</p>
                                    <p class="slider-title slider-title--small">Buku best seller dengan cetakan yang sudah ke 40, dapatkan segera sebelum kehabisan.</p>
                                    <a class="hero-slider-button" href="#"> <i class="ion-ios-plus-empty"></i> Beli Sekarang</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--=======  End of single slider item  =======-->

            <!--=======  single slider item  =======-->

            <div class="single-slider-item">
                <div class="hero-slider-item-wrapper hero-slider-item-wrapper--fullwidth--fixed-height hero-slider-bg-17">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                              <div class="hero-slider-content">
                                  <p class="slider-title slider-title--big-light">Segera Terbit!</p>
                                  <p class="slider-title slider-title--big-light">Fiqih Wanita</p>
                                  <p class="slider-title slider-title--small">Buku best seller dengan cetakan yang sudah ke 40, dapatkan segera sebelum kehabisan.</p>
                                  <a class="hero-slider-button" href="#"> <i class="ion-ios-plus-empty"></i>Pre-Order</a>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--=======  End of single slider item  =======-->

            <!--=======  single slider item  =======-->

            <div class="single-slider-item">
                <div class="hero-slider-item-wrapper hero-slider-item-wrapper--fullwidth--fixed-height hero-slider-bg-18">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                              <div class="hero-slider-content">
                                  <p class="slider-title slider-title--big-light">Buku Baru!</p>
                                  <p class="slider-title slider-title--big-light">Fiqih Wanita</p>
                                  <p class="slider-title slider-title--small">Buku best seller dengan cetakan yang sudah ke 40, dapatkan segera sebelum kehabisan.</p>
                                  <a class="hero-slider-button" href="#"> <i class="ion-ios-plus-empty"></i> Beli Sekarang</a>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--=======  End of single slider item  =======-->

        </div>
    </div>

    <!--=======  End of hero slider wrapper  =======-->
</div>

<!--====================  End of hero slider area  ====================-->
<!--====================  category area ====================-->
<div class="category-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--=======  category wrapper  =======-->
                <div class="category-wrapper category-wrapper--style2">
                    <div class="row">
                        <div class="col-md-4">
                            <!--=======  single category item  =======-->
                            <div class="single-category-item single-category-item--style1">
                                <div class="single-category-item__image">
                                    <a href="#">
                                        <img src="{{ asset('assets/img/banners/promo-bulan-ini.jpg') }}" class="img-fluid" alt="">
                                    </a>
                                </div>
                            </div>
                            <!--=======  End of single category item  =======-->
                        </div>
                        <div class="col-md-4">
                            <!--=======  single category item  =======-->
                            <div class="single-category-item single-category-item--style2">
                                <div class="single-category-item__image">
                                    <a href="#">
                                        <img src="{{ asset('assets/img/banners/pre-order2.jpg') }}" class="img-fluid" alt="">
                                    </a>
                                </div>
                            </div>
                            <!--=======  End of single category item  =======-->
                        </div>
                        <div class="col-md-4">
                            <!--=======  single category item  =======-->
                            <div class="single-category-item single-category-item--style1">
                                <div class="single-category-item__image">
                                    <a href="#">
                                        <img src="{{ asset('assets/img/banners/kirim-naskah.jpg') }}" class="img-fluid" alt="">
                                    </a>
                                </div>
                            </div>
                            <!--=======  End of single category item  =======-->
                        </div>
                    </div>
                </div>
                <!--=======  End of category wrapper  =======-->
            </div>
        </div>
    </div>
</div>
<!--====================  End of category area  ====================-->
<!--====================  feature logo area ====================-->
<div class="section-space">
</div>
<!--====================  End of feature logo area  ====================-->


<!--====================  single row slider ====================-->
<div class="single-row-slider-area section-space footer-area--flash-bg">
    <div class="container ">
        <div class="row">
            <div class="col-lg-12">
              <!--=======  shop page header  =======-->
              <div class="shop-header">
                <div class="shop-header__left">
                    <div class="d-flex">
                        <span style="font-family: 'Racing Sans One', cursive; font-size:36px; font-weight:600; color:#ce2f29; padding-top:2px; margin-right:20px;">FLASH SALE</span>
                        <div class="product-countdown" data-countdown="2021/02/05"></div>

                    </div>
                </div>
                  <div class="shop-header__right">
                      <div class="single-select-block d-inline-block">
                          <span style="font-size:14px; font-weight:400; color:#0f8949;"><a href="#">Lihat Semua >></a></span>
                      </div>
                  </div>
              </div>
              <!--=======  End of shop page header  =======-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!--=======  single row slider wrapper  =======-->
                <div class="single-row-slider-wrapper" >
                    <div class="ht-slick-slider" data-slick-setting='{
                    "slidesToShow": 5,
                    "slidesToScroll": 1,
                    "arrows": true,
                    "autoplay": true,
                    "autoplaySpeed": 5000,
                    "speed": 1000,
                    "infinite": false,
                    "prevArrow": {"buttonClass": "slick-prev", "iconClass": "ion-chevron-left" },
                    "nextArrow": {"buttonClass": "slick-next", "iconClass": "ion-chevron-right" }
                }' data-slick-responsive='[
                    {"breakpoint":1501, "settings": {"slidesToShow": 4} },
                    {"breakpoint":1199, "settings": {"slidesToShow": 4, "arrows": false} },
                    {"breakpoint":991, "settings": {"slidesToShow": 3, "arrows": false} },
                    {"breakpoint":767, "settings": {"slidesToShow": 2, "arrows": false} },
                    {"breakpoint":575, "settings": {"slidesToShow": 2, "arrows": false} },
                    {"breakpoint":479, "settings": {"slidesToShow": 1, "arrows": false} }
                ]'>

                        <div class="col">
                            <!--=======  single grid product  =======-->
                            <div class="single-grid-product">
                                <div class="single-grid-product__image">
                                    <div class="single-grid-product__label">
                                        <span class="sale">-20%</span>
                                    </div>
                                    <img src="{{ asset('assets/img/buku/khalid-bin-walid.jpg') }}" class="img-fluid" alt="">
                                    <div class="hover-icons">
                                        <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                        <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                    </div>
                                </div>
                                <div class="single-grid-product__content">

                                    <h3 class="single-grid-product__title"> <a href="khalid-bin-walid.html">Khalid Bin Al-Walid</a></h3>
                                    <p>Manshur Abdul Hakim</p><br>
                                    <p class="single-grid-product__price"><span class="discounted-price">Rp.102.000</span> <span class="main-price discounted">Rp.120.000</span></p>
                                    <br>
                                </div>
                            </div>
                            <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/al-wafi.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">Al-Wafi</a></h3>
                                  <p>Dr. Musthafa Dieb Al-Bugha, Syeikh Muhyiddin Mistu</p>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.76.000</span> <span class="main-price discounted">Rp.95.000</span></p>
                                  <br>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/bangkit-dan-runtuhnya-andalusia.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">Bangkit dan Runtuhnya Andalusia</a></h3>
                                  <p>Prof. DR. Raghib As-Sirjani</p>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.152.000</span> <span class="main-price discounted">Rp.190.000</span></p>
                                  <br>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/al-baqiyatus-shalihat.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">Al-Baqiyatus Shalihat</a></h3>
                                  <p>Rabi' Abdur Rauf Az-Zawawi</p><br>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.76.000</span> <span class="main-price discounted">Rp.95.000</span></p>
                                  <br>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/jati-diri-wanita-muslimah.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">Jati Diri Wanita Muslimah</a></h3>
                                  <p>Dr. Muhammad Ali Al-Hasyimi</p><br>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.76.000</span> <span class="main-price discounted">Rp.95.000</span></p>
                                  <br>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/as-suluk-al-ijtimai.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">As-Suluk Al-Ijtima'i</a></h3>
                                  <p>Syaikh Hasan Ayyub</p><br>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.76.000</span> <span class="main-price discounted">Rp.95.000</span></p>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                    </div>
                </div>
                <!--=======  End of single row slider wrapper  =======-->
            </div>
        </div>
    </div>
</div>
<!--====================  End of single row slider  ====================-->

<!--====================  single row slider ====================-->
<div class="single-row-slider-area section-space">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
              <!--=======  shop page header  =======-->
              <div class="shop-header2">
                  <div class="shop-header__left">
                      <div class="shop-header__left__message">
                          <span style="font-size:24px; font-weight:600; color:#033147;">BUKU BARU</span>
                      </div>
                  </div>
                  <div class="shop-header__right">
                      <div class="single-select-block d-inline-block">
                          <span style="font-size:14px; font-weight:400; color:#033147;"><a href="#">Lihat Semua >></a></span>
                      </div>
                  </div>
              </div>
              <!--=======  End of shop page header  =======-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!--=======  single row slider wrapper  =======-->
                <div class="single-row-slider-wrapper">
                    <div class="ht-slick-slider" data-slick-setting='{
                    "slidesToShow": 5,
                    "slidesToScroll": 1,
                    "arrows": true,
                    "autoplay": false,
                    "autoplaySpeed": 5000,
                    "speed": 1000,
                    "infinite": false,
                    "prevArrow": {"buttonClass": "slick-prev", "iconClass": "ion-chevron-left" },
                    "nextArrow": {"buttonClass": "slick-next", "iconClass": "ion-chevron-right" }
                }' data-slick-responsive='[
                    {"breakpoint":1501, "settings": {"slidesToShow": 4} },
                    {"breakpoint":1199, "settings": {"slidesToShow": 4, "arrows": false} },
                    {"breakpoint":991, "settings": {"slidesToShow": 3, "arrows": false} },
                    {"breakpoint":767, "settings": {"slidesToShow": 2, "arrows": false} },
                    {"breakpoint":575, "settings": {"slidesToShow": 2, "arrows": false} },
                    {"breakpoint":479, "settings": {"slidesToShow": 1, "arrows": false} }
                ]'>

                        <div class="col">
                            <!--=======  single grid product  =======-->
                            <div class="single-grid-product">
                                <div class="single-grid-product__image">
                                    <div class="single-grid-product__label">
                                        <span class="sale">-20%</span>
                                    </div>
                                    <img src="{{ asset('assets/img/buku/tadabur-juz-amma.jpg') }}" class="img-fluid" alt="">
                                    <div class="hover-icons">
                                        <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                        <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                    </div>
                                </div>
                                <div class="single-grid-product__content">

                                    <h3 class="single-grid-product__title"> <a href="#">Tadabur Juz Amma</a></h3>
                                    <p>DR. Saiful Bahri</p><br>
                                    <p class="single-grid-product__price"><span class="discounted-price">Rp.100.000</span> <span class="main-price discounted">Rp.125.000</span></p>
                                </div>
                            </div>
                            <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/bangkit-dan-runtuhnya-khilafah-utsmaniyah.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">Bangkit Dan Runtuhnya Khilafah Utsmaniyah</a></h3>
                                  <p>Prof. DR. Ali Muhammad Ash-Shallabi</p>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.128.000</span> <span class="main-price discounted">Rp.160.000</span></p>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/sejarah-bangsa-tartar.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">Sejarah Bangsa TarTar</a></h3>
                                  <p>Prof. DR. Raghrib As-Sirjani</p><br>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.116.000</span> <span class="main-price discounted">Rp.145.000</span></p>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/ar-risalah.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">Ar-Risalah</a></h3>
                                  <p>Imam As-Syafi'i</p><br>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.92.000</span> <span class="main-price discounted">Rp.115.000</span></p>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/jati-diri-wanita-muslimah.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">Jati Diri Wanita Muslimah</a></h3>
                                  <p>Dr. Muhammad Ali Al-Hasyimi</p><br>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.76.000</span> <span class="main-price discounted">Rp.95.000</span></p>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/as-suluk-al-ijtimai.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">As-Suluk Al-Ijtima'i</a></h3>
                                  <p>Syaikh Hasan Ayyub</p><br>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.76.000</span> <span class="main-price discounted">Rp.95.000</span></p>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                    </div>
                </div>
                <!--=======  End of single row slider wrapper  =======-->
            </div>
        </div>
    </div>
</div>
<!--====================  End of single row slider  ====================-->

<!--====================  single row slider ====================-->
<div class="single-row-slider-area section-space">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
              <!--=======  shop page header  =======-->
              <div class="shop-header2">
                  <div class="shop-header__left">
                      <div class="shop-header__left__message">
                          <span style="font-size:24px; font-weight:600; color:#033147;">BUKU BEST SELLERS</span>
                      </div>
                  </div>
                  <div class="shop-header__right">
                      <div class="single-select-block d-inline-block">
                          <span style="font-size:14px; font-weight:400; color:#033147;"><a href="#">Lihat Semua >></a></span>
                      </div>
                  </div>
              </div>
              <!--=======  End of shop page header  =======-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!--=======  single row slider wrapper  =======-->
                <div class="single-row-slider-wrapper">
                    <div class="ht-slick-slider" data-slick-setting='{
                    "slidesToShow": 5,
                    "slidesToScroll": 1,
                    "arrows": true,
                    "autoplay": false,
                    "autoplaySpeed": 5000,
                    "speed": 1000,
                    "infinite": false,
                    "prevArrow": {"buttonClass": "slick-prev", "iconClass": "ion-chevron-left" },
                    "nextArrow": {"buttonClass": "slick-next", "iconClass": "ion-chevron-right" }
                }' data-slick-responsive='[
                    {"breakpoint":1501, "settings": {"slidesToShow": 4} },
                    {"breakpoint":1199, "settings": {"slidesToShow": 4, "arrows": false} },
                    {"breakpoint":991, "settings": {"slidesToShow": 3, "arrows": false} },
                    {"breakpoint":767, "settings": {"slidesToShow": 2, "arrows": false} },
                    {"breakpoint":575, "settings": {"slidesToShow": 2, "arrows": false} },
                    {"breakpoint":479, "settings": {"slidesToShow": 1, "arrows": false} }
                ]'>

                        <div class="col">
                            <!--=======  single grid product  =======-->
                            <div class="single-grid-product">
                                <div class="single-grid-product__image">
                                    <div class="single-grid-product__label">
                                        <span class="sale">-20%</span>
                                    </div>
                                    <img src="{{ asset('assets/img/buku/tadabur-juz-amma.jpg') }}" class="img-fluid" alt="">
                                    <div class="hover-icons">
                                        <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                        <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                    </div>
                                </div>
                                <div class="single-grid-product__content">

                                    <h3 class="single-grid-product__title"> <a href="#">Tadabur Juz Amma</a></h3>
                                    <p>DR. Saiful Bahri</p><br>
                                    <p class="single-grid-product__price"><span class="discounted-price">Rp.100.000</span> <span class="main-price discounted">Rp.125.000</span></p>
                                </div>
                            </div>
                            <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/bangkit-dan-runtuhnya-khilafah-utsmaniyah.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">Bangkit Dan Runtuhnya Khilafah Utsmaniyah</a></h3>
                                  <p>Prof. DR. Ali Muhammad Ash-Shallabi</p>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.128.000</span> <span class="main-price discounted">Rp.160.000</span></p>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/sejarah-bangsa-tartar.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">Sejarah Bangsa TarTar</a></h3>
                                  <p>Prof. DR. Raghrib As-Sirjani</p><br>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.116.000</span> <span class="main-price discounted">Rp.145.000</span></p>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/ar-risalah.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">Ar-Risalah</a></h3>
                                  <p>Imam As-Syafi'i</p><br>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.92.000</span> <span class="main-price discounted">Rp.115.000</span></p>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/jati-diri-wanita-muslimah.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">Jati Diri Wanita Muslimah</a></h3>
                                  <p>Dr. Muhammad Ali Al-Hasyimi</p><br>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.76.000</span> <span class="main-price discounted">Rp.95.000</span></p>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/as-suluk-al-ijtimai.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">As-Suluk Al-Ijtima'i</a></h3>
                                  <p>Syaikh Hasan Ayyub</p><br>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.76.000</span> <span class="main-price discounted">Rp.95.000</span></p>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                    </div>
                </div>
                <!--=======  End of single row slider wrapper  =======-->
            </div>
        </div>
    </div>
</div>
<!--====================  End of single row slider  ====================-->

<!--====================  single row slider ====================-->
<div class="single-row-slider-area section-space">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
              <!--=======  shop page header  =======-->
              <div class="shop-header2">
                  <div class="shop-header__left">
                      <div class="shop-header__left__message">
                          <span style="font-size:24px; font-weight:600; color:#033147;">BUKU ANAK</span>
                      </div>
                  </div>
                  <div class="shop-header__right">
                      <div class="single-select-block d-inline-block">
                          <span style="font-size:14px; font-weight:400; color:#033147;"><a href="#">Lihat Semua >></a></span>
                      </div>
                  </div>
              </div>
              <!--=======  End of shop page header  =======-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!--=======  single row slider wrapper  =======-->
                <div class="single-row-slider-wrapper">
                    <div class="ht-slick-slider" data-slick-setting='{
                    "slidesToShow": 5,
                    "slidesToScroll": 1,
                    "arrows": true,
                    "autoplay": false,
                    "autoplaySpeed": 5000,
                    "speed": 1000,
                    "infinite": false,
                    "prevArrow": {"buttonClass": "slick-prev", "iconClass": "ion-chevron-left" },
                    "nextArrow": {"buttonClass": "slick-next", "iconClass": "ion-chevron-right" }
                }' data-slick-responsive='[
                    {"breakpoint":1501, "settings": {"slidesToShow": 4} },
                    {"breakpoint":1199, "settings": {"slidesToShow": 4, "arrows": false} },
                    {"breakpoint":991, "settings": {"slidesToShow": 3, "arrows": false} },
                    {"breakpoint":767, "settings": {"slidesToShow": 2, "arrows": false} },
                    {"breakpoint":575, "settings": {"slidesToShow": 2, "arrows": false} },
                    {"breakpoint":479, "settings": {"slidesToShow": 1, "arrows": false} }
                ]'>

                        <div class="col">
                            <!--=======  single grid product  =======-->
                            <div class="single-grid-product">
                                <div class="single-grid-product__image">
                                    <div class="single-grid-product__label">
                                        <span class="sale">-20%</span>
                                    </div>
                                    <img src="{{ asset('assets/img/buku/tadabur-juz-amma.jpg') }}" class="img-fluid" alt="">
                                    <div class="hover-icons">
                                        <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                        <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                    </div>
                                </div>
                                <div class="single-grid-product__content">

                                    <h3 class="single-grid-product__title"> <a href="#">Tadabur Juz Amma</a></h3>
                                    <p>DR. Saiful Bahri</p><br>
                                    <p class="single-grid-product__price"><span class="discounted-price">Rp.100.000</span> <span class="main-price discounted">Rp.125.000</span></p>
                                </div>
                            </div>
                            <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/bangkit-dan-runtuhnya-khilafah-utsmaniyah.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">Bangkit Dan Runtuhnya Khilafah Utsmaniyah</a></h3>
                                  <p>Prof. DR. Ali Muhammad Ash-Shallabi</p>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.128.000</span> <span class="main-price discounted">Rp.160.000</span></p>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/sejarah-bangsa-tartar.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">Sejarah Bangsa TarTar</a></h3>
                                  <p>Prof. DR. Raghrib As-Sirjani</p><br>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.116.000</span> <span class="main-price discounted">Rp.145.000</span></p>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/ar-risalah.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">Ar-Risalah</a></h3>
                                  <p>Imam As-Syafi'i</p><br>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.92.000</span> <span class="main-price discounted">Rp.115.000</span></p>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/jati-diri-wanita-muslimah.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">Jati Diri Wanita Muslimah</a></h3>
                                  <p>Dr. Muhammad Ali Al-Hasyimi</p><br>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.76.000</span> <span class="main-price discounted">Rp.95.000</span></p>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                        <div class="col">
                          <!--=======  single grid product  =======-->
                          <div class="single-grid-product">
                              <div class="single-grid-product__image">
                                  <div class="single-grid-product__label">
                                      <span class="sale">-20%</span>
                                  </div>
                                  <img src="{{ asset('assets/img/buku/as-suluk-al-ijtimai.jpg') }}" class="img-fluid" alt="">
                                  <div class="hover-icons">
                                      <a href="javascript:void(0)"><i class="ion-bag"></i></a>
                                      <a href="javascript:void(0)"><i class="ion-heart"></i></a>
                                      <a href="javascript:void(0)" data-toggle="modal" data-target="#quick-view-modal-container"><i class="ion-android-open"></i></a>
                                  </div>
                              </div>
                              <div class="single-grid-product__content">

                                  <h3 class="single-grid-product__title"> <a href="#">As-Suluk Al-Ijtima'i</a></h3>
                                  <p>Syaikh Hasan Ayyub</p><br>
                                  <p class="single-grid-product__price"><span class="discounted-price">Rp.76.000</span> <span class="main-price discounted">Rp.95.000</span></p>
                              </div>
                          </div>
                          <!--=======  End of single grid product  =======-->
                        </div>

                    </div>
                </div>
                <!--=======  End of single row slider wrapper  =======-->
            </div>
        </div>
    </div>
</div>
<!--====================  End of single row slider  ====================-->

<!--====================  blog post slider area ====================-->
<div class="blog-post-slider-area">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <!--=======  blog post slider border wrapper  =======-->
                <div class="blog-post-slider-border-wrapper section-space--inner">
                    <div class="row">
                        <div class="col-lg-12">
                            <!--=======  section title  =======-->
                            <div class="section-title-wrapper text-left section-space--half">
                                <span style="font-family: 'Great Vibes', cursive; font-size:44px; color:#033147; line-height:36px; text-align:left; font-weight:400;">Spesial Dari Redaksi</span>
                                <span style="font-size:16px; font-weight:400; color:#0f8949; float:right; padding-top:10px;"><a href="spesial-dari-redaksi.html">Lihat Semua >></a></span>
                            </div>
                            <!--=======  End of section title  =======-->
                        </div>
                        <div class="col-lg-12">
                            <!--=======  blog post slider wrapper  =======-->
                            <div class="blog-post-slider-wrapper">
                                <div class="ht-slick-slider" data-slick-setting='{
                                "slidesToShow": 3,
                                "slidesToScroll": 1,
                                "arrows": false,
                                "autoplay": false,
                                "autoplaySpeed": 5000,
                                "speed": 1000,
                                "infinite": true
                            }' data-slick-responsive='[
                                {"breakpoint":1501, "settings": {"slidesToShow": 3} },
                                {"breakpoint":1199, "settings": {"slidesToShow": 3} },
                                {"breakpoint":991, "settings": {"slidesToShow": 2} },
                                {"breakpoint":767, "settings": {"slidesToShow": 1} },
                                {"breakpoint":575, "settings": {"slidesToShow": 1} },
                                {"breakpoint":479, "settings": {"slidesToShow": 1} }
                            ]'>

                                    <div class="col">
                                        <!--=======  single slider post  =======-->
                                        <div class="single-slider-post">
                                            <div class="single-slider-post__image">
                                                <a href="#">
                                                    <img src="{{ asset('assets/img/redaksi/cara-menadaburi-al-quran.jpg') }}" class="img-fluid" alt="">
                                                </a>
                                            </div>
                                            <div class="single-slider-post__content">
                                                <h3 class="title"><a href="cara-menadabburi-alquran.html">Cara Menadaburi Al-Qur'an</a></h3>
                                                <p class="post-meta">Oleh <a href="#">Redaksi</a></p>
                                                <p class="short-desc">Al Quran adalah firman Allah SWT yang diturunkan dengan tujuan untuk diamalkan oleh manusia sebagai pedoman kehidupan mereka...</p>
                                                <a href="#" class="blog-post-link">Selengkapnya</a>
                                            </div>
                                        </div>
                                        <!--=======  End of single slider post  =======-->
                                    </div>

                                    <div class="col">
                                        <!--=======  single slider post  =======-->
                                        <div class="single-slider-post">
                                            <div class="single-slider-post__image">
                                                <a href="#">
                                                    <img src="{{ asset('assets/img/redaksi/memahami-ekonomi-rasulullah.jpg') }}" class="img-fluid" alt="">
                                                </a>
                                            </div>
                                            <div class="single-slider-post__content">
                                                <h3 class="title"><a href="#">Memahami Ekonomi Rasulullah SAW</a></h3>
                                                <p class="post-meta">Oleh <a href="#">Redaksi</a></p>
                                                <p class="short-desc">Buku ini mengupas tentang ekonomi Rasulullah, karakteristik Rasulullah dalam bekerja, memimpin, berbisnis...</p>
                                                <a href="#" class="blog-post-link">Selengkapnya</a>
                                            </div>
                                        </div>
                                        <!--=======  End of single slider post  =======-->
                                    </div>

                                    <div class="col">
                                      <!--=======  single slider post  =======-->
                                      <div class="single-slider-post">
                                          <div class="single-slider-post__image">
                                              <a href="#">
                                                  <img src="{{ asset('assets/img/redaksi/mengenal-perbedaan-madzhab-hambali-dan-salafi-kontemporer.jpg') }}" class="img-fluid" alt="">
                                              </a>
                                          </div>
                                          <div class="single-slider-post__content">
                                              <h3 class="title"><a href="#">Mengenal Perbedaan madzhab Hambali dan Salafi Kontemporer</a></h3>
                                              <p class="post-meta">Oleh <a href="#">Redaksi</a></p>
                                              <p class="short-desc">Buku ini menguraikan perbedaan antara pendapat Madzhab Hambali yang benar sesuai jumhur ulama...</p>
                                              <a href="#" class="blog-post-link">Selengkapnya</a>
                                          </div>
                                      </div>
                                      <!--=======  End of single slider post  =======-->
                                    </div>

                                    <div class="col">
                                      <!--=======  single slider post  =======-->
                                      <div class="single-slider-post">
                                          <div class="single-slider-post__image">
                                              <a href="#">
                                                  <img src="{{ asset('assets/img/redaksi/keteladanan-para-tokoh-sejarah-nasional.jpg') }}" class="img-fluid" alt="">
                                              </a>
                                          </div>
                                          <div class="single-slider-post__content">
                                              <h3 class="title"><a href="#">Keteladanan Para Tokoh Sejarah Nasional</a></h3>
                                              <p class="post-meta">Oleh <a href="#">Redaksi</a></p>
                                              <p class="short-desc">Buku ini sangat penting dibaca oleh bangsa Indonesia. Membaca buku ini, akan membuka cakrawala kita, terutama untuk para pemuda...</p>
                                              <a href="#" class="blog-post-link">Selengkapnya</a>
                                          </div>
                                      </div>
                                      <!--=======  End of single slider post  =======-->
                                    </div>

                                </div>
                            </div>
                            <!--=======  End of blog post slider wrapper  =======-->
                        </div>
                    </div>
                </div>
                <!--=======  End of blog post slider border wrapper  =======-->
            </div>
        </div>

    </div>
</div>
<br>
<!--====================  End of blog post slider area  ====================-->

<!--====================  quick view ====================-->
<div class="modal fade quick-view-modal-container" id="quick-view-modal-container" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-xl-12 col-lg-12">
                    <!--=======  single product main content area  =======-->
                    <div class="single-product-main-content-area">
                        <div class="row">
                            <div class="col-xl-5 col-lg-6">
                                <!--=======  product details slider area  =======-->

                                <div class="product-details-slider-area">

                                    <div class="big-image-wrapper">

                                        <div class="product-details-big-image-slider-wrapper-quick product-details-big-image-slider-wrapper--bottom-space ht-slick-slider" data-slick-setting='{
                                          "slidesToShow": 1,
                                          "slidesToScroll": 1,
                                          "arrows": false,
                                          "autoplay": false,
                                          "autoplaySpeed": 5000,
                                          "fade": true,
                                          "speed": 500,
                                          "prevArrow": {"buttonClass": "slick-prev", "iconClass": "fa fa-angle-left" },
                                          "nextArrow": {"buttonClass": "slick-next", "iconClass": "fa fa-angle-right" }
                                          }' data-slick-responsive='[
                                          {"breakpoint":1501, "settings": {"slidesToShow": 1, "arrows": false} },
                                          {"breakpoint":1199, "settings": {"slidesToShow": 1, "arrows": false} },
                                          {"breakpoint":991, "settings": {"slidesToShow": 1, "arrows": false, "slidesToScroll": 1} },
                                          {"breakpoint":767, "settings": {"slidesToShow": 1, "arrows": false, "slidesToScroll": 1} },
                                          {"breakpoint":575, "settings": {"slidesToShow": 1, "arrows": false, "slidesToScroll": 1} },
                                          {"breakpoint":479, "settings": {"slidesToShow": 1, "arrows": false, "slidesToScroll": 1} }
                                          ]'>
                                            <div class="single-image">
                                                <img src="{{ asset('assets/img/buku/khalid-bin-walid.jpg') }}" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--=======  End of product details slider area  =======-->
                            </div>
                            <div class="col-xl-7 col-lg-6">
                                <!--=======  single product content description  =======-->
                                <div class="single-product-content-description">
                                    <h4 class="product-title">Khalid bin Walid</h4>

                                    <p class="single-grid-product__price"><span class="discounted-price">Rp. 102.000</span> <span class="main-price discounted">Rp. 120.000</span></p>

                                    <p class="single-info">Penulis: <span class="value">Manshur Abdul Hakim</span> </p>
                                    <p class="single-info">ISBN: <span class="value">978-979-592-6870</span> </p>
                                    <p class="single-info">Cover: <span class="value">Hard Cover</span> </p>
                                    <p class="single-info">Stok: <span class="value">Tersedia</span> </p>

                                    <p class="product-description" style="text-align:justify;">Khalid bin Al-Walid belum pernah mengalami kekalahan sekalipun dalam semua pertempuran yang dihadapinya sepanjang hidupnya,
                                      dengan keistimewaan kegeniusannya dan pengalaman medan tempur yang luas, dia mampu merumuskan strategi perang yang terprogram dengan baik,
                                      mempertimbangkan dan menimbang kekuatan musuhnya.</p>

                                    <div class="product-actions product-actions--quick-view">
                                        <div class="quantity-selection">
                                            <label>Qty</label>
                                            <input type="number" value="1" min="1">
                                        </div>

                                        <div class="product-buttons">
                                            <a class="cart-btn" href="#"> <i class="ion-bag"></i> Masukkan Keranjang</a>
                                            <span class="wishlist-compare-btn">
                                              <a> <i class="ion-heart"></i></a>
                                            </span>
                                        </div>

                                    </div>


                                </div>
                                <!--=======  End of single product content description  =======-->
                            </div>
                        </div>
                    </div>
                    <!--=======  End of single product main content area  =======-->
                </div>
            </div>
        </div>

    </div>
</div>
<!--====================  End of quick view  ====================-->

@endsection