@if ($config['data']->status == 1)
<div class="col">
    <!--=======  single grid product  =======-->
    @php
    $product_price = !empty($config['data']->price) ? $config['data']->price : 0;
    $discount_price = $product_price;
    $stock = empty($config['data']->stock) ? 0 : $config['data']->stock;
    $discount = 0;

    /*
    if (!empty($config['parent'])) {
        if (isset($config['parent']) && $config['parent']->percentage > 0) {
            $discount_price = $product_price - ($product_price * ($config['parent']->percentage / 100));
            $discount = $config['parent']->percentage . '%';
        }
        
        if ($config['parent']->fix_price > 0) {
            $discount_price = $product_price - $config['parent']->fix_price;
            $discount = number_format($config['parent']->fix_price, 0, '', '.');
        }
    }*/
    
    if (!empty($config['data']->discount)) {
        $discount = $config['data']->discount . '%';
        $discount_price = $config['data']->price - ($config['data']->price * ($config['data']->discount / 100));
    }
    
    $image_url = empty($config['data']->image_url) ? 'default-image.jpeg' : $config['data']->image_url;
    $id = empty($config['data']->id) ? 0 : $config['data']->id;
    $title = empty($config['data']->title) ? '' : $config['data']->title;
    $link = empty($config['data']->link) ? '' : $config['data']->link;
    $author = empty($config['data']->author) ? '' : $config['data']->author;
    $price = empty($config['data']->price) ? 0 : $config['data']->price;
    @endphp
    <div class="single-grid-product grid-view-product">
        <div class="single-grid-product__image">
            @if ($discount > 0)
            <div class="single-grid-product__label">
                <span class="sale">{{ $discount }}</span>
            </div>
            @endif
            
            <img src="{{ asset('uploads/' . $image_url) }}" class="img-fluid" alt="{{ $title }}">
            <div class="hover-icons">
                <a href="javascript:;"
                    class="add_to_cart"
                    data-id="{{ $id }}"
                    data-title="{{ $title }}"
                    data-image_url="{{ $image_url }}"
                    data-mainprice="{{ $price }}"
                    data-price="{{ $discount_price }}"
                    data-qty="1"
                    data-link="{{ $link }}"
                ><i class="ion-bag"></i></a>
                <a href="javascript:;"
                    class="add_to_wishlist"
                    data-id="{{ $config['data']->id }}"
                ><i class="ion-heart"></i></a>
                <a href="javascript:;" data-toggle="modal" class="view-book" data-target="#quick-view-modal-container" data-id="{{ $id }}"><i class="ion-android-open"></i></a>
            </div>
        </div>
        <div class="single-grid-product__content">
            <h3 class="single-grid-product__title"> <a href="{{ url('detail/' . $link) }}">{{ $title }}</a></h3>
            <p>{{ !empty($author) ? $author->name : '' }}</p>
            <p class="single-grid-product__price">
                @if ($stock == 0)
                    <label class="badge badge-danger" style="font-weight: bold">HABIS</label>
                @else
                    <span class="discounted-price">Rp. {{ number_format($discount_price, 0, '', '.') }}</span>
                    @if ($discount > 0)
                        <span class="main-price discounted">Rp. {{ number_format($price, 0, '', '.') }}</span></p>
                    @endif
                @endif
        </div>
    </div>
    <!--=======  End of single grid product  =======-->
</div>
@endif