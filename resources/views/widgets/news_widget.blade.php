<div class="col">
    <!--=======  single slider post  =======-->
    <div class="single-slider-post">
        <div class="single-slider-post__image">
            <a href="{{ url('news/' . $config['data']->link) }}">
                <img src="{{ asset('uploads/' . $config['data']->image_url) }}" class="img-fluid" alt="{{ $config['data']->title }}">
            </a>
        </div>
        <div class="single-slider-post__content">
            <h3 class="title"><a href="{{ url('news/' . $config['data']->link) }}">{{ $config['data']->title }}</a></h3>
            <div class="short-desc">
                {!! \Str::limit($config['data']->body, 200) !!}
            </div>
            <a href="{{ url('news/' . $config['data']->link) }}" class="blog-post-link">Selengkapnya</a>
        </div>
    </div>
    <!--=======  End of single slider post  =======-->
</div>