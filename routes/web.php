<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('home', 'HomeController@index')->name('home');
Route::get('phpinfo', function () {
    phpinfo();
});

Auth::routes();

Route::prefix('admin')->name('admin.')->namespace('Admin')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Route::get('/{link}', 'HomeController@detail');
    Route::get('home', 'HomeController@index')->name('home');
    Route::resource('category', 'CategoryController');
    Route::get('book/select', 'BookController@select');
    Route::resource('book', 'BookController');
    Route::get('order/member', 'OrderController@member')->name('order.member');
    Route::put('order/status', 'OrderController@status')->name('order.status');
    Route::get('order/summary', 'OrderController@summary')->name('order.summary');
    Route::resource('order', 'OrderController');
    Route::resource('catalog', 'CatalogController');
    Route::resource('ads', 'AdsController');
    Route::resource('event', 'EventController');
    Route::resource('news', 'NewsController');
    Route::resource('promo', 'PromoController');
    Route::resource('author', 'AuthorController');
    Route::resource('slider', 'SliderController');
    Route::resource('report', 'ReportController');
    Route::get('member/order', 'MemberController@transactions')->name('member.order');
    Route::resource('member', 'MemberController');
    Route::resource('user', 'UserController');
    Route::post('logout', '\App\Http\Controllers\Auth\AdminLoginController@logout')->name('logout');
});

Route::get('admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('admin/login', 'Auth\AdminLoginController@login')->name('admin.login');
Route::get('admin/register', 'Auth\AdminLoginController@showRegisterPage')->name('admin.register');
Route::post('admin/register', 'Auth\AdminLoginController@register')->name('admin.register');

// frontend
Route::get('register', 'Auth\RegisterController@showRegisterForm')->name('register');
Route::get('member', 'HomeController@member')->name('member');
Route::post('search', 'HomeController@search')->name('search');
Route::get('get-city/{id}', 'HomeController@getCity')->name('get-city');
Route::get('get-district/{id}', 'HomeController@getDistrict')->name('get-district');
Route::get('cart', 'HomeController@cart')->name('cart');
Route::get('book/{id}', 'HomeController@book')->name('book');
Route::get('detail/{link}', 'HomeController@detail')->name('detail');
Route::get('checkout', 'HomeController@checkout')->name('checkout');
Route::post('checkout', 'HomeController@saveCheckout')->name('checkout');
Route::get('courier-check', 'HomeController@courierCheck')->name('courier-check');
Route::post('summary', 'HomeController@summary')->name('summary');
Route::get('catalog', 'HomeController@catalog')->name('catalog');
Route::get('events', 'HomeController@event')->name('events');
Route::get('event/{link}', 'HomeController@eventDetail')->name('event');
Route::get('promo/{link}', 'HomeController@promo')->name('promo');
Route::get('news-all', 'HomeController@news')->name('news-all');
Route::get('news/{link}', 'HomeController@showNews')->name('news');
Route::get('category/{link}', 'HomeController@category')->name('category');
Route::get('about-us', 'HomeController@aboutUs')->name('about-us');
Route::get('send-document', 'HomeController@sendDocument')->name('send-document');
Route::get('send-ilustrator', 'HomeController@sendIlustrator')->name('send-ilustrator');
Route::post('save-document', 'HomeController@saveDocument')->name('save-document');
Route::post('save-address', 'UserController@saveAddress')->name('save-address');
Route::post('edit-profile', 'UserController@editProfile')->name('edit-profile');
Route::get('call-us', 'HomeController@callUs')->name('call-us');
Route::post('message', 'HomeController@message')->name('message');
Route::get('wishlist', 'HomeController@wishlist')->name('wishlist');
Route::post('wishlist', 'HomeController@setWishlist')->name('wishlist');
Route::post('delete-wishlist', 'HomeController@deleteWishlist')->name('delete-wishlist');
Route::get('reseller', 'HomeController@reseller')->name('reseller');
Route::get('credit', 'HomeController@credit')->name('credit');
Route::get('testimonial', 'HomeController@testimonial')->name('testimonial');
Route::get('forgot-password', 'HomeController@forgotPassword')->name('forgot-password');
Route::post('send-forgot-password', 'HomeController@sendForgotPassword')->name('send-forgot-password');
Route::get('cara-belanja', 'HomeController@caraBelanja')->name('cara-belanja');
Route::get('pembayaran', 'HomeController@pembayaran')->name('pembayaran');
Route::get('pengiriman', 'HomeController@pengiriman')->name('pengiriman');